#include "Sheep.h"
#include "Wolf.h"
#include "Grass.h"


Sheep::Sheep()
{
	/*Empty*/
}

Sheep::Sheep(PotatoEngine::ResourceManager * resManager, glm::vec2 loc, char health)
{
	loc_ = loc;
	health_ = health;
	//Get data for later use, while printing. (sim is 1920x1080 simulated)

	textureSize_ = sf::IntRect(0, 0, 16, 16);
	textureColor_ = sf::Color(255, 255, 255, 255);
	imageTexture_ = resManager->getTexture("resources/textures/Sheep.png");
	
}

Sheep::~Sheep()
{
	cleanDetectionData();
}

void Sheep::update()
{
	/*SET BEHAVOIR*/

	//If sheep sees wolf, and is not eating, then run! else its to  focused on eating.
	if (detectedWolfs_.size() > 0 && currentBehaviour_ != EAT_GRASS)
	{
		currentBehaviour_ = EVADE;
	}//Else if sheep is on grass, and is not at max, then eat some grass
	else if (standingGrass_->hasGrass() && health_ < SHEEP_MAX_HEALTH-2)
	{
		currentBehaviour_ = EAT_GRASS;
	}// Else if sheep is at max,then produce more sheeps
	else if (health_ >= SHEEP_REQUIRED_HP_TO_SPAWN)
	{
		currentBehaviour_ = MORE_SHEEP;
	} //Else check if we have not seeked before, if not, seek
	else if ( currentBehaviour_ != SEEK)
	{
		currentBehaviour_ = SEEK;
	}
	else //Else if we have  seeked before, then just wander. wander around
	{
		currentBehaviour_ = WANDER;
	}

	//After setting  behaviour, run its behaviour.
	handleBehaviour();

	//Loss health each tick.
	health_ -= SHEEP_HEALTH_DECAY;
}

void Sheep::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	drawArea_ = sf::FloatRect(
		loc_.x * (1920 / GRID_SIZE_WIDTH) - 0.1f,
		loc_.y * (1080 / GRID_SIZE_HEIGHT) - 0.1f,
		(1920 / GRID_SIZE_WIDTH) + 0.2f,
		(1080 / GRID_SIZE_HEIGHT) + 0.2f);

	spriteBatch->draw(
		imageTexture_,
		drawArea_,
		textureSize_,
		textureColor_);
}



void Sheep::cleanDetectionData()
{
	detectedGrass_.clear();
	detectedSheep_.clear();
	detectedWolfs_.clear();
}

void Sheep::addSeenGrass(std::shared_ptr<Grass> seenGrass)
{
	detectedGrass_.push_back(seenGrass);
	if (seenGrass->getLocation() == loc_)
	{
		standingGrass_ = seenGrass;
	}
}

void Sheep::addSeenSheep(std::shared_ptr<Sheep> seenSheep)
{
	detectedSheep_.push_back(seenSheep);
}

void Sheep::addSeenWolf(std::shared_ptr<Wolf> seenWolf)
{
	detectedWolfs_.push_back(seenWolf);
}

void Sheep::handleBehaviour()
{
	glm::vec2 newLoc = glm::vec2(0);
	glm::vec2 moveDir = glm::vec2(0);
	int standingOneGrassIde = loc_.x + loc_.y * GRID_SIZE_WIDTH;

	switch (currentBehaviour_)
	{
	case EVADE: 
		/*RUN AWAY FROM WOLF!!!!*/
		locOfClosedWolf_ = glm::vec2(100, 100);
		for (auto &wolf : detectedWolfs_)
		{
			if (glm::distance(wolf->getLocation(), loc_) < glm::distance(locOfClosedWolf_, loc_))
			{
				locOfClosedWolf_ = wolf->getLocation();
			}
		}

		//Set move dirs
		if (locOfClosedWolf_.y > loc_.y && loc_.y > 0)
			moveDir.y = -1;
		else if (locOfClosedWolf_.y < loc_.y && loc_.y < GRID_SIZE_HEIGHT-1)
			moveDir.y = 1;

		if (locOfClosedWolf_.x > loc_.x && loc_.x > 0)
			moveDir.x = -1;
		else if (locOfClosedWolf_.x < loc_.x && loc_.x < GRID_SIZE_WIDTH-1)
			moveDir.x = 1;

		loc_ = loc_ + moveDir;
		break;
	case EAT_GRASS:
		/*REDUCE HEALTH OF GRASS AND INCRASE SHEPE HEALTH*/
		standingGrass_->dealDamage(SHEEP_EATING_AMOUNT);
		health_ += SHEEP_EAT_HEALTH_GAIN;
		if (health_ > SHEEP_MAX_HEALTH)
			health_ = SHEEP_MAX_HEALTH;

		break;
	case MORE_SHEEP: 
		/*PRODUCE MORE SHEEP AND REMOVE HEALTH*/
		health_ -= SHEEP_HEALTH_LOST_PRODUCE;
		//Production of new sheep is handled by the grid, 
		//as sheep does not handle the worlddata of  sheeps.
		
		break;
	case SEEK: 
		/*SEARCH AND DESTROY GRASS ;)*/
		foundGrass_ = false;
		for (auto &grass : detectedGrass_)
		{
			if (grass->hasGrass())
			{
				if (foundGrass_ == false )
				{
					foundGrass_ = true;
					movementGoal_ = grass->getLocation();
				}
				else if (glm::distance(movementGoal_, loc_) > 
					glm::distance(grass->getLocation(),loc_))
				{
					movementGoal_ = grass->getLocation();
				}
			}
		}
		break;
	case WANDER:
		/* WANDER */
		if (foundGrass_)
		{
			/*Move towards grass*/
			
			if (movementGoal_.x > loc_.x)
				moveDir.x = 1;
			else if (movementGoal_.x < loc_.x)
				moveDir.x = -1;

			if (movementGoal_.y > loc_.y)
				moveDir.y = 1;
			else if (movementGoal_.y < loc_.y)
				moveDir.y = -1;

			//Check so there is nothing at the space
			bool canMove = true;
			for (auto &sheep : detectedSheep_)
			{
				if (sheep->getLocation() == (loc_ + moveDir))
				{
					canMove = false;
					foundGrass_ = false;
					continue;
				}
			}

			if (canMove)
			{
				loc_ = loc_ + moveDir;
			}
		}
		else
		{
			/*Move randomly*/
			loc_ = loc_ + getRandomMovDir();
		}
		break;
	}
}


glm::vec2 Sheep::getRandomMovDir()
{
	glm::vec2 randomLoc = glm::vec2(0);

	bool searchingForSpace = true;
	int testes = 0;

	while (searchingForSpace && testes < 8)
	{
		//y
		if (loc_.y == 0)
			randomLoc.y = std::rand() % 2;
		else if (loc_.y == GRID_SIZE_HEIGHT-1)
			randomLoc.y = std::rand() % 2 - 1;
		else
			randomLoc.y = std::rand() % 3 - 1;
		//X
		if (loc_.x == 0)
			randomLoc.x = std::rand() % 2;
		else if (loc_.x == GRID_SIZE_WIDTH-1)
			randomLoc.x = std::rand() % 2 - 1;
		else
			randomLoc.x = std::rand() % 3 - 1;
		

		testes++;

		searchingForSpace = false;
		for (auto &sheep : detectedSheep_)
		{
			if (sheep->getLocation() == (loc_ + randomLoc))
			{
				searchingForSpace = true;
				continue;
			}
		}
	}

	if (testes >= 8)
	{
		randomLoc = glm::vec2(0);
	}

	return randomLoc;
}
