#ifndef ASSIGNMENT1_SHEEP_H
#define ASSIGNMENT1_SHEEP_H

#include <iostream>
#include <memory>
#include <vector>
#include <functional>
#include <PotatoEngine\ResourceManager.h>

#include "Entity.h"
#include "SimulationSettings.h"



class Wolf;
class Grass;
enum SheepBehaviour
{
	EVADE,
	EAT_GRASS,
	MORE_SHEEP,
	SEEK,
	WANDER
};

class Sheep : public Entity
{
public:
	//Constructors
	Sheep();
	Sheep(PotatoEngine::ResourceManager* resManager, glm::vec2 loc, char health = SHEEP_START_HEALTH);
	
	//Deconstructor
	~Sheep();

	void update() override;
	void draw(PotatoEngine::SpriteBatch * spriteBatch) override;

	SheepBehaviour getBehaviour() { return currentBehaviour_; }
	void setBehaviour(SheepBehaviour newBehaviour) { currentBehaviour_ = newBehaviour; }

	//Functions for handling detection
	void cleanDetectionData();
	void addSeenGrass(std::shared_ptr<Grass> seenGrass);
	void addSeenSheep(std::shared_ptr<Sheep> seenSheep);
	void addSeenWolf(std::shared_ptr<Wolf> seenWolf);

private:
	void handleBehaviour();
	glm::vec2 getRandomMovDir();

	//Variables

	//Points to detected stuff.
	std::shared_ptr<Grass> standingGrass_;
	std::vector<std::shared_ptr<Grass>> detectedGrass_;
	std::vector<std::shared_ptr<Sheep>> detectedSheep_;
	std::vector<std::shared_ptr<Wolf>> detectedWolfs_;

	//location of cloest wolf.
	glm::vec2 locOfClosedWolf_;
	

	//AI STUFF
	SheepBehaviour currentBehaviour_ = SheepBehaviour::WANDER;
	glm::vec2 movementGoal_ = glm::vec2(0);
	bool foundGrass_ = false;
};

#endif // ASSIGNMENT1_SHEEP_H

