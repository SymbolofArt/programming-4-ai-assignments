#include "Grass.h"



Grass::Grass()
{
}

Grass::Grass(PotatoEngine::ResourceManager * resManger, glm::vec2 worldsize, glm::vec2 loc, char health)
	:
	resourceManger_(resManger)
{
	loc_ = loc;
	health_ = health;

	float heX = (1920 / worldsize.x);
	float heY = (1080 / worldsize.y);

	drawArea_ = sf::FloatRect(
		loc_.x * heX - 0.1f,
		loc_.y * heY - 0.1f,
		heX + 0.2f,
		heY + 0.2f);

	textureSize_ = sf::IntRect(0, 0, 16, 16);
	textureColor_ = sf::Color(255, 255, 255, 255);

	if (health_ > 0)
		isGrass_ = true;

	if (health_ > GRASS_MATURE_LEVEL)
		isMature_ = true;
}


Grass::~Grass()
{
}


void Grass::addNeighbor(std::shared_ptr<Grass> neighbor)
{
	neighbors_.push_back(neighbor);
}

void Grass::update()
{
	//Check if grass is dying and handle its death.
	if (isDying_)
	{
		health_ -= GRASS_DYING_RATE;

		if (health_ <= 0)
		{
			isGrass_ = false;
			isDying_ = false;
			health_ = 0;
			isMature_ = false;
		}
		else if (isMature_ && health_ <= GRASS_MATURE_LEVEL)
			isMature_ = false;
	}//Else handle i growing state
	else if (isGrass_)
	{
		health_ += GRASS_GROW_RATE;
		if (health_ >= GRASS_MAX_HEALTH)
		{
			isDying_ = true;
		}
		else if (!isMature_ && health_ >= GRASS_MATURE_LEVEL)
		{
			isMature_ = true;
		}
	}

	if (isMature_)
		spreadGrass();

}

void Grass::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	//Selecft wish image to use depending on the grass health value.
	if (isGrass_)
	{
		if (health_ > 80)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_1.png");
		else if (health_ > 60)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_2.png");
		else if (health_ > 40)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_3.png");
		else if (health_ > 20)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_4.png");
		else
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_5.png");
	}
	else
	{
		imageTexture_ = resourceManger_->getTexture("resources/textures/Dirt.png");
	}
	//Draw the grass
	spriteBatch->draw(
		imageTexture_,
		drawArea_,
		textureSize_,
		textureColor_);
}

bool Grass::spreadGrassToMe(char health)
{
	//If it does not have grass, spread it to this tile. else do nothig..
	int x = loc_.x;
	int y = loc_.y;
	if (isGrass_ == false)
	{
		isGrass_ = true;
		health_ = health;
		isDying_ = false;
		isMature_ = false;
		return true;
	}
	else
	{
		return false;
	}
}

void Grass::dealDamage(short amount)
{
	health_ -= amount;
	if (health_ <= 0)
	{
		health_ = 0;
		isGrass_ = false;
		isDying_ = false;
		isMature_ = false;
	}
}

void Grass::spreadGrass()
{
	int amountOfNeighbors = neighbors_.size();

	if (amountOfNeighbors == 0)
		return;


	if (std::rand() % 100 <= GRASS_SPREAD_CHANCE)
	{
		int chosenOne = std::rand() % amountOfNeighbors;		
		neighbors_.at(chosenOne)->spreadGrassToMe(10);		
	}
	
}
