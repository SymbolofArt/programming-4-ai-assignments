#ifndef ASSIGNMENT1_ENTITY_H
#define ASSIGNMENT1_ENTITY_H
#include <glm\glm.hpp>
#include <PotatoEngine/SpriteBatch.h>

class Entity
{
public:

	virtual void update() = 0;
	virtual void draw(PotatoEngine::SpriteBatch * spriteBatch) = 0;

	
	void setLocation(glm::vec2 newLocation) { loc_ = newLocation; }
	glm::vec2 getLocation() { return loc_; }


	glm::vec4 getEntityCollisionBox()
	{
		glm::vec4 coll = glm::vec4(
			drawArea_.left,		//A = X
			drawArea_.top,		//B = Y
			drawArea_.width,	//C = Z
			drawArea_.height);	//D = W
		return coll;
	}


	void setHealth(short newHealth) { health_ = newHealth; }
	short getHealth() { return health_; }

	void dealDamage(short damage)
	{ 
		if (health_ > damage)
			health_ -= damage;
		else
			health_ = 0;
	}

protected:

	glm::vec2 loc_;
	short health_;

	//Drawing stuff

	sf::FloatRect drawArea_;
	sf::IntRect textureSize_;
	sf::Color textureColor_;
	sf::Texture* imageTexture_;
};


#endif // ASSIGNMENT1_ENTITY_H

