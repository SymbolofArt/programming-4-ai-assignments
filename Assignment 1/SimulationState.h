#ifndef ASSIGNMENT1_SIMULATIONSTATE_H
#define ASSIGNMENT1_SIMULATIONSTATE_H
enum SimulationStates
{
	STATE_NULL,
	STATE_EXIT,
	STATE_MENU,
	STATE_SIMULATING
};


//Simulation state base class
class SimulationState
{
public:

	virtual ~SimulationState() {};

	virtual void update(float deltaTime) = 0;
	virtual void draw() = 0;

};
#endif // ASSIGNMENT1_SIMULATIONSTATE_H