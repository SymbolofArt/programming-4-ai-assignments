#pragma once

/* SIMULATION SETTINGS */
#define SIMULATION_SPEED			60											///60 = 1sec

#define SIMULATION_START_GRASS		20											///Number of grass that will be on the grid world when the simulation starts.
#define SIMULATION_START_SHEEP		10											///Number of sheep that will be on the grid world when the simulation starts.
#define SIMULATION_START_WOLF		1											///Number of wolfs that will be on the grid world when the simulation starts.

/* GRID SETTINGS */
#define GRID_SIZE_HEIGHT			14											///Height of the simulation in nmumber of tiles
#define GRID_SIZE_WIDTH				32											///Width of the simulation in number of tiles.
#define GRID_SIZE					(GRID_SIZE_HEIGHT * GRID_SIZE_WIDTH)		///Total number of tiles,		---DO NOT CHANGE THIS VALUE!!-----

/* GRASS SETTINGS */

#define GRASS_MAX_HEALTH			100											///Max health the grass can have, when it reach that value it starts dying.
#define GRASS_MATURE_LEVEL			50											///IF grass is above this value, it is mature and can spread to other tiles.
#define GRASS_GROW_RATE				10											///How much hp the grass will gain each tick (if its not dying)
#define GRASS_DYING_RATE			5											///When grass has reached max health, it stops growing and starts dying at this rate.
#define GRASS_SPREAD_CHANCE			15											///Change the grass will spread each tick, Off 100%, tex 25 = 25%

/* SHEEP SETTINGS */

#define SHEEP_START_HEALTH			50											///How much health the sheep starts at.
#define SHEEP_MAX_HEALTH			100											///Max health an sheep can have.
#define SHEEP_HEALTH_DECAY			1											///Health lost each tick
#define SHEEP_REQUIRED_HP_TO_SPAWN	(SHEEP_MAX_HEALTH-SHEEP_HEALTH_DECAY)		///HP required to spawn new sheep
#define SHEEP_HEALTH_LOST_PRODUCE	50											///Health lost when sheep produce new sheep

#define SHEEP_EATING_AMOUNT			25											///How much damage the sheep does to the grass when it eats from it
#define SHEEP_EAT_HEALTH_GAIN		12											///How much health the sheep gain each time it eats.

#define SHEEP_DETECTION_AREA		3											///How long the sheep can detect grass, other sheeps and wolfs.


/* WOLF SETTINGS */
#define WOLF_START_HEALTH			75											///How much health the wolf starts at
#define WOLF_MAX_HEALTH				200											///Max health an wolf can have.
#define WOLF_HEALTH_DECAY			3											///Health lost each tick
#define WOLF_REWUIRED_HP_TO_SPAWN	(WOLF_MAX_HEALTH-WOLF_HEALTH_DECAY)			///HP required to spawn a new wolf
#define WOLF_HEALTH_LOST_PRODUCE	125											///Health lost when a wolf produce a new wolf.

#define WOLF_DAMAGE_DONE			50											///How much damage the wolf does to the shepe it attack
#define WOLF_HEALTH_GAIN			50											///How much health the wolf get each time it attack an sheep

#define WOLF_DETECTION_AREA			4											///How long the wolf can detect other sheeps and wolfs.

																				///1 = true; 0 = false
#define WOLF_DOUBLE_WALK_WN_PURSUE  0											///Wolf can walk two tiles when pursing a sheep
#define WOLF_WALK_ATTACK			1											///Wolf can attack a sheep it walks on. (it really helps, else the sheep just run away and the wolf starve :( )