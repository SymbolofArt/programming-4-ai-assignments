#ifndef ASSIGNMENT1_GRIDWORLD_H
#define ASSIGNMENT1_GRIDWORLD_H
#include <cstdlib>
#include <iostream>
#include <ctime>

#include "glm\glm.hpp"
#include <PotatoEngine/SpriteBatch.h>
#include <PotatoEngine\ResourceManager.h>
#include <PotatoEngine\Converter.h>


#include <PotatoEngine\CollisionManager.h>

#include "System.h"

#include "Grass.h"
#include "Sheep.h"
#include "Wolf.h"



class GridWorld
{
public:
	//Constructors
	GridWorld();
	GridWorld(System* system);

	//Deconstructors
	~GridWorld();


	void draw(PotatoEngine::SpriteBatch * spriteBatch);

	void update(float deltaTime);

private:

	/*Update stuff*/
	void updateSheeps();
	void updateWolfs();
	void checkForDeath();
	void checkForBirth();

	//Generation
	void generateWorldData(int grassAmount);
	void generateSheep(int amount);
	void generateWolfs(int amount);

	bool isSpotFree(glm::vec2 loc);

	bool spawnNewSheep(glm::vec2 parentLoc);
	bool spawnNewWolf(glm::vec2 parentLoc);

	
	//Variables
	System* system_;
	std::vector<std::shared_ptr<Grass>> sharedWorldData_;

	std::vector<std::shared_ptr<Sheep>> sharedWorldSheeps_;

	std::vector<std::shared_ptr<Wolf>> sharedWorldWolfs_;


};
#endif // ASSIGNMENT1_GRIDWORLD_H


