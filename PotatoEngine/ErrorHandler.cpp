#include "ErrorHandler.h"
#include <iostream>

namespace PotatoEngine
{
	//Prints out an error message and exits the game
	void fatalError(std::string errorString)
	{
		std::cout << errorString << std::endl;

		exit(1);	// Exits the game

	}


}