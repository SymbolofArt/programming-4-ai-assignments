#ifndef POTATOENGINE_WINDOW_H
#define POTATOENGINE_WINDOW_H
#include <SFML/Window.hpp>
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp> 
#include <string>

namespace PotatoEngine
{

	enum WindowFlags { NORMAL = 0x1, FULLSCREEN = 0x2, BORDERLESS = 0x4 };

	class Window
	{
	public:
		Window();
		~Window();

		///Creates the window
		int createWindow(std::string windowName, int screenWidth,
			int screenHeight, bool fullscreen = false);
		
		///End the current frame
		void swapBuffer();
		
		//Getters
		int getScreenWidth() const { return m_iScreenWidth; }
		int getScreenHeight() const { return m_iScreenHeight; }
		sf::Window* getWindow() const { return m_pxWindow; }
		//Setters
		void setFullscreen(bool fullscreen);
		///Set the resolution
		void setResolution(int width, int height);
	private:
		sf::Window* m_pxWindow;				///SFML window object
		int m_iScreenWidth;
		int m_iScreenHeight;
		std::string m_sWindowName;
	};

}
#endif // POTATOENGINE_WINDOW_H