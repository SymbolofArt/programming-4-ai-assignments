#include "InputManager.h"
namespace PotatoEngine
{
	InputManager::InputManager() : mouseCoords_(0.0f)
	{
		/*Empty*/
	}


	InputManager::~InputManager()
	{
		/*Empty*/
	}

	void InputManager::update()
	{
		//Loop through m_keyMap and copy it over to m_previousKeyMap
		for (auto& it : keyMap_)
		{
			previousKeyMap_[it.first] = it.second;
		}

		for (auto& it : mouseMap_)
		{
			previousMouseMap_[it.first] = it.second;
		}
	}

	//Keyboard
	void InputManager::pressKey(unsigned int keyID)
	{
		//Set key to pressed
		keyMap_[keyID] = true;

		latestKey_ = keyID;

	}

	void InputManager::releaseKey(unsigned int keyID)
	{
		//Set the key not pressed
		keyMap_[keyID] = false;
	}

	bool InputManager::isKeyDown(unsigned int keyID)
	{
		auto it = keyMap_.find(keyID);
		if (it != keyMap_.end())
		{
			//Found key, return whats inside
			return it->second;
		}

		//If not found, return false.
		return false;
	}

	bool InputManager::isKeyPressed(unsigned int keyID)
	{
		//Check if it was pressed this frame but not the frame before.
		if (isKeyDown(keyID) && !wasKeyDown(keyID))
		{
			return true;
		}
		return false;
	}

	bool InputManager::isAnyKeyDown()
	{
		bool anyKeyDown = false;
		//Go true the keys and check if any is down
		for (auto& it : keyMap_)
		{
			if (it.second)
			{
				//If key is down, set anykeydown to true and break from the loop.
				anyKeyDown = true;
				break;
			}
		}
		return anyKeyDown;
	}

	bool InputManager::isAnyKeyPressed()
	{
		bool anyKeyPressed = false;
		//Go true the keys and check if any is down
		for (auto& it : previousKeyMap_)
		{
			if (it.second)
			{
				//If key is down, set anykeydown to true and break from the loop.
				anyKeyPressed = true;
				break;
			}
		}
		return anyKeyPressed;
	}

	int InputManager::getLatestKeyIDPressed()
	{
		return latestKey_;
	}

	char InputManager::getLatestKeyChar()
	{
		char keyChar = '?';

		switch (latestKey_)
		{
		case 0: keyChar = 'A'; break;
		case 1: keyChar = 'B'; break;
		case 2: keyChar = 'C'; break;
		case 3: keyChar = 'D'; break;
		case 4: keyChar = 'E'; break;
		case 5: keyChar = 'F'; break;
		case 6: keyChar = 'G'; break;
		case 7: keyChar = 'H'; break;
		case 8: keyChar = 'I'; break;
		case 9: keyChar = 'J'; break;
		case 10: keyChar = 'K'; break;
		case 11: keyChar = 'L'; break;
		case 12: keyChar = 'M'; break;
		case 13: keyChar = 'N'; break;
		case 14: keyChar = 'O'; break;
		case 15: keyChar = 'P'; break;
		case 16: keyChar = 'Q'; break;
		case 17: keyChar = 'R'; break;
		case 18: keyChar = 'S'; break;
		case 19: keyChar = 'T'; break;
		case 20: keyChar = 'U'; break;
		case 21: keyChar = 'V'; break;
		case 22: keyChar = 'W'; break;
		case 23: keyChar = 'X'; break;
		case 24: keyChar = 'Y'; break;
		case 25: keyChar = 'Z'; break;
		case 26: keyChar = '0'; break;
		case 27: keyChar = '1'; break;
		case 28: keyChar = '2'; break;
		case 29: keyChar = '3'; break;
		case 30: keyChar = '4'; break;
		case 31: keyChar = '5'; break;
		case 32: keyChar = '6'; break;
		case 33: keyChar = '7'; break;
		case 34: keyChar = '8'; break;
		case 35: keyChar = '9'; break;
		case 57: keyChar = ' '; break;
		}
		return keyChar;
	}

	bool InputManager::wasKeyDown(unsigned int keyID)
	{
		auto it = previousKeyMap_.find(keyID);
		if (it != previousKeyMap_.end())
		{
			//Found key, return whats inside
			return it->second;
		}

		//If not found, return false.
		return false;
	}

	void InputManager::setKeyPressedStatus(int keyID, bool value)
	{
		auto it = previousKeyMap_.find(keyID);
		if (it != previousKeyMap_.end())
		{
			it->second = value;
		}
	}

	//Mouse
	void InputManager::pressMouse(MouseKey mouseKey)
	{
		mouseMap_[mouseKey] = true;
	}

	void InputManager::releaseMouse(MouseKey mouseKey)
	{

		mouseMap_[mouseKey] = false;
	}

	bool InputManager::isMouseDown(MouseKey mouseKey)
	{
		auto it = mouseMap_.find(mouseKey);
		if (it != mouseMap_.end())
		{
			//Found key, return whats inside
			return it->second;
		}

		//If not found, return false.
		return false;
	}

	bool InputManager::isMousePressed(MouseKey mouseKey)
	{
		//Check if it was pressed this frame but not the frame before.
		if (isMouseDown(mouseKey) && !wasMouseDown(mouseKey))
		{
			return true;
		}
		return false;
	}


	bool InputManager::wasMouseDown(MouseKey mouseKey)
	{
		auto it = previousMouseMap_.find(mouseKey);
		if (it != previousMouseMap_.end())
		{
			//Found key, return whats inside
			return it->second;
		}

		//If not found, return false.
		return false;
	}

	void InputManager::setMouseCoords(int x, int y)
	{
		mouseCoords_.x = static_cast<float>(x);
		mouseCoords_.y = static_cast<float>(y);
	}

	void InputManager::setMouseCoords(glm::vec2 pos)
	{
		mouseCoords_ = pos;
	}



}