#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include "GUI.h"
#include <iostream>
#include <windows.h>
#include "Converter.h"

/*
		TODO:
		   1: Make font use caching for batter performance. 
*/
namespace PotatoEngine
{

	/*---------------------GUI BUTTON---------------------*/

	GUIButton::GUIButton()
	{
		/*Empty*/
	}

	GUIButton::GUIButton(
		ResourceManager* resourceManager,
		glm::vec2 pos,
		glm::vec2 size,
		std::string imagePath,
		std::string text,
		unsigned int fontSize,
		std::string textFontPath,
		std::function<void(GUIButton&, float)> pressFunc,
		bool enabled) :
		enabled_(enabled)
	{
		if (!enabled_)
		{
			status_ = 3;
		}
		position_ = pos;
		size_ = size;
		buttonImage_ = resourceManager->getTexture(imagePath);
		buttonImage_->setSmooth(true);

		font_ = std::make_shared<sf::Font>();
		font_->loadFromFile(textFontPath);
		buttonText_ = std::make_shared<sf::Text>(text, *font_);
		buttonText_->setCharacterSize(fontSize);

		//Setting position. works?
		sf::FloatRect textRect = buttonText_->getLocalBounds();

		buttonText_->setPosition(sf::Vector2f(
			pos.x + size.x / 2.27f,
			pos.y + size.y / 2.0f));

		buttonText_->setOrigin(textRect.left + textRect.width / 2.0f,
			textRect.top + textRect.height / 2.0f);

		pressFunc_ = pressFunc;
		statusHelper_ = 0;
	}

	GUIButton::~GUIButton()
	{
		/*Empty*/
	}

	void GUIButton::update(InputManager * inputManager, float deltatime)
	{
		if (enabled_)
		{
			//Check if the mouse is inside or outside the box
			if (inputManager->getMouseCoords().x > GUIObject::position_.x &&
				inputManager->getMouseCoords().y > GUIObject::position_.y &&
				inputManager->getMouseCoords().x < GUIObject::position_.x + GUIObject::size_.x &&
				inputManager->getMouseCoords().y < GUIObject::position_.y + GUIObject::size_.y)
			{
				if (statusHelper_ == 0)
				{
					if (inputManager->isMousePressed(LEFT) && status_ != 2)
					{
						status_ = 0;
						statusHelper_ = 1;
					}
					else
					{
						status_ = 1;
					}

				}
				else
				{
					if (!inputManager->isMouseDown(LEFT) && status_ == 0)
					{
						statusHelper_ = 0;
						status_ = 1;
						pressFunc_(*this, deltatime);
					}
				}
			}
			else
			{
				status_ = 2;
				statusHelper_ = 0;
			}
		}
	}

	void GUIButton::draw(SpriteBatch* spriteBatch)
	{
		spriteBatch->draw(buttonImage_,
			sf::FloatRect(position_.x, position_.y, size_.x, size_.y),
			sf::IntRect(0,
				buttonImage_->getSize().y / 4 * status_,
				buttonImage_->getSize().x,
				buttonImage_->getSize().y / 4),
			sf::Color(255, 255, 255, 255));
	}

	void GUIButton::drawText(sf::RenderWindow* window)
	{
		window->draw(*buttonText_);

	}

	void PotatoEngine::GUIButton::runFunction(float deltatime)
	{
		pressFunc_(*this, deltatime);
	}

	void PotatoEngine::GUIButton::setEnabled(bool enabled)
	{
		enabled_ = enabled;
		if (enabled)
		{
			status_ = 2;
		}
		else
		{
			status_ = 3;
		}
	}

	void PotatoEngine::GUIButton::setPos(glm::vec2 position)
	{
		GUIObject::setPos(position);

		sf::FloatRect textRect = buttonText_->getLocalBounds();
		buttonText_->setPosition(sf::Vector2f(
			position_.x + size_.x / 2.27f,
			position_.y + size_.y / 2.0f));
		buttonText_->setOrigin(textRect.left + textRect.width / 2.0f,
			textRect.top + textRect.height / 2.0f);
	}

	/*---------------------GUI LABEL---------------------*/
	GUILabel::GUILabel()
	{
		/* Empty */
	}

	GUILabel::~GUILabel()
	{
		/* Empty */
	}

	void PotatoEngine::GUILabel::draw(SpriteBatch * spriteBatch)
	{
		/*Empty, dont use*/
	}

	GUILabel::GUILabel(ResourceManager* resourceManager, glm::vec2 pos, std::string text, std::string fontPath, int fontSize)
	{
		position_ = pos;


		font_ = std::make_shared<sf::Font>();
		font_->loadFromFile(fontPath);


		text_ = std::make_shared<sf::Text>(text, *font_);

		text_->setCharacterSize(fontSize);
		text_->setPosition(sf::Vector2f(position_.x, position_.y));

	}

	void GUILabel::drawText(sf::RenderWindow* window)
	{
		if (position_.x != text_->getPosition().x ||
			position_.y != text_->getPosition().y)
		{
			text_->setPosition(sf::Vector2f(position_.x, position_.y));
		}

		window->draw(*text_);

	}

	void GUILabel::setFontSize(int size)
	{
		text_->setCharacterSize(size);
	}

	void PotatoEngine::GUILabel::setText(std::string text)
	{
		text_->setString(text);
	}

	void PotatoEngine::GUILabel::setOrigin(unsigned char data)
	{

		sf::FloatRect textRect = text_->getLocalBounds();
		//X
		if (data & StylePos::X_MIDDLE)
		{
			text_->setOrigin(textRect.left + textRect.width / 2.0f,
				text_->getOrigin().y);
		}
		else if (data & StylePos::X_LEFT)
		{
			text_->setOrigin(textRect.left,
				text_->getOrigin().y);
		}
		else if (data & StylePos::X_RIGHT)
		{
			text_->setOrigin(textRect.left + textRect.width,
				text_->getOrigin().y);
		}

		//Y
		if (data & StylePos::Y_MIDDLE)
		{
			text_->setOrigin(text_->getOrigin().x,
				textRect.top + textRect.height / 2.0f);
		}
		else if (data & StylePos::Y_TOP)
		{
			text_->setOrigin(text_->getOrigin().x,
				textRect.top);
		}
		else if (data & StylePos::Y_BOTTOM)
		{
			text_->setOrigin(text_->getOrigin().x,
				textRect.top + textRect.height);
		}

	}

	void PotatoEngine::GUILabel::setColor(sf::Color color)
	{
		text_->setColor(color);
	}

	int GUILabel::getFontSize() const
	{
		return text_->getCharacterSize();
	}

	std::string PotatoEngine::GUILabel::getText() const
	{
		return text_->getString();
	}


	/*---------------------GUI IMAGE---------------------*/

	GUIImage::GUIImage(
		ResourceManager* resourceManager,
		glm::vec2 pos,
		glm::vec2 size,
		std::string imagePath,
		glm::vec4 uvRect,
		sf::Color color)
	{
		position_ = pos;
		size_ = size;
		image_ = resourceManager->getTexture(imagePath);
		image_->setRepeated(true);

		destRect_ = sf::FloatRect(position_.x, position_.y, size_.x, size_.y);

		if (uvRect == glm::vec4(0.0f))
		{
			uvRect_ = sf::IntRect(0, 0, image_->getSize().x, image_->getSize().y);
		}
		else
		{
			uvRect_ = sf::IntRect(
				static_cast<int>(uvRect.x),
				static_cast<int>(uvRect.y),
				static_cast<int>(uvRect.z),
				static_cast<int>(uvRect.w));
		}
		color_ = color;

	}

	void GUIImage::draw(SpriteBatch* spriteBatch)
	{
		destRect_ = sf::FloatRect(position_.x, position_.y, size_.x, size_.y);
		spriteBatch->draw(image_,
			destRect_,
			uvRect_,
			color_);
	}

	void GUIImage::setColor(sf::Color color)
	{
		color_ = color;
	}

	void PotatoEngine::GUIImage::setUVRect(sf::IntRect uvRect)
	{
		uvRect_ = uvRect;
	}

	sf::Color GUIImage::getColor() const
	{
		return color_;
	}

	sf::IntRect PotatoEngine::GUIImage::getUVRect() const
	{
		return uvRect_;
	}


	/*---------------------GUI SLIDER---------------------*/

	GUISlider::GUISlider()
	{

	}

	PotatoEngine::GUISlider::GUISlider(ResourceManager * resourceManager,
		glm::vec2 pos,
		glm::vec2 size,
		std::string imagePath,
		float minValue,
		float maxValue,
		float startingValue) :
		minValue_(minValue),
		maxValue_(maxValue),
		value_(startingValue),
		draging_(false),
		status_(0)
	{
		position_ = pos;
		size_ = size;

		sliderImage_ = resourceManager->getTexture(imagePath);
		sliderImage_->setSmooth(true);
	}

	GUISlider::~GUISlider()
	{

	}

	void PotatoEngine::GUISlider::update(InputManager * inputManager, float deltatime)
	{
		if (inputManager->isMouseDown() &&
			inputManager->getMouseCoords().x > position_.x &&
			inputManager->getMouseCoords().x < position_.x + size_.x &&
			inputManager->getMouseCoords().y > position_.y &&
			inputManager->getMouseCoords().y < position_.y + size_.y)
		{
			draging_ = true;
			status_ = 2;
		}
		else if (!inputManager->isMouseDown() && draging_)
		{
			draging_ = false;
			status_ = 0;
		}
		else if (inputManager->getMouseCoords().x > position_.x &&
			inputManager->getMouseCoords().x < position_.x + size_.x &&
			inputManager->getMouseCoords().y > position_.y &&
			inputManager->getMouseCoords().y < position_.y + size_.y)
		{
			status_ = 1;
		}
		else if (!draging_)
		{
			status_ = 0;
		}

		if (draging_)
		{
			//inputManager->getMouseCoords().x;
			float mouseLoc = inputManager->getMouseCoords().x;

			float minBoxPos = position_.x + size_.y / 2;
			float maxBoxPos = position_.x + size_.x - size_.y;
			float diffrenze = maxBoxPos - minBoxPos;
			diffrenze = size_.x;
			float helper = mouseLoc - minBoxPos;
			helper /= (diffrenze / maxValue_);

			value_ = helper;

			if (value_ < minValue_)
			{
				value_ = minValue_;
			}
			else if (value_ > maxValue_)
			{
				value_ = maxValue_;
			}
		}

	}

	void PotatoEngine::GUISlider::draw(SpriteBatch * spriteBatch)
	{
		//Draw line
		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				position_.x,
				position_.y,
				size_.x,
				size_.y),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				sliderImage_->getSize().y / 3 * 2,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));

		//Draw object corners
		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				position_.x,
				position_.y,
				size_.y / 2,
				size_.y),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				sliderImage_->getSize().y / 3 * 1,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));

		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				position_.x + size_.x - (size_.y / 2),
				position_.y,
				size_.y / 2,
				size_.y),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				sliderImage_->getSize().y / 3 * 1,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));

		//Draw box sliuder
		//Calculate box pos
		float boxPosx;

		float minBoxPos = position_.x + size_.y / 2;
		float maxBoxPos = position_.x + size_.x - size_.y;

		//minValue = minBoxPos
		//maxValue = maxBoxPos

		float diffrenze = maxBoxPos - minBoxPos;

		//diffrenze = 100

		boxPosx = minBoxPos + diffrenze / maxValue_ * value_;


		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				boxPosx,
				position_.y,
				size_.y / 2,
				size_.y),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				0,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));



	}

	void PotatoEngine::GUISlider::setValue(float value)
	{
		value_ = value;
	}

	void PotatoEngine::GUISlider::setMinValue(float minValue)
	{
		minValue_ = minValue;
	}

	void PotatoEngine::GUISlider::setMaxValue(float maxValue)
	{
		maxValue_ = maxValue;
	}

	float PotatoEngine::GUISlider::getValue()
	{
		return value_;
	}

	float PotatoEngine::GUISlider::getMinValue()
	{
		return minValue_;
	}

	float PotatoEngine::GUISlider::getMaxValue()
	{
		return maxValue_;
	}

	/*---------------------GUI TEXTBOX---------------------*/

	GUITextBox::GUITextBox()
	{
		/*Empty*/
	}

	PotatoEngine::GUITextBox::GUITextBox(ResourceManager * resourceManager,
		glm::vec2 pos,
		glm::vec2 size,
		std::string imagePath,
		std::string fontPath,
		std::string startText) 
		:
		inputing_(false),
		counter_(0),
		printLine_(false),
		spamHelper_(10),
		enterPressed_(false)
	{
		position_ = pos;
		size_ = size;

		font_ = std::make_shared<sf::Font>();
		font_->loadFromFile(fontPath);

		//Calculate max characters that can fitt inside the textbox
		maxCharacters_ = (int)((size_.x) / size_.y);

		textBoxTexture_ = resourceManager->getTexture(imagePath);
		textBoxTexture_->setSmooth(true);

		//Set up the text origin and start positions.

		text_ = std::make_shared<sf::Text>(startText, *font_);
		text_->setCharacterSize((int)(size_.y - size_.y/10));

		//text_->setString("Something");

		text_->setPosition(sf::Vector2f(
			position_.x + size_.x / 10,
			position_.y - size.y/10));// +size_.y / 2));

		sf::FloatRect textRect = text_->getLocalBounds();
		text_->setOrigin(
			textRect.left,
			textRect.top);
	}

	PotatoEngine::GUITextBox::~GUITextBox()
	{
		/*Empty*/
	}

	void PotatoEngine::GUITextBox::update(InputManager * inputManager, float deltatime)
	{
		enterPressed_ = false;
		counter_ += 1 * deltatime;
		if (counter_ > 30)
		{
			printLine_ = !printLine_;
			counter_ -= 30;
		}

		spamHelper_ += 1 * deltatime;

		if (inputManager->isMousePressed() &&
			inputManager->getMouseCoords().x > position_.x &&
			inputManager->getMouseCoords().x < position_.x + size_.x &&
			inputManager->getMouseCoords().y > position_.y &&
			inputManager->getMouseCoords().y < position_.y + size_.y
			)
		{
			inputing_ = true;
		}
		else if (inputManager->isMousePressed())
		{
			inputing_ = false;
		}

		if (inputing_)
		{
			if (inputManager->isKeyPressed(inputManager->getLatestKeyIDPressed()))
			{
				inputHandler(inputManager);
				spamHelper_ = 0;
			}
			else if (inputManager->isKeyDown(inputManager->getLatestKeyIDPressed())
				&& spamHelper_ > 25)
			{
				inputHandler(inputManager);
				spamHelper_ = 20;
			}
		}

	}

	void PotatoEngine::GUITextBox::draw(SpriteBatch * spriteBatch)
	{

		//Draw middle
		spriteBatch->draw(textBoxTexture_,
			sf::FloatRect(
				position_.x + size_.x / 10,
				position_.y,
				size_.x - (size_.x / 5),
				size_.y),
			sf::IntRect(
				textBoxTexture_->getSize().x / 3 * 1,
				0,
				textBoxTexture_->getSize().x / 3,
				textBoxTexture_->getSize().y
				),
			sf::Color(255, 255, 255, 255));


		//Draw left end
		spriteBatch->draw(textBoxTexture_,
			sf::FloatRect(
				position_.x,
				position_.y,
				size_.x / 10,
				size_.y),
			sf::IntRect(
				textBoxTexture_->getSize().x / 3 * 0,
				0,
				textBoxTexture_->getSize().x / 3,
				textBoxTexture_->getSize().y
				),
			sf::Color(255, 255, 255, 255));
		//Draw right end
		spriteBatch->draw(textBoxTexture_,
			sf::FloatRect(
				position_.x + size_.x - size_.x / 10,
				position_.y,
				size_.x / 10,
				size_.y),
			sf::IntRect(
				textBoxTexture_->getSize().x / 3 * 2,
				0,
				textBoxTexture_->getSize().x / 3,
				textBoxTexture_->getSize().y
				),
			sf::Color(255, 255, 255, 255));

	}

	void PotatoEngine::GUITextBox::drawText(sf::RenderWindow * window)
	{
		if (printLine_ && inputing_)
		{

			std::string newText = text_->getString();
			newText += '|';
			text_->setString(newText);
			window->draw(*text_);

			newText.pop_back();
			text_->setString(newText);
		}
		else
		{

			window->draw(*text_);
		}
	}

	std::string PotatoEngine::GUITextBox::getText() const
	{
		return text_->getString();
	}

	void PotatoEngine::GUITextBox::setInputing(bool inputing)
	{
		inputing_ = inputing;
	}

	bool PotatoEngine::GUITextBox::wasEnterPressed() const
	{
		return enterPressed_;
	}

	void PotatoEngine::GUITextBox::inputHandler(InputManager * inputManager)
	{
		char addMe = inputManager->getLatestKeyChar();
		int keyID = inputManager->getLatestKeyIDPressed();



		std::string newText = text_->getString();
		if (addMe != '?')
		{


			bool capslock = ((GetKeyState(VK_CAPITAL) & 0x0001) != 0);

			if ((!inputManager->isKeyDown(sf::Keyboard::LShift)
				&& 
				!capslock)
				||
				(inputManager->isKeyDown(sf::Keyboard::LShift)
					&&
					capslock))
			{
				addMe = putchar(tolower(addMe));
			}

			


			if (newText.size() < maxCharacters_)
			{
				newText += addMe;
			}
		}
		else if (keyID == 59 && newText.size() > 0)
		{
			newText.pop_back();
		}
		else if (keyID == 58)
		{
			inputing_ = false;
			enterPressed_ = true;
		}

		text_->setString(newText);
	}
	
	/*---------------------GUI NUMBERBOX---------------------*/
	
	GUINumerBox::GUINumerBox()
	{

	}

	PotatoEngine::GUINumerBox::GUINumerBox(ResourceManager * resourceManager,
		glm::vec2 pos,
		glm::vec2 size,
		std::string imagePath,
		std::string fontPath,
		int startNumber,
		int minNumber,
		int maxNumber,
		sf::Color textColor)
		:
		inputing_(false),
		counter_(0),
		printLine_(false),
		spamHelper_(10),
		number_(startNumber),
		minNumber_(minNumber),
		maxNumber_(maxNumber)
	{
		position_ = pos;
		size_ = size;

		font_ = std::make_shared<sf::Font>();
		font_->loadFromFile(fontPath);

		//Set max character
		maxCharacters_ = getIntCharacterSize(maxNumber_);

		if (imagePath == "NULL")
		{
			printBackground_ = false;
		}
		else
		{
			printBackground_ = true;
			textBoxTexture_ = resourceManager->getTexture(imagePath);
			textBoxTexture_->setSmooth(true);
		}



		//Set up the text origin and start positions.

		text_ = std::make_shared<sf::Text>(std::to_string(number_), *font_);
		text_->setCharacterSize((int)(size_.y - size_.y / 10));

		text_->setPosition(sf::Vector2f(
			position_.x + size_.x / 10,
			position_.y - size.y / 10));

		sf::FloatRect textRect = text_->getLocalBounds();
		text_->setOrigin(
			textRect.left,
			textRect.top - textRect.height/3);

		text_->setColor(textColor);

	}

	PotatoEngine::GUINumerBox::~GUINumerBox()
	{
		/*Empty*/
	}

	void PotatoEngine::GUINumerBox::update(InputManager * inputManager, float deltatime)
	{
		counter_ += 1 * deltatime;
		if (counter_ > 30)
		{
			printLine_ = !printLine_;
			counter_ -= 30;
		}

		spamHelper_ += 1 * deltatime;

		if (inputManager->isMousePressed() &&
			inputManager->getMouseCoords().x > position_.x &&
			inputManager->getMouseCoords().x < position_.x + size_.x &&
			inputManager->getMouseCoords().y > position_.y &&
			inputManager->getMouseCoords().y < position_.y + size_.y
			)
		{
			inputing_ = true;
		}
		else if (inputManager->isMousePressed())
		{
			inputing_ = false;
		}

		if (inputing_)
		{
			if (inputManager->isKeyPressed(inputManager->getLatestKeyIDPressed()))
			{
				inputHandler(inputManager);
				spamHelper_ = 0;
			}
			else if (inputManager->isKeyDown(inputManager->getLatestKeyIDPressed())
				&& spamHelper_ > 25)
			{
				inputHandler(inputManager);
				spamHelper_ = 20;
			}
		}
		else if (std::stoi(std::string(text_->getString())) != number_)
		{
			int newNumber = std::stoi(std::string(text_->getString()));
			if (newNumber > maxNumber_)
			{
				number_ = maxNumber_;
			}
			else if (newNumber < minNumber_)
			{
				number_ = minNumber_;
			}
			else
			{
				number_ = newNumber;
			}
			text_->setString(std::to_string(number_));
		}
	}

	void PotatoEngine::GUINumerBox::draw(SpriteBatch * spriteBatch)
	{
		if (!printBackground_)
			return;
		//Draw middle
		spriteBatch->draw(textBoxTexture_,
			sf::FloatRect(
				position_.x + size_.x / 10,
				position_.y,
				size_.x - (size_.x / 5),
				size_.y),
			sf::IntRect(
				textBoxTexture_->getSize().x / 3 * 1,
				0,
				textBoxTexture_->getSize().x / 3,
				textBoxTexture_->getSize().y
				),
			sf::Color(255, 255, 255, 255));


		//Draw left end
		spriteBatch->draw(textBoxTexture_,
			sf::FloatRect(
				position_.x,
				position_.y,
				size_.x / 10,
				size_.y),
			sf::IntRect(
				textBoxTexture_->getSize().x / 3 * 0,
				0,
				textBoxTexture_->getSize().x / 3,
				textBoxTexture_->getSize().y
				),
			sf::Color(255, 255, 255, 255));
		//Draw right end
		spriteBatch->draw(textBoxTexture_,
			sf::FloatRect(
				position_.x + size_.x - size_.x / 10,
				position_.y,
				size_.x / 10,
				size_.y),
			sf::IntRect(
				textBoxTexture_->getSize().x / 3 * 2,
				0,
				textBoxTexture_->getSize().x / 3,
				textBoxTexture_->getSize().y
				),
			sf::Color(255, 255, 255, 255));
	}

	void PotatoEngine::GUINumerBox::drawText(sf::RenderWindow * window)
	{
		if (printLine_ && inputing_)
		{

			std::string newText = text_->getString();
			newText += '|';
			text_->setString(newText);
			window->draw(*text_);

			newText.pop_back();
			text_->setString(newText);
		}
		else
		{

			window->draw(*text_);
		}
	}

	void PotatoEngine::GUINumerBox::setNumber(int number)
	{
		if (number > maxNumber_)
		{
			number_ = maxNumber_;
		}
		else if (number < minNumber_)
		{
			number_ = minNumber_;
		} 
		else if (number != number_)
		{
			number_ = number;
		}

		//Update text
		text_->setString(std::to_string(number_));
	}

	int PotatoEngine::GUINumerBox::getNumber() const
	{
		return number_;
	}

	void PotatoEngine::GUINumerBox::inputHandler(InputManager * inputManager)
	{
		char addMe = inputManager->getLatestKeyChar();
		int keyID = inputManager->getLatestKeyIDPressed();

		std::string newText = text_->getString();
		if (addMe != '?')
		{
			//make sure its only numbers
			switch (addMe)
			{
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				break;
			default:
				return;
			}


			if (newText.size() < maxCharacters_)
			{
				newText += addMe;
			}
		}
		else if (keyID == 59 && newText.size() > 0)
		{
			newText.pop_back();
		}
		else if (keyID == 58)
		{
			inputing_ = false;
		}

		text_->setString(newText);
	}



	/*---------------------GUI CheckBox---------------------*/
	GUICheckBox::GUICheckBox()
	{
		/*Empty*/
	}

	PotatoEngine::GUICheckBox::GUICheckBox(
		ResourceManager * resourceManager,
		glm::vec2 pos,
		glm::vec2 size,
		std::string imagePath,
		bool enabled) :
		enabled_(enabled)
	{
		position_ = pos;
		size_ = size;
		checkBoxImage_ = resourceManager->getTexture(imagePath);
	}

	GUICheckBox::~GUICheckBox()
	{
		/*Empty*/
	}

	void PotatoEngine::GUICheckBox::update(InputManager * inputManager, float deltatime)
	{
		if (inputManager->getMouseCoords().x > GUIObject::position_.x &&
			inputManager->getMouseCoords().y > GUIObject::position_.y &&
			inputManager->getMouseCoords().x < GUIObject::position_.x + GUIObject::size_.x &&
			inputManager->getMouseCoords().y < GUIObject::position_.y + GUIObject::size_.y)
		{
			mouseHover = true;
			if (inputManager->isMousePressed(LEFT))
			{
				enabled_ = !enabled_;
			}
		}
		else
		{
			mouseHover = false;
		}
	}

	void PotatoEngine::GUICheckBox::draw(SpriteBatch * spriteBatch)
	{
		if (enabled_)
		{
			spriteBatch->draw(checkBoxImage_,
				sf::FloatRect(position_.x, position_.y, size_.x, size_.y),
				sf::IntRect(0,
					checkBoxImage_->getSize().y / 3 * 0,
					checkBoxImage_->getSize().x,
					checkBoxImage_->getSize().y / 3),
				sf::Color(255, 255, 255, 255));
		}
		else
		{
			spriteBatch->draw(checkBoxImage_,
				sf::FloatRect(position_.x, position_.y, size_.x, size_.y),
				sf::IntRect(0,
					checkBoxImage_->getSize().y / 3 * 2,
					checkBoxImage_->getSize().x,
					checkBoxImage_->getSize().y / 3),
				sf::Color(255, 255, 255, 255));
		}

		if (mouseHover)
		{
			//Draw other side.
			spriteBatch->draw(checkBoxImage_,
				sf::FloatRect(
					position_.x ,
					position_.y,
					size_.x,
					size_.y),
				sf::IntRect(
					0,
					checkBoxImage_->getSize().y / 3 * 1,
					checkBoxImage_->getSize().x,
					checkBoxImage_->getSize().y / 3),
				sf::Color(255, 255, 255, 255/2));
		}
	}

	void PotatoEngine::GUICheckBox::setEnabled(bool enabled)
	{
		enabled_ = enabled;
	}
	bool PotatoEngine::GUICheckBox::isEnabled()
	{
		return enabled_;
	}


	/*---------------------GUI GUIVerticalSlider---------------------*/

	GUIVerticalSlider::GUIVerticalSlider()
	{
		
	}
	PotatoEngine::GUIVerticalSlider::GUIVerticalSlider(ResourceManager * resourceManager,
		glm::vec2 pos,
		glm::vec2 size,
		std::string imagePath,
		float minValue,
		float maxValue,
		float startingValue) :
		minValue_(minValue),
		maxValue_(maxValue),
		value_(startingValue),
		draging_(false),
		status_(0)
	{
		position_ = pos;
		size_ = size;

		sliderImage_ = resourceManager->getTexture(imagePath);
		sliderImage_->setSmooth(true);
	}

	PotatoEngine::GUIVerticalSlider::~GUIVerticalSlider()
	{
	}

	void PotatoEngine::GUIVerticalSlider::update(InputManager * inputManager, float deltatime)
	{
		if (inputManager->isMouseDown() &&
			inputManager->getMouseCoords().x > position_.x &&
			inputManager->getMouseCoords().x < position_.x + size_.x &&
			inputManager->getMouseCoords().y > position_.y &&
			inputManager->getMouseCoords().y < position_.y + size_.y)
		{
			draging_ = true;
			status_ = 2;
		}
		else if (!inputManager->isMouseDown() && draging_)
		{
			draging_ = false;
			status_ = 0;
		}
		else if (inputManager->getMouseCoords().x > position_.x &&
			inputManager->getMouseCoords().x < position_.x + size_.x &&
			inputManager->getMouseCoords().y > position_.y &&
			inputManager->getMouseCoords().y < position_.y + size_.y)
		{
			status_ = 1;
		}
		else if (!draging_)
		{
			status_ = 0;
		}

		if (draging_)
		{
			//inputManager->getMouseCoords().x;
			float mouseLoc = inputManager->getMouseCoords().y;

			float minBoxPos = position_.y + size_.x / 2;
			float maxBoxPos = position_.y + size_.y - size_.x * 2 - size_.x / 2;
			float diffrenze = maxBoxPos - minBoxPos;

			float helper = mouseLoc - minBoxPos;
			helper /= (diffrenze / maxValue_);

			value_ = helper;

			if (value_ < minValue_)
			{
				value_ = minValue_;
			}
			else if (value_ > maxValue_)
			{
				value_ = maxValue_;
			}
		}
	}

	void PotatoEngine::GUIVerticalSlider::draw(SpriteBatch * spriteBatch)
	{
		//Draw line
		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				position_.x,
				position_.y,
				size_.x,
				size_.y),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				sliderImage_->getSize().y / 3 * 2,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));

		//Draw object corners
		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				position_.x,
				position_.y,
				size_.x,
				size_.x / 2),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				sliderImage_->getSize().y / 3 * 1,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));

		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				position_.x,
				position_.y + size_.y - (size_.x / 2),
				size_.x,
				size_.x / 2),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				sliderImage_->getSize().y / 3 * 1,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));

		//Draw box sliuder
		//Calculate box pos
		float boxPosy;

		float minBoxPos = position_.y + size_.x / 2;
		float maxBoxPos = position_.y + size_.y - size_.x * 2 - size_.x / 2;


		float diffrenze = maxBoxPos - minBoxPos;

		//diffrenze = 100

		boxPosy = minBoxPos + diffrenze / maxValue_ * value_;


		spriteBatch->draw(sliderImage_,
			sf::FloatRect(
				position_.x,
				boxPosy,
				size_.x,
				size_.x * 2),
			sf::IntRect(
				sliderImage_->getSize().x / 3 * status_,
				0,
				sliderImage_->getSize().x / 3,
				sliderImage_->getSize().y / 3
				),
			sf::Color(255, 255, 255, 255));
	}

	void PotatoEngine::GUIVerticalSlider::setValue(float value)
	{
		value_ = value;
	}

	void PotatoEngine::GUIVerticalSlider::setMinValue(float minValue)
	{
		minValue_ = minValue;
	}

	void PotatoEngine::GUIVerticalSlider::setMaxValue(float maxValue)
	{
		maxValue_ = maxValue;
	}

	float PotatoEngine::GUIVerticalSlider::getValue()
	{
		return value_;
	}

	float PotatoEngine::GUIVerticalSlider::getMinValue()
	{
		return minValue_;
	}

	float PotatoEngine::GUIVerticalSlider::getMaxValue()
	{
		return maxValue_;
	}


}
