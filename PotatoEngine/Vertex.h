#ifndef POTATOENGINE_VERTEX_H
#define POTATOENGINE_VERTEX_H
#include <GL/GL.h>

namespace PotatoEngine
{
	//4 float positions, x and y
	struct Position
	{
		//Constructors
		Position() : x(0), y(0) { }
		Position(float X, float Y) : x(X), y(Y) { }

		//Positions
		float x;
		float y;
	};

	//4 bytes for color. RGBA 0-255
	struct ColorRGBA
	{
		//Constructors
		ColorRGBA() : r(0), g(0), b(0), a(255) { }
		ColorRGBA(GLubyte R, GLubyte G, GLubyte B, GLubyte A) :
			r(R), g(G), b(B), a(A) { }

		GLubyte r; // Red
		GLubyte g; // Green
		GLubyte b; // Blue
		GLubyte a; // Alpha
	};

	struct UV
	{
		//Constructors
		UV() : u(0), v(0) { }
		UV(float U, float V) : u(U), v(V) { }

		float u;
		float v;

	};


	//The vertex definition
	struct Vertex
	{
		//4 float as position, x and y.
		Position position;

		//4 bytes of RGBA color
		ColorRGBA color;

		//UV texture coordinates. 
		UV uv;

		void setColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a)
		{
			color.r = r;
			color.g = b;
			color.b = b;
			color.a = a;
		}

		void setUV(float u, float v)
		{
			uv.u = u;
			uv.v = v;
		}

		void setPosition(float x, float y)
		{
			position.x = x;
			position.y = y;

		}
	};
}
#endif // POTATOENGINE_VERTEX_H