#ifndef POTATOENGINE_COLLISIONMANAGER_H
#define POTATOENGINE_COLLISIONMANAGER_H
#include <glm/glm.hpp>

namespace PotatoEngine
{
	class CollisionManager
	{
	public:
		//Constructors
		CollisionManager();
		//Deconstructors
		~CollisionManager();

		bool MouseCollision(glm::vec2 mousePos, glm::vec4 rect);		///Check if mouse is inside glm::vec4 rect
		bool AABB(glm::vec4 &a, glm::vec4 &b);							///Check if two glm::vec4 is colliding

		bool circleRectCollision(glm::vec3 cir, glm::vec4 rect);

		bool circleCollision(glm::vec3 cir1, glm::vec3 cir2);

		glm::vec2 getCollisionBoxSize(glm::vec4 &a, glm::vec4 &b);		///Returns a box of how much two inputed rects are colliding

		//Getters
		float getAreaFromBox(glm::vec2 a) { return a.x *a.y; }			///Get area from a box

	};
}
#endif // POTATOENGINE_COLLISIONMANAGER_H