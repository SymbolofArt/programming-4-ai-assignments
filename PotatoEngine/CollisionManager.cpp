#include "CollisionManager.h"
#include <glm/glm.hpp>
#include <cmath>

namespace PotatoEngine
{
	CollisionManager::CollisionManager()
	{
	}


	CollisionManager::~CollisionManager()
	{
	}


	bool CollisionManager::MouseCollision(glm::vec2 mousePos, glm::vec4 rect)
	{
		bool result = false;
		//Check if mouse is inside rect, if yes, set result to true
		if (
			(mousePos.x >= rect.x && mousePos.x <= rect.x + rect.w) && //Check X
			(mousePos.y >= rect.y && mousePos.y <= rect.y + rect.z)	 //Check Y
			)
		{
			result = true;
		}

		return result;
	}

	// Axis Aligned Bounding Box
	bool CollisionManager::AABB(glm::vec4 &a, glm::vec4 &b)
	{
		//Simple axis aligned bouding box.
		if (a.x + a.z < b.x ||
			b.x + b.z < a.x ||
			a.y + a.w < b.y ||
			b.y + b.w < a.y)
			return false;
		else
			return true;
	}

	bool CollisionManager::circleRectCollision(glm::vec3 cir, glm::vec4 rect)
	{
		glm::vec2 circleDistance;
		circleDistance.x = abs(cir.x - rect.x);
		circleDistance.y = abs(cir.y - rect.y);

		if (circleDistance.x > (rect.z / 2 + cir.z) ||
			circleDistance.y > (rect.w / 2 + cir.z))
		{
			return false;
		}

		if (circleDistance.x <= (rect.z / 2) ||
			circleDistance.y <= (rect.w / 2))
		{
			return true;
		}

		float cornerdistance_sq = 
			pow((circleDistance.x - rect.z / 2), 2) +
			pow((circleDistance.y - rect.w / 2),2);

		return (cornerdistance_sq <= pow(cir.z, 2));
	}

	bool CollisionManager::circleCollision(glm::vec3 cir1, glm::vec3 cir2)
	{	
		return 
			(sqrt((cir2.x - cir1.x) * (cir2.x - cir1.x) +
					(cir2.y - cir1.y) *
					(cir2.y - cir1.y)) <
				(cir1.z + cir2.z));
	}

	glm::vec2 CollisionManager::getCollisionBoxSize(glm::vec4 & a, glm::vec4 & b)
	{
		glm::vec2 size = glm::vec2(0);

		//Set x
		if (a.x <= b.x && (a.x + a.w) >= b.x)
		{
			size.x = a.w - (b.x - a.x);
		}
		else if (b.x < a.x && (b.x + b.w) > a.x)
		{
			size.x = b.w - (a.x - b.x);
		}

		//Set y
		if (a.y < b.y && (a.y + a.z) > b.y)
		{

			size.y = a.z - (b.y - a.y);
		}
		else if (b.y < a.y && (b.y + b.z) > a.t)
		{
			size.y = b.z - (a.y - b.y);
		}

		//Make sure it ain't bigger then the box.
		if (size.x > a.w)
			size.x = a.w;
		if (size.y > a.z)
			size.y = a.z;

		//Return the size of the collision
		return size;
	}





}