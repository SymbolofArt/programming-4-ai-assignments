#include "AudioManager.h"
#include <iostream>

namespace PotatoEngine
{
	AudioManager::AudioManager()
	{

	}

	AudioManager::AudioManager(ResourceManager * resourceManager)
		: resourceManager_(resourceManager)
	{
		soundVolume_ = 100;
		music_ = new sf::Music();
	}

	AudioManager::~AudioManager()
	{

	}

	void AudioManager::playSound(std::string filepath, float pitch, bool loop, float volumeModifer)
	{
		activeSound_.emplace_back();
		int x = activeSound_.size() - 1;
		activeSound_[x].setBuffer(*resourceManager_->getSoundBuffer(filepath));
		activeSound_[x].setPitch(pitch);
		activeSound_[x].setVolume(soundVolume_ * volumeModifer);
		activeSound_[x].setLoop(loop);
		activeSound_[x].play();

		
		
	}

	void AudioManager::playMusic(std::string filepath, bool loop)
	{
		music_->openFromFile(filepath);
		music_->setLoop(loop);
		music_->play();
		latestMusicPlayed_ = filepath;

	}

	void AudioManager::stopMusic()
	{
		music_->stop();
	}

	void AudioManager::pauseMusic()
	{
		music_->pause();
	}

	void AudioManager::stopAllSounds()
	{
		for (unsigned int i = 0; i < activeSound_.size(); )
		{
			activeSound_[i].stop();
			activeSound_[i] = activeSound_.back();
			activeSound_.pop_back();
		}
	}

	void AudioManager::update()
	{
		int soundHelper = 0;
		for (unsigned int i = 0; i < activeSound_.size(); i++ )
		{
			if (activeSound_[i].getStatus() != sf::Sound::Playing)
			{
				soundHelper++;
				//activeSound_[i] = activeSound_.back();
				//activeSound_.pop_back();
			}
		}

		if (soundHelper != 0 && soundHelper >= activeSound_.size())
		{
			stopAllSounds();

		}
	}

	void AudioManager::setSoundVolume(float soundVolume)
	{
		soundVolume_ = soundVolume;
	}

	void AudioManager::setMusicVolume(float musicVolume)
	{
		music_->setVolume(musicVolume);
	}
	
	float AudioManager::getSoundVolume()
	{
		return soundVolume_;
	}

	float AudioManager::getMusicVolume()
	{
		return music_->getVolume();
	}

	std::string AudioManager::getLatestMusicPlayed()
	{
		return latestMusicPlayed_;
	}
	
}