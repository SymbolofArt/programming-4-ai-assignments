#ifndef POTATOENGINE_ERRORHANDLER_H
#define POTATOENGINE_ERRORHANDLER_H
#include <string>

namespace PotatoEngine
{
	/// <summary>
	/// Shutsdown the game and print an error message.
	/// </summary>
	///<param name="errorString">Error message to print before shuting down the game.</param>
	/// <returns></returns>
	extern void fatalError(std::string errorString);
}

#endif // POTATOENGINE_ERRORHANDLER_H