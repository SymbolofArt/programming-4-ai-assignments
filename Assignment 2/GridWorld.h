#ifndef ASSIGNMENT2_GRIDWORLD_H
#define ASSIGNMENT2_GRIDWORLD_H
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string> 

#include "glm\glm.hpp"
#include <PotatoEngine/SpriteBatch.h>
#include <PotatoEngine\ResourceManager.h>
#include <PotatoEngine\Converter.h>


#include <PotatoEngine\CollisionManager.h>

#include "System.h"
#include "Tile.h"
#include "StarHunter.h"


#include <PotatoEngine\GUI.h>

class GridWorld
{
public:
	//Constructors
	GridWorld();
	GridWorld(System* system);

	//Deconstructors
	~GridWorld();


	void draw(PotatoEngine::SpriteBatch * spriteBatch);

	void drawText();

	void update(float deltaTime);

	void processInput();

private:

	//Generation
	void generateWorldData(int numberOFWalls);
	void addNeigbors();

	void moveStar(glm::vec2 newLocation);

	void setUpText();
	void updateAllText();

	//Variables
	System* system_;
	std::vector<std::shared_ptr<Tile>> sharedWorldData_;


	glm::vec2 actorGoal_;
	bool runSimulation = false;
	bool pauseSimulation = false;
	//bool automaticStarMover = false;


	StarHunter starHunter_;


	//Settings stuff
	bool settingsActorUseJPS_;

	bool settingDrawSearchData_;
	bool settingInstantFindPath_;
	bool settingWalkToTarget_;
	bool automaticStarMover;
	bool drawTextInfo_;


	std::vector<PotatoEngine::GUILabel> infoLabels_;

	

};
#endif // ASSIGNMENT2_GRIDWORLD_H


