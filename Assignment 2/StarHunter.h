#ifndef ASSIGNMENT2_STARHUNTER_H
#define ASSIGNMENT2_STARHUNTER_H

#include <PotatoEngine\ResourceManager.h>

#include "Entity.h"
#include "SimulationSettings.h"
#include "Tile.h"

#include "Windows.h"
enum Behaviour
{
	WAITING,
	WALKING,
	FINDING_WAY,
	GATHERING_MAP_DATA
};

struct DetectedTileData
{
public:
	std::shared_ptr<DetectedTileData> parent;
	glm::vec2 location;
	bool isRock;
	bool isClosed;
	bool isPath;
	int GCost; //Gcost = distance from starting nod
	int HCost; //H cost (heuristic) = distance from end node.
	int FCost; //Gcost + Hcost
	int tileID;
	int retrackChildID;

	glm::vec2 pathThingy_;

	bool searchRight;
	bool searchRightUp;
	bool searchRightDown;

	bool searchLeft;
	bool searchLeftUp;
	bool searchLeftDown;

	bool searchUp;
	bool searchDown;


	DetectedTileData(glm::vec2 loc, bool IsRock, int id)
	{
		location = loc;
		isRock = IsRock;
		isClosed = false;
		isPath = false;
		parent = nullptr;
		GCost = 0;
		HCost = 0;
		FCost = 0;
		tileID = id;

		searchRight = false;
		searchRightUp = false;
		searchRightDown = false;
		searchLeft = false;
		searchLeftUp = false;
		searchLeftDown = false;
		searchUp = false;
		searchDown = false;


	}

	///Create path
	void createPath(int RetrackChild = -1)
	{
		if (isPath)
			return;

		retrackChildID = RetrackChild;
		isPath = true;
		if (parent != nullptr)
			parent->createPath(tileID);
	}


	void generateSearchData(glm::vec2 goalLoc, std::shared_ptr<DetectedTileData> Parent, int GCOST)
	{
		/*if (parent != nullptr)
			return;*/
		parent = Parent;
		HCost = 0;
		//Get HCost_
		glm::vec2 tmpLoc = location;
		
		//Is faster at getting quick HCost, 
		//however, the pathfinding becomes slower most of the time.
		/*HCost = abs(location.x - goalLoc.x) * 10 + abs(location.y - goalLoc.y) * 10;
		GCost = GCOST;
		FCost = HCost + GCost;
		return;*/

		while (tmpLoc.x != goalLoc.x || tmpLoc.y != goalLoc.y)
		{
			//Up Right
			if (tmpLoc.x < goalLoc.x && tmpLoc.y < goalLoc.y)
			{
				tmpLoc += glm::vec2(1, 1);
				HCost += 14;
			}// Down Right
			else if (tmpLoc.x < goalLoc.x && tmpLoc.y > goalLoc.y)
			{
				tmpLoc += glm::vec2(1, -1);
				HCost += 14;
			} //Left Up
			else if (tmpLoc.x > goalLoc.x && tmpLoc.y < goalLoc.y)
			{
				tmpLoc += glm::vec2(-1, 1);
				HCost += 14;
			}//Left Down
			else if (tmpLoc.x > goalLoc.x && tmpLoc.y > goalLoc.y)
			{
				tmpLoc += glm::vec2(-1, -1);
				HCost += 14;
			}//UP
			else if (tmpLoc.y < goalLoc.y)
			{
				tmpLoc += glm::vec2(0, 1);
				HCost += 10;
			}//Down
			else if (tmpLoc.y > goalLoc.y)
			{
				tmpLoc += glm::vec2(0, -1);
				HCost += 10;
			}//Right
			else if (tmpLoc.x < goalLoc.x)
			{
				tmpLoc += glm::vec2(1, 0);
				HCost += 10;
			}//Left
			else if (tmpLoc.x > goalLoc.x)
			{
				tmpLoc += glm::vec2(-1, 0);
				HCost += 10;
			}
		}

		GCost = GCOST;
		FCost = HCost + GCost;
	}

};

class StarHunter :
	public Entity
{
public:
	//Constructors
	StarHunter();
	StarHunter(PotatoEngine::ResourceManager* resManager, glm::vec2 loc);

	//Deconstructor
	~StarHunter();


	void update() override;
	void draw(PotatoEngine::SpriteBatch * spriteBatch) override;

	void updateDetectedTileData();


	void setTarget(glm::vec2 locationOfTarget);
	void setPointerToWorldMap(std::vector<std::shared_ptr<Tile>> worldData);
	void setBehaviour(Behaviour newBehaviour);

	Behaviour getBehaviour();

	void cleanDrawData();

	bool isStuck();

	void updateSettings(bool drawSearch, bool instantFind, bool walkToTarget, bool useJPS);

	int getStarsCollected();
	void setStarsCollected(int newStarsCollected);

private:

	void searchPath();
	void checkNeighbors();
	int getGCost(glm::vec2 a, glm::vec2 b);

	void walking();

	void searchPathJPS();

	bool JPSsearchRight(std::shared_ptr<DetectedTileData> checkMe);
	bool JPSsearchLeft(std::shared_ptr<DetectedTileData> checkMe);
	bool JPSsearchUp(std::shared_ptr<DetectedTileData> checkMe);
	bool JPSsearchDown(std::shared_ptr<DetectedTileData> checkMe);



	void JSPsearchRightUp();
	void JSPsearchRightDown();

	void JSPsearchLeftUp();
	void JSPsearchLeftDown();


	bool isInOpenList(int idToCheck);
	bool isInOpenList(std::shared_ptr<DetectedTileData> checkMe);

	//Variables
	Behaviour actorBehaviour_;
	int starsCollected_;

	//Search things
	bool openListEmpty_;
	bool hasGeneredFirstCurrentTile_;
	bool isStuck_;
	glm::vec2 actorGoal_;
	std::shared_ptr<DetectedTileData> currentTile_; //Current tile we are checking.
	std::shared_ptr<DetectedTileData> playerLocationTile_; //Current tile we are checking.


	glm::vec2 JPSsearchLocerino_;


	//Tile info
	bool needToSearchPath_;
	std::vector<std::shared_ptr<DetectedTileData>> detectedTileData_;
	std::vector<std::shared_ptr<DetectedTileData>> openList_;
	std::vector<std::shared_ptr<DetectedTileData>> closedList_;
	
	//World data
	std::vector<std::shared_ptr<Tile>> sharedWorldData_;
	
	//Draw stuff
	float heX_;
	float heY_;
	sf::Texture* openTileImageTexture_;
	sf::Texture* closedTileImageTexture_;
	sf::Texture* pathTileImageTexture_;
	sf::Color tileTextureColor_;

	//Settings things.
	bool settingDrawSearchData_;
	bool settingInstantFindPath_;
	bool settingWalkToTarget_;
	bool settingUseJPS_;
};




#endif // ASSIGNMENT2_STARHUNTER_H