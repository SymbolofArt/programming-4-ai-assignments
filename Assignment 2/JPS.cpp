﻿#include "JPS.h"

Grid::Grid()
{
	/* Empty*/
}

Grid::Grid(int width, int height, std::vector<bool> gridData)
	:
	width_(width),
	height_(height)
{
	//Create all nodes.
	for (int y = 0; y < height_; y++)
		for (int x = 0; x < width_; x++)
			gridNodes_.emplace_back(x, y, gridData.at(width_ * y + x));

}

Node Grid::getNodeAt(int x, int y)
{
	return gridNodes_.at(width_ * y + x);
}

Node Grid::getNodeAt(glm::vec2 location)
{
	return getNodeAt(location.x, location.y);
}

bool Grid::isWalkableAt(int x, int y)
{
	return isInsideGrid(x, y) && gridNodes_.at(width_ * y + x).walkable;
}

bool Grid::isWalkableAt(glm::vec2 location)
{
	return isWalkableAt(location.x, location.y);
}

bool Grid::isInsideGrid(int x, int y)
{
	return (x >= 0 && x < width_) && (y >= 0 && y < height_);
}

bool Grid::isInsideGrid(glm::vec2 location)
{
	return isInsideGrid(location.x, location.y);
}

void Grid::setIsWalkableAt(int x, int y, bool isWalkable)
{
	if (isInsideGrid(x, y))
		gridNodes_.at(width_ * y + x).walkable = isWalkable;
}

void Grid::setIsWalkableAt(glm::vec2 location, bool isWalkable)
{
	setIsWalkableAt(location.x, location.y, isWalkable);
}

std::vector<Node> Grid::getNeighbors(Node node)
{
	int x = node.x;
	int y = node.y;
	std::vector<Node> neighbors;
	
	// ↑
	if (isWalkableAt(x, y - 1))
		neighbors.push_back(getNodeAt(y - 1, x));
	// →
	if (isWalkableAt(x + 1, y))
		neighbors.push_back(getNodeAt(y, x + 1));
	// ↓
	if (isWalkableAt(x, y + 1))
		neighbors.push_back(getNodeAt(y + 1, x));
	// ←
	if (isWalkableAt(x - 1, y))
		neighbors.push_back(getNodeAt(y, x - 1));
	
	//Digional
	// ↖
	if (isWalkableAt(x - 1, y - 1)) 
		neighbors.push_back(getNodeAt(y - 1, x - 1));
	// ↗
	if (isWalkableAt(x + 1, y - 1))
		neighbors.push_back(getNodeAt(y - 1, x + 1));
	// ↘
	if (isWalkableAt(x + 1, y + 1))
		neighbors.push_back(getNodeAt(y + 1, x + 1));
	// ↙
	if (isWalkableAt(x - 1, y + 1))
		neighbors.push_back(getNodeAt(y + 1, x - 1));

	//return all neighbors
	return neighbors;
}




JPS::JPS()
{
}


JPS::~JPS()
{
}


