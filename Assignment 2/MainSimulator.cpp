#include "MainSimulator.h"



MainSimulator::MainSimulator()
{
	/* Empty */
}


MainSimulator::~MainSimulator()
{
	/* Empty */
}

void MainSimulator::run()
{
	initSystems();
	
	system_ = System(
		&simulationState_,
		&window_,
		&cameraView_,
		&textureProgram_,
		&inputManager_,
		&resourceManager_,
		&audioManager_,
		&collisionManager_,
		&animationManager_,
		&gameDictionary_);
	
	//Load resources
	resourceLoader();
	
	//Set State
	//simulationState_ = SimulationStates::STATE_MENU;
	simulationState_ = SimulationStates::STATE_SIMULATING;
	switchState();

	mainLoop();
}

void MainSimulator::initSystems()
{
	//Create the main window

	int width = 1600;
	int height = 900;
	bool fullscreen = false;
	bool borderless = false;
	float musicVolume = 0;
	float sfxVolume = 0;

	sf::Uint32 winStyle;
	if (fullscreen)
	{
		winStyle = sf::Style::Fullscreen;
	}
	else if (borderless)
	{
		winStyle = sf::Style::None;
	}
	else
	{
		winStyle = sf::Style::Titlebar;
	}

	window_.create(sf::VideoMode(width, height), "A* StarHunter", winStyle);


	cameraView_.reset(sf::FloatRect(0, 0, 1920, 1080));

	// Set its target viewport to be half of the window
	cameraView_.setViewport(sf::FloatRect(0.f, 0.f, 1.0f, 1.0f));


	//Compile the non existing shaders

	//Initializing the audio manager
	audioManager_ = PotatoEngine::AudioManager(&resourceManager_);



	//Set up sound
	audioManager_.setMusicVolume(musicVolume);
	audioManager_.setSoundVolume(sfxVolume);
}

void MainSimulator::resourceLoader()
{
	resourceManager_.getTexture("resources/textures/tile_empty.png");
	resourceManager_.getTexture("resources/textures/tile_goal.png");
	resourceManager_.getTexture("resources/textures/tile_rock.png");
	resourceManager_.getTexture("resources/textures/tile_start.png");

	resourceManager_.getTexture("resources/textures/tile_AStar_closed.png");
	resourceManager_.getTexture("resources/textures/tile_AStar_open.png");
	resourceManager_.getTexture("resources/textures/tile_AStar_path.png");
}

void MainSimulator::switchState()
{
	if (currentState_ != nullptr)
	{
		delete currentState_;
	}

	switch (simulationState_)
	{
	case STATE_MENU:
		//currentState_ = new StateMenu(system_);
		break;
	case STATE_SIMULATING:
		currentState_ = new LiveState(&system_);
		break;
	}
	simulationState_ = SimulationStates::STATE_NULL;
}

void MainSimulator::processInput()
{
	//Update the input manager before we handle input data.
	inputManager_.update();
	sf::Event e;
	while (window_.pollEvent(e))
	{
		switch (e.type)
		{
		case sf::Event::Closed:
			simulationState_ = SimulationStates::STATE_EXIT;
			break;
		case sf::Event::MouseMoved:
			inputManager_.setMouseCoords(
				(int)window_.mapPixelToCoords(sf::Vector2i(e.mouseMove.x, e.mouseMove.y)).x,
				(int)window_.mapPixelToCoords(sf::Vector2i(e.mouseMove.x, e.mouseMove.y)).y);

			break;
		case sf::Event::KeyPressed:
			inputManager_.pressKey(e.key.code);
			break;
		case sf::Event::KeyReleased:
			inputManager_.releaseKey(e.key.code);
			break;
		case sf::Event::MouseButtonPressed:
			if (e.mouseButton.button == sf::Mouse::Left)
			{
				inputManager_.pressMouse(PotatoEngine::LEFT);
			}
			else if (e.mouseButton.button == sf::Mouse::Right)
			{
				inputManager_.pressMouse(PotatoEngine::RIGHT);
			}
			break;
		case sf::Event::MouseButtonReleased:
			if (e.mouseButton.button == sf::Mouse::Left)
			{
				inputManager_.releaseMouse(PotatoEngine::LEFT);
			}
			else if (e.mouseButton.button == sf::Mouse::Right)
			{
				inputManager_.releaseMouse(PotatoEngine::RIGHT);
			}
			break;
		}
	}

	if (inputManager_.isKeyPressed(sf::Keyboard::Escape))
	{
		simulationState_ = SimulationStates::STATE_EXIT;
	}

}

void MainSimulator::mainLoop()
{
	/* FPS HANDLER */
	PotatoEngine::FpsLimiter fps_limiter;
	fps_limiter.setMaxFPS(120.0f);
	//Const's used for delta time, (To make sure the game don't runt faster/slower based on the users FPS)
	const float DESIRED_FPS = 60.0f;
	const float MS_PER_SECOND = 1000.0f;
	const float DESIRED_FRAMETIME = MS_PER_SECOND / DESIRED_FPS;
	const float MAX_DELTA_TIME = 1.0f;
	const int MAX_PHYSICS_STEPS = 1;
	float previousTicks = fps_limiter.getElapsedTime();

	window_.clear(sf::Color::Black);

	/* MAIN LOOP */
	while (simulationState_ != SimulationStates::STATE_EXIT)
	{
		//Starts fps limiter.
		fps_limiter.begin();

		if (simulationState_ != SimulationStates::STATE_NULL)
		{
			switchState();
		}
		//fps handler stuff
		float newTicks = fps_limiter.getElapsedTime();
		float frameTime = newTicks - previousTicks;
		previousTicks = newTicks;
		float totalDeltaTime = frameTime / DESIRED_FRAMETIME;
		
		//Updade input
		processInput();

		//Update the audio manager
		audioManager_.update();

		//Update using delta time.
		int i = 0;
		while (totalDeltaTime > 0.0f &&
			i < MAX_PHYSICS_STEPS &&
			simulationState_ == SimulationStates::STATE_NULL
			&& window_.hasFocus())
		{

			float deltaTime = std::min(totalDeltaTime, MAX_DELTA_TIME);
			currentState_->update(deltaTime);
			totalDeltaTime -= deltaTime;
			i++;
		}


		currentState_->draw();
		//Update the view
		window_.setView(cameraView_);

		window_.display();

		//End Fps limiter
		fps_ = fps_limiter.end();
		//fps_ = 100;
		//fps_limiter.end();

		window_.setTitle(std::to_string(fps_));
	}
	window_.clear();
	window_.close();
}
