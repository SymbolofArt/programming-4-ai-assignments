#include "Tile.h"

Tile::Tile()
{
	/* Empety */
}

Tile::Tile(PotatoEngine::ResourceManager* resManager, glm::vec2 loc)
	:
	resourceManger_(resManager),
	isRock_(false),
	isGoal_(false)
{
	loc_ = loc;

	float heX = (1920 / (GRID_SIZE_WIDTH));
	float heY = (1080 / (GRID_SIZE_HEIGHT));

	//drawArea_ = sf::FloatRect(
	//	loc_.x * heX - 0.1f,
	//	loc_.y * heY - 0.1f,
	//	heX + 0.2f,
	//	heY + 0.2f);
	
	drawArea_ = sf::FloatRect(
		loc_.x * heX,
		loc_.y * heY,
		heX,
		heY);


	textureSize_ = sf::IntRect(0, 0, 16, 16);
	textureColor_ = sf::Color(255, 255, 255, 255);

	imageTexture_ = resourceManger_->getTexture("resources/textures/tile_empty.png");
}

Tile::~Tile()
{
	/* Empety */
}


void Tile::update()
{

}

void Tile::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	//Draw the grass
	spriteBatch->draw(
		imageTexture_,
		drawArea_,
		textureSize_,
		textureColor_);
}

void Tile::addNeighbor(std::shared_ptr<Tile> neighbor)
{
	neighbors_.push_back(neighbor);
}

std::vector<std::shared_ptr<Tile>> Tile::getAllNeighbors()
{
	return neighbors_;
}

bool Tile::isRock()
{
	return isRock_;
}

void Tile::setIsRock(bool isRock)
{
	if (isGoal_)
		return;

	isRock_ = isRock;
	if (isRock_)
		imageTexture_ = resourceManger_->getTexture("resources/textures/tile_rock.png");
	else
		imageTexture_ = resourceManger_->getTexture("resources/textures/tile_empty.png");
}


void Tile::setGoal(bool isGoal)
{
	isRock_ = false;
	isGoal_ = isGoal;

	if (isGoal_)
		imageTexture_ = resourceManger_->getTexture("resources/textures/tile_goal.png");
	else
		imageTexture_ = resourceManger_->getTexture("resources/textures/tile_empty.png");
}
