Hotkeys


Left Mouse Click = Place walls.
Right Mouse Click = Remove walls.

Spacebar = Pause the simulation and/or start the simulation if its paused/stopped
BackSpace = Turn the simulation off and remove all path drawing etc.
R = Reset the simulation, does like the backspace, but it also remove all rocks and set them to nothing.

Tab = Show a text window in top left that shows whatever things are enabled or disabled.

Numpad1 = Toggle whatever it shall drawn the actor search path finding.
Numpad2 = Toggle whatever the actor should finds it target instantly our do one step by one each simulation update.
Numpad3 = Toggle whatever the actor should walk to the target or not.


***** Required the simulation is turned off *****
A = Move the starhunter.
G = Move the star.
Numpad0 = Turn on automatic star spawner/mover.





//Random dev test things. (can be used whenever, but require console to see output)
Numpad9 = Show tile info about the one you are over, (id, cord, if its rock or not..)
(gets shown in console, ***WARNING*** Clears all text in the console ***WARNING*** )