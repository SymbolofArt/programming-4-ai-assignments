#include "LiveState.h"

LiveState::LiveState()
{
	/*Empty*/
}

LiveState::LiveState(System* system, float simulationSpeed)
	:
	system_(system)
{
	std::srand((int)std::time(0)); // use current time as seed for random generator

	world_ = GridWorld(system);
}

LiveState::~LiveState()
{

}

void LiveState::processInput()
{
}

void LiveState::update(float deltaTime)
{
	world_.processInput();
	simulationTimer_ += deltaTime;
	if (simulationTimer_ > SIMULATION_SPEED)
	{
		world_.update(deltaTime);
		simulationTimer_ -= SIMULATION_SPEED;
	}
}

void LiveState::draw()
{
	spriteBatch_.begin();

	//Draw the world.
	world_.draw(&spriteBatch_);
	// 1920 x 1080

	//End batch and draw it
	spriteBatch_.end();
	system_->window->draw(spriteBatch_);

	world_.drawText();
	
	
}
