#ifndef ASSIGNMENT2_JPS_H
#define ASSIGNMENT2_JPS_H
#include <glm\glm.hpp>
#include <iostream>
#include <memory>
#include <vector>
#include <functional>

struct Node
{
	Node(int X, int Y, bool Walkable) 
		: x(X),y(Y),walkable(Walkable)	{ /*Empty*/ }
	Node(glm::vec2 loc, bool Walkable)
		: x(loc.x),y(loc.y),walkable(Walkable) { /*Empty*/ }

	int x;
	int y;
	bool walkable;

	Node* parent;

};

class Grid
{
public:
	Grid();
	Grid(int width, int height, std::vector<bool> gridData);

	Node getNodeAt(int x, int y);
	Node getNodeAt(glm::vec2 location);

	bool isWalkableAt(int x, int y);
	bool isWalkableAt(glm::vec2 location);

	bool isInsideGrid(int x, int y);
	bool isInsideGrid(glm::vec2 location);

	void setIsWalkableAt(int x, int y, bool isWalkable);
	void setIsWalkableAt(glm::vec2 location, bool isWalkable);

	std::vector<Node> getNeighbors(Node node);

private:
	int width_;
	int height_;

	std::vector<Node> gridNodes_;
};



class JPS
{
public:
	JPS();
	~JPS();

private:
	Grid gridData_;

};
#endif // ASSIGNMENT2_JPS_H
