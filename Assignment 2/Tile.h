#ifndef ASSIGNMENT2_TILE_H
#define ASSIGNMENT2_TILE_H

#include <iostream>
#include <memory>
#include <vector>
#include <functional>
#include <PotatoEngine\ResourceManager.h>

#include "Entity.h"
#include "SimulationSettings.h"



class Tile : public Entity
{
public:
	//Constructors
	Tile();
	Tile(PotatoEngine::ResourceManager* resManager, glm::vec2 loc);

	//Deconstructor
	~Tile();

	void update() override;
	void draw(PotatoEngine::SpriteBatch * spriteBatch) override;

	
	void addNeighbor(std::shared_ptr<Tile> neighbor);
	std::vector<std::shared_ptr<Tile>> getAllNeighbors();

	bool isRock();
	void setIsRock(bool isRock);

	void setGoal(bool isGoal);

private:

	//Variable
	PotatoEngine::ResourceManager* resourceManger_;
	bool isRock_;
	bool isGoal_;

	//pointers to its neighbors
	std::vector<std::shared_ptr<Tile>> neighbors_;
};

#endif // ASSIGNMENT2_TILE_H