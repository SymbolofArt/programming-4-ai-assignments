#include "GridWorld.h"



GridWorld::GridWorld()
{
	//generateWorldData(1);
}

GridWorld::GridWorld(System* system)
	:
	system_(system),
	settingDrawSearchData_(ACTOR_DRAW_SEARCH_DATA),
	settingInstantFindPath_(ACTOR_INSTANT_FIND_PATH),
	settingWalkToTarget_(ACTOR_WALK_TO_TARGET),
	automaticStarMover(SIMULATE_RANDOM_STARS),
	drawTextInfo_(SHOW_SETTING_HUD),
	settingsActorUseJPS_(ACTOR_USE_JPS)
{

	starHunter_ = StarHunter(system_->resourceManager, glm::vec2(1, 1));
	starHunter_.updateSettings(settingDrawSearchData_, settingInstantFindPath_, settingWalkToTarget_, settingsActorUseJPS_);
	actorGoal_ = glm::vec2(GRID_SIZE_WIDTH - 1, GRID_SIZE_HEIGHT - 1);
	
	
	generateWorldData(WORLD_START_ROCKS);
	//Set goal
	sharedWorldData_.at
	(actorGoal_.y * GRID_SIZE_WIDTH + actorGoal_.x)->setGoal(true);



	starHunter_.setTarget(actorGoal_);
	starHunter_.setPointerToWorldMap(sharedWorldData_);
	starHunter_.setBehaviour(GATHERING_MAP_DATA);
	//runSimulation = true;

	setUpText();

}


GridWorld::~GridWorld()
{
	/* EMPTY */
}

void GridWorld::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	for (auto &tile : sharedWorldData_)
	{
		tile->draw(spriteBatch);
	}
	starHunter_.draw(spriteBatch);

	if (!drawTextInfo_)
		return;
	spriteBatch->draw(
		nullptr,
		sf::FloatRect(0,0,420,160),
		sf::IntRect(0,0,0,0),
		sf::Color(0,0,0,150));
}

void GridWorld::drawText()
{
	if (!drawTextInfo_)
		return;
	updateAllText();
	//Draw text
	for (auto &text : infoLabels_)
	{
		text.drawText(system_->window);
	}
}

void GridWorld::update(float deltaTime)
{
	if(runSimulation && !pauseSimulation)
	{
		if (automaticStarMover && (starHunter_.getLocation() == actorGoal_ || starHunter_.isStuck()))
		{
			//starHunter_.cleanDrawData();
			int randomTileID;
			bool searchingForNewStarTile = true;
			while (searchingForNewStarTile)
			{
				randomTileID = std::rand() % sharedWorldData_.size();
				if (sharedWorldData_.at(randomTileID)->isRock() == false 
					&&
					sharedWorldData_.at(randomTileID)->getLocation() != starHunter_.getLocation())
				{
					moveStar(sharedWorldData_.at(randomTileID)->getLocation());
					searchingForNewStarTile = false;
				}
			}
		}
		starHunter_.update();
	}
}

void GridWorld::processInput()
{

	if (system_->inputManager->isKeyPressed(sf::Keyboard::Space))
	{
		if (runSimulation)
		{
			pauseSimulation = !pauseSimulation;

			printf("Simulation %s\n", pauseSimulation ? "Paused" : "Resumed");
		}
		else
		{
			printf("Simulation Started\n");
			pauseSimulation = false;
			runSimulation = true;
		}
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::Tab))
	{
		drawTextInfo_ = !drawTextInfo_;
		printf("Draw text info has been: %s\n", drawTextInfo_ ? "Enabled" : "Disabled");
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::Numpad1))
	{
		settingDrawSearchData_ = !settingDrawSearchData_;
		printf("Actor draw search path has been %s\n", settingDrawSearchData_ ? "Enabled" : "Disabled");
		starHunter_.updateSettings(settingDrawSearchData_, settingInstantFindPath_, settingWalkToTarget_, settingsActorUseJPS_);
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::Numpad2))
	{
		settingInstantFindPath_ = !settingInstantFindPath_;
		printf("Actor instnat find path has been %s\n", settingInstantFindPath_ ? "Enabled" : "Disabled");
		starHunter_.updateSettings(settingDrawSearchData_, settingInstantFindPath_, settingWalkToTarget_, settingsActorUseJPS_);
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::Numpad3))
	{
		settingWalkToTarget_ = !settingWalkToTarget_;
		printf("Actor walk to target has been %s\n", settingWalkToTarget_ ? "Enabled" : "Disabled");
		starHunter_.updateSettings(settingDrawSearchData_, settingInstantFindPath_, settingWalkToTarget_, settingsActorUseJPS_);
	}

	
	std::shared_ptr<Tile> targetTile = nullptr;
	int tileIDHelper = 0;
	glm::vec2 mouseCords = system_->inputManager->getMouseCoords();
	for (auto &tile : sharedWorldData_)
	{
		if (system_->collisionManager->MouseCollision(mouseCords, tile->getEntityCollisionBox()))
		{
			targetTile = tile;
			break;
		}
		tileIDHelper++;
	}

	if (targetTile == nullptr)
		return;



	if (system_->inputManager->isKeyDown(sf::Keyboard::Numpad9))
	{
		//found the one it collides with. break and print its data.
		system("cls");
		printf("---------------\n");
		printf("Tile id: %i\n", tileIDHelper);
		printf("Tile cord: x: %i, y: %i\n", (int)targetTile->getLocation().x, (int)targetTile->getLocation().y);

		printf("Is Rock: %s\n", targetTile->isRock() ? "true" : "false");
		printf("---------------\n");
	}

	//spawn rocks
	if (system_->inputManager->isMouseDown(PotatoEngine::LEFT))
	{
		targetTile->setIsRock(true);	
	}

	//Remove rocks.
	if (system_->inputManager->isMouseDown(PotatoEngine::RIGHT))
	{
		targetTile->setIsRock(false);
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::BackSpace))
	{
		if (runSimulation)
		{
			runSimulation = false;
			printf("Simulation Stoped\n");
			
			//Reset draw data.
			starHunter_.cleanDrawData();
		}
		starHunter_.setStarsCollected(0);
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::R))
	{
		//Reset
		if (runSimulation)
		{
			printf("Simulation Stoped\n");
			runSimulation = false;
			starHunter_.cleanDrawData();
		}

		for (auto &tile : sharedWorldData_)
		{
			tile->setIsRock(false);
		}
		starHunter_.setStarsCollected(0);
		printf("Map Cleared\n");
	}
	
	if (runSimulation == true)
		return;

	if (system_->inputManager->isKeyPressed(sf::Keyboard::A))
	{
		starHunter_.setLocation(targetTile->getLocation());
		printf("Starhunter location has been moved to: x:%i - y:%i\n", 
			(int)targetTile->getLocation().x, (int)targetTile->getLocation().y);
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::G))
	{
		for (auto &tile : sharedWorldData_)
		{
			if (tile->getLocation() == actorGoal_)
			{
				tile->setGoal(false);
				break;
			}
		}

		//Change actor goal
		actorGoal_ = targetTile->getLocation();
		targetTile->setGoal(true);
		starHunter_.setTarget(actorGoal_);

		printf("Star location has been moved to: x:%i - y:%i\n",
			(int)targetTile->getLocation().x, (int)targetTile->getLocation().y);
	}

	if (system_->inputManager->isKeyPressed(sf::Keyboard::Numpad0))
	{
		automaticStarMover = !automaticStarMover;

		printf("Automatic Star Move has been: %s\n", automaticStarMover ? "Enabled" : "Disabled");
		updateAllText();
	}
	
	if (system_->inputManager->isKeyPressed(sf::Keyboard::Numpad4))
	{
		settingsActorUseJPS_ = !settingsActorUseJPS_;
		printf("JPS has been %s\n", settingsActorUseJPS_ ? "Enabled" : "Disabled");
		starHunter_.updateSettings(settingDrawSearchData_, settingInstantFindPath_, settingWalkToTarget_, settingsActorUseJPS_);
	}

}

void GridWorld::generateWorldData(int numberOFWalls)
{
	//Create the world
	for (int y = 0; y < GRID_SIZE_HEIGHT; y++)
	{
		for (int x = 0; x < GRID_SIZE_WIDTH; x++)
		{
			sharedWorldData_.emplace_back(std::make_shared<Tile>(
				system_->resourceManager,glm::vec2(x, y)));		
		}
	}
	//Create rocks
	for (int i = 0; i < WORLD_START_ROCKS; i++)
	{
		int randIndex = std::rand() % sharedWorldData_.size();

		if (sharedWorldData_.at(randIndex)->getLocation() != starHunter_.getLocation()
			&&
			sharedWorldData_.at(randIndex)->getLocation() != actorGoal_
			&&
			sharedWorldData_.at(randIndex)->isRock() == false)
		{
			sharedWorldData_.at(randIndex)->setIsRock(true);
		}
	}

	addNeigbors();


}

void GridWorld::addNeigbors()
{
	for (int i = 0; i < sharedWorldData_.size(); i++)
	{
		if (sharedWorldData_.at(i)->getLocation().x < GRID_SIZE_WIDTH - 1)	//Right
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + 1));

			if (sharedWorldData_.at(i)->getLocation().y < GRID_SIZE_HEIGHT - 1)	//Bottom Right
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + GRID_SIZE_WIDTH + 1));
			}

			if (sharedWorldData_.at(i)->getLocation().y > 0)		//Top Right
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - GRID_SIZE_WIDTH + 1));
			}
		}

		if (sharedWorldData_.at(i)->getLocation().x > 0)	//Left
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - 1));

			if (sharedWorldData_.at(i)->getLocation().y < GRID_SIZE_HEIGHT - 1)	//Bottom Left
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + GRID_SIZE_WIDTH - 1));
			}

			if (sharedWorldData_.at(i)->getLocation().y > 0)		//Top Left
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - GRID_SIZE_WIDTH - 1));
			}
		}

		if (sharedWorldData_.at(i)->getLocation().y < GRID_SIZE_HEIGHT - 1)	//Bottom
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + GRID_SIZE_WIDTH));
		}

		if (sharedWorldData_.at(i)->getLocation().y > 0)		//Top
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - GRID_SIZE_WIDTH));
		}


	}
}

void GridWorld::moveStar(glm::vec2 newLocation)
{
	std::shared_ptr<Tile> newStarTile = nullptr;

	bool oldStarFound = false;
	bool newStarFound = false;

	for (auto &tile : sharedWorldData_)
	{
		if (tile->getLocation() == actorGoal_)
		{
			tile->setGoal(false);
			oldStarFound = true;
		}
		if (tile->getLocation() == newLocation)
		{
			newStarFound = true;
			newStarTile = tile;
		}
	}


	actorGoal_ = newLocation;
	newStarTile->setGoal(true);
	starHunter_.setTarget(actorGoal_);
}

void GridWorld::setUpText()
{
	infoLabels_.emplace_back(PotatoEngine::GUILabel(
		system_->resourceManager,
		glm::vec2(10, 10),
		"(Numpad 0) SIMULATE RANDOM STARS:",
		"resources/fonts/StardosStencil-Regular.ttf",
		16
	));
	if (automaticStarMover)
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 10),
			"ENABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(1).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 10),
			"DISABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(1).setColor(sf::Color(255, 0, 0, 255));
	}


	infoLabels_.emplace_back(PotatoEngine::GUILabel(
		system_->resourceManager,
		glm::vec2(10, 30),
		"(Numpad 1) ACTOR DRAW SEARCH DATA:",
		"resources/fonts/StardosStencil-Regular.ttf",
		16
	));
	if (settingDrawSearchData_)
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 30),
			"ENABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(3).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 30),
			"DISABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(3).setColor(sf::Color(255, 0, 0, 255));
	}

	infoLabels_.emplace_back(PotatoEngine::GUILabel(
		system_->resourceManager,
		glm::vec2(10, 50),
		"(Numpad 2) ACTOR INSTANT FIND PATH:",
		"resources/fonts/StardosStencil-Regular.ttf",
		16
	));
	if (settingInstantFindPath_)
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 50),
			"ENABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(5).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 50),
			"DISABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(5).setColor(sf::Color(255, 0, 0, 255));
	}


	infoLabels_.emplace_back(PotatoEngine::GUILabel(
		system_->resourceManager,
		glm::vec2(10, 70),
		"(Numpad 3) ACTOR WALK TO TARGET:",
		"resources/fonts/StardosStencil-Regular.ttf",
		16
	));

	if (settingWalkToTarget_)
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 70),
			"ENABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(7).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 70),
			"DISABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(7).setColor(sf::Color(255, 0, 0, 255));
	}

	infoLabels_.emplace_back(PotatoEngine::GUILabel(
		system_->resourceManager,
		glm::vec2(10, 90),
		"(Numpad 4) ACTOR USE JPS:",
		"resources/fonts/StardosStencil-Regular.ttf",
		16
	));

	if (settingsActorUseJPS_)
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 90),
			"ENABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(7).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.emplace_back(PotatoEngine::GUILabel(
			system_->resourceManager,
			glm::vec2(320, 90),
			"DISABLED",
			"resources/fonts/StardosStencil-Regular.ttf",
			16
		));
		infoLabels_.at(7).setColor(sf::Color(255, 0, 0, 255));
	}


	infoLabels_.emplace_back(PotatoEngine::GUILabel(
		system_->resourceManager,
		glm::vec2(10, 110),
		"Total starts collected: ",
		"resources/fonts/StardosStencil-Regular.ttf",
		16
	));

	infoLabels_.emplace_back(PotatoEngine::GUILabel(
		system_->resourceManager,
		glm::vec2(170, 110),
		"0",
		"resources/fonts/StardosStencil-Regular.ttf",
		16
	));
}

void GridWorld::updateAllText()
{
	if (automaticStarMover)
	{
		infoLabels_.at(1).setText("ENABLED");
		infoLabels_.at(1).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.at(1).setText("DISABLED");
		infoLabels_.at(1).setColor(sf::Color(255, 0, 0, 255));
	}

	if (settingDrawSearchData_)
	{
		infoLabels_.at(3).setText("ENABLED");
		infoLabels_.at(3).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.at(3).setText("DISABLED");
		infoLabels_.at(3).setColor(sf::Color(255, 0, 0, 255));
	}
	
	if (settingInstantFindPath_)
	{
		infoLabels_.at(5).setText("ENABLED");
		infoLabels_.at(5).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.at(5).setText("DISABLED");
		infoLabels_.at(5).setColor(sf::Color(255, 0, 0, 255));
	}
	
	if (settingWalkToTarget_)
	{
		infoLabels_.at(7).setText("ENABLED");
		infoLabels_.at(7).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.at(7).setText("DISABLED");
		infoLabels_.at(7).setColor(sf::Color(255, 0, 0, 255));
	}
	
	if (settingsActorUseJPS_)
	{
		infoLabels_.at(9).setText("ENABLED");
		infoLabels_.at(9).setColor(sf::Color(0, 255, 0, 255));
	}
	else
	{
		infoLabels_.at(9).setText("DISABLED");
		infoLabels_.at(9).setColor(sf::Color(255, 0, 0, 255));
	}

	infoLabels_.at(11).setText(std::to_string(starHunter_.getStarsCollected()));

}


