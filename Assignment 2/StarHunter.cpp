#include "StarHunter.h"



StarHunter::StarHunter()
{
}

StarHunter::StarHunter(PotatoEngine::ResourceManager * resManager, glm::vec2 loc)
{
	actorBehaviour_ = WAITING;
	openListEmpty_ = false;
	hasGeneredFirstCurrentTile_ = false;
	isStuck_ = false;
	loc_ = loc;
	starsCollected_ = 0;

	heX_ = (1920 / (GRID_SIZE_WIDTH));
	heY_ = (1080 / (GRID_SIZE_HEIGHT));



	textureSize_ = sf::IntRect(0, 0, 16, 16);
	textureColor_ = sf::Color(255, 255, 255, 255);
	tileTextureColor_ = sf::Color(255, 255, 255, 255);

	imageTexture_ = resManager->getTexture("resources/textures/tile_start.png");

	openTileImageTexture_ = resManager->getTexture("resources/textures/tile_Astar_open.png");
	closedTileImageTexture_ = resManager->getTexture("resources/textures/tile_Astar_closed.png");
	pathTileImageTexture_ = resManager->getTexture("resources/textures/tile_Astar_path.png");
}


StarHunter::~StarHunter()
{
}

void StarHunter::update()
{
	if (actorBehaviour_ == WAITING)
	{
		/* Just waiting around, it a lonly day, lony lifeee.*/
		if (loc_ != actorGoal_)
			actorBehaviour_ = GATHERING_MAP_DATA;
	}
	else if (actorBehaviour_ == WALKING)
	{
		walking();
	}
	else if (actorBehaviour_ == FINDING_WAY)
	{
		if (settingUseJPS_)
		{
			searchPathJPS();
		}
		else
			searchPath();
		/*long int before = GetTickCount();
		if (settingInstantFindPath_)
		{
			searchPath();
		}
		else
		searchPathJPS();
		long int after = GetTickCount();
		printf("Searchpath time: %i\n", after-before);*/
	}
	else if (actorBehaviour_ == GATHERING_MAP_DATA)
	{
		updateDetectedTileData();
	}
}

void StarHunter::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	//Draw player
	drawArea_ = sf::FloatRect(
		loc_.x * heX_,
		loc_.y * heY_,
		heX_,
		heY_);

	spriteBatch->draw(
		imageTexture_,
		drawArea_,
		textureSize_,
		textureColor_);
	
	if (settingDrawSearchData_  == false)
		return;

	//Draw open list
	for (auto &tile : openList_)
	{
		drawArea_ = sf::FloatRect(
			tile->location.x * heX_,
			tile->location.y * heY_,
			heX_,
			heY_);
		
		if (tile->isPath)
			spriteBatch->draw(
				pathTileImageTexture_,
				drawArea_,
				textureSize_,
				tileTextureColor_);
		else
		spriteBatch->draw(
			openTileImageTexture_,
			drawArea_,
			textureSize_,
			tileTextureColor_);
	}

	//Draw closed list
	for (auto &tile : closedList_)
	{
		if (tile->location == loc_ || tile->location == actorGoal_)
			continue;
		drawArea_ = sf::FloatRect(
			tile->location.x * heX_,
			tile->location.y * heY_,
			heX_,
			heY_);

		if (tile->isPath)
			spriteBatch->draw(
				pathTileImageTexture_,
				drawArea_,
				textureSize_,
				tileTextureColor_);
		else
			spriteBatch->draw(
				closedTileImageTexture_,
				drawArea_,
				textureSize_,
				tileTextureColor_);
	}

}

void StarHunter::updateDetectedTileData()
{
	detectedTileData_.clear();
	openList_.clear();
	closedList_.clear();
	needToSearchPath_ = true;
	hasGeneredFirstCurrentTile_ = false;
	isStuck_ = false;
	openListEmpty_ = false;

	int indexIdHelper = 0;
	for (auto &tile : sharedWorldData_)
	{
		detectedTileData_.emplace_back(
			std::make_shared<DetectedTileData>
			(tile->getLocation(), tile->isRock(),indexIdHelper));
		indexIdHelper++;
	}

	actorBehaviour_ = FINDING_WAY;
}

void StarHunter::setTarget(glm::vec2 locationOfTarget)
{
	actorGoal_ = locationOfTarget;
}

void StarHunter::setPointerToWorldMap(std::vector<std::shared_ptr<Tile>> worldData)
{
	sharedWorldData_ = worldData;
}

void StarHunter::setBehaviour(Behaviour newBehaviour)
{
	actorBehaviour_ = newBehaviour;
}

Behaviour StarHunter::getBehaviour()
{
	return actorBehaviour_;
}

void StarHunter::cleanDrawData()
{
	detectedTileData_.clear();
	openList_.clear();
	closedList_.clear();
	actorBehaviour_ = GATHERING_MAP_DATA;
}

bool StarHunter::isStuck()
{
	return isStuck_;
}

void StarHunter::updateSettings(bool drawSearch, bool instantFind, bool walkToTarget, bool useJPS)
{
	settingDrawSearchData_ = drawSearch;
	settingInstantFindPath_ = instantFind;
	settingWalkToTarget_ = walkToTarget;
	settingUseJPS_ = useJPS;
}

int StarHunter::getStarsCollected()
{
	return starsCollected_;
}

void StarHunter::setStarsCollected(int newStarsCollected)
{
	starsCollected_ = newStarsCollected;
}

void StarHunter::searchPath()
{
	//If its the first check, we manually add it to the currentTile thing. ay
	if (!hasGeneredFirstCurrentTile_)
	{
		hasGeneredFirstCurrentTile_ = true;
		for (auto &tile : detectedTileData_)
		{
			if (tile->location == loc_)
			{
				currentTile_ = tile;
				playerLocationTile_ = tile;
			}
		}
		currentTile_->generateSearchData(actorGoal_, nullptr, 0);
		currentTile_->isClosed = true;
	}

	if (currentTile_->location == actorGoal_)
	{
		currentTile_->createPath();
		actorBehaviour_ = WALKING;
		return;
	}

	if (openListEmpty_)
	{
		isStuck_ = true;
		actorBehaviour_ = GATHERING_MAP_DATA;
		return;
	}



	//Check if there is new neighbors to add or update.
	checkNeighbors();

	//After going true all, go true all in openList and check for wish has lowest f value.
	std::shared_ptr<DetectedTileData> currentLowest = nullptr;
	int clID = 0;
	int helper = 0;
	bool overrideFirstCheck = true;
	
	//Get the cloest one.
	for (auto &openTile : openList_)
	{
		if (overrideFirstCheck)
		{
			currentLowest = openTile;
			overrideFirstCheck = false;
		}
		else
		{

			if (openTile->FCost < currentLowest->FCost
				|| //OR
				(openTile->FCost == currentLowest->FCost
					&& //AND
					openTile->HCost < currentLowest->HCost
					))
			{
				currentLowest = openTile;
				clID = helper;
			}
		}

		helper++;
	}

	if (openList_.size() == 0 || currentLowest == nullptr)
	{
		openListEmpty_ = true;
		return;
	}
	//Remove the new one to check from the open list.
	openList_.at(clID) = nullptr;
	int sizeBeforeErase = (int)openList_.size();
	
	openList_.erase(openList_.begin() + clID);
	/*openList_.erase(
		std::remove(
			openList_.begin(),
			openList_.end(),
			openList_.at(clID)),
		openList_.end());*/

	currentTile_ = currentLowest;
	currentTile_->isClosed = true;
	closedList_.push_back(currentTile_);

	if (settingInstantFindPath_)
		searchPath();
}


void StarHunter::checkNeighbors()
{
	//Get neighbors.
	std::vector<std::shared_ptr<DetectedTileData>> neighbors;
	neighbors.clear();
	//Check and add all neighbors to the neighbors list.
	if (currentTile_->location.x < GRID_SIZE_WIDTH - 1)			//Right
	{
		neighbors.push_back(
			detectedTileData_.at
		(currentTile_->tileID + 1));

		if (currentTile_->location.y < GRID_SIZE_HEIGHT - 1)	//Bottom Right
			neighbors.push_back(detectedTileData_.at
			(currentTile_->tileID + GRID_SIZE_WIDTH + 1));
		if (currentTile_->location.y > 0)						//Top Right
			neighbors.push_back(detectedTileData_.at
			(currentTile_->tileID - GRID_SIZE_WIDTH + 1));
	}

	if (currentTile_->location.x > 0)							//Left
	{
		neighbors.push_back(detectedTileData_.at
		(currentTile_->tileID - 1));
		if (currentTile_->location.y < GRID_SIZE_HEIGHT - 1)	//Bottom Left
			neighbors.push_back(detectedTileData_.at
			(currentTile_->tileID + GRID_SIZE_WIDTH - 1));
		if (currentTile_->location.y > 0)						//Top Left
			neighbors.push_back(detectedTileData_.at
			(currentTile_->tileID - GRID_SIZE_WIDTH - 1));
	}

	if (currentTile_->location.y < GRID_SIZE_HEIGHT - 1)		//Bottom
		neighbors.push_back(detectedTileData_.at
		(currentTile_->tileID + GRID_SIZE_WIDTH));
	if (currentTile_->location.y > 0)							//Top
		neighbors.push_back(detectedTileData_.at
		(currentTile_->tileID - GRID_SIZE_WIDTH));

	//Variables to help and check stuff.
	bool skipMe = false;
	int newGCost = 0;

	//Go true all of its neighbors.
	for (auto &neighborTile : neighbors)
	{
		//If its a rock or is closed, just continue and dont check this one.
		if (neighborTile->isRock)// || neighborTile->isClosed)
			continue;

		if (neighborTile->isClosed)
		{
			int currentF = neighborTile->FCost;
			newGCost = currentTile_->GCost
				+ getGCost(currentTile_->location, neighborTile->location);

			int newFCost = neighborTile->HCost + newGCost;
			if (newFCost < currentF)
			{
				neighborTile->generateSearchData(actorGoal_, currentTile_, newGCost);
			}
			continue;
		}

		//Check if its in the open list, if yes we need to calculate and see if its a better GCost.
		bool IsInOpenList = false;

		for (auto &openTile : openList_)
		{
			if (openTile == neighborTile)
			{
				//Get the tiles current fcost
				int currentF = neighborTile->FCost;
				//Get what it would be for f cost if it changes parent.

				//Get new GCost
				newGCost = currentTile_->GCost
					+ getGCost(currentTile_->location, neighborTile->location);

				int newFCost = neighborTile->HCost + newGCost;

				//check if its better! if yes, generate new neighbor search data!
				if (newFCost < currentF)
				{
					neighborTile->generateSearchData(actorGoal_, currentTile_, newGCost);
				}

				IsInOpenList = true;
				break;
			}
		}

		if (IsInOpenList)
			continue;
		
		//If it was not found in the closed or open list, add it to the openlist.
		openList_.push_back(neighborTile);
		newGCost = currentTile_->GCost;
		newGCost += getGCost(currentTile_->location, neighborTile->location);
		neighborTile->generateSearchData(actorGoal_, currentTile_, newGCost);
		

	}

}

int StarHunter::getGCost(glm::vec2 a, glm::vec2 b)
{
	//Check if its a corner, if yes, its 14, else 10. : )
	if (a.x != b.x && a.y != b.y)
	{
		return 14;
	}
	else
	{
		return 10;
	}
}

void StarHunter::walking()
{
	if(settingWalkToTarget_ == false)
	{
		return;
	}

	if (playerLocationTile_->retrackChildID < 0)
	{
		actorBehaviour_ = WAITING;
		starsCollected_++;
	}
	else
	{
		playerLocationTile_ = detectedTileData_.at(playerLocationTile_->retrackChildID);
		
		for (auto &tile : sharedWorldData_)
		{
			if (tile->getLocation() == playerLocationTile_->location)
			{
				if (tile->isRock())
				{
					actorBehaviour_ = GATHERING_MAP_DATA;
				}
				else
				{
					loc_ = playerLocationTile_->location;
				}
				break;
			}
		}
	}

	//playerLocationTile;
}


//********** JPS ************////

void StarHunter::searchPathJPS()
{
	if (!hasGeneredFirstCurrentTile_)
	{
		hasGeneredFirstCurrentTile_ = true;
		for (auto &tile : detectedTileData_)
		{
			if (tile->location == loc_)
			{
				currentTile_ = tile;
				playerLocationTile_ = tile;
				break;
			}
		}
		currentTile_->generateSearchData(actorGoal_, nullptr, 0);
		currentTile_->isClosed = true;
		//right
		if (currentTile_->location.x < (GRID_SIZE_WIDTH - 1))
			currentTile_->searchRight = !detectedTileData_.at(currentTile_->tileID + 1)->isRock;
		//left
		if (currentTile_->location.x > 0)
			currentTile_->searchLeft = !detectedTileData_.at(currentTile_->tileID - 1)->isRock;
		//down
		if (currentTile_->location.y < (GRID_SIZE_HEIGHT - 1))
			currentTile_->searchDown = !detectedTileData_.at(currentTile_->tileID + GRID_SIZE_WIDTH)->isRock;	
		//up
		if (currentTile_->location.y > 0)
			currentTile_->searchUp = !detectedTileData_.at(currentTile_->tileID - GRID_SIZE_WIDTH)->isRock;	
		//right up
		if (currentTile_->location.x < (GRID_SIZE_WIDTH - 1)
			&& (currentTile_->location.y > 0))
			currentTile_->searchRightUp = !detectedTileData_.at(currentTile_->tileID - GRID_SIZE_WIDTH + 1)->isRock;
		//right down
		if (currentTile_->location.x < (GRID_SIZE_WIDTH - 1)
			&& (currentTile_->location.y < (GRID_SIZE_HEIGHT - 1)))
			currentTile_->searchRightDown = !detectedTileData_.at(currentTile_->tileID + GRID_SIZE_WIDTH + 1)->isRock;
		//left up
		if (currentTile_->location.x > 0
			&& (currentTile_->location.y > 0))
			currentTile_->searchLeftUp = !detectedTileData_.at(currentTile_->tileID - GRID_SIZE_WIDTH - 1)->isRock;
		//left down
		if (currentTile_->location.x > 0
			&& (currentTile_->location.y < (GRID_SIZE_HEIGHT - 1)))
			currentTile_->searchLeftDown = !detectedTileData_.at(currentTile_->tileID + GRID_SIZE_WIDTH - 1)->isRock;


	}

	if (currentTile_->location == actorGoal_)
	{
		currentTile_->createPath();
		actorBehaviour_ = WALKING;
		return;
	}

	if (openListEmpty_)
	{
		for (auto &tile : closedList_)
		{
			if (tile->location == actorGoal_)
			{
				currentTile_->createPath();
				actorBehaviour_ = WALKING;
				return;
			}
		}

		actorBehaviour_ = WAITING;
		return;
		isStuck_ = true;
		actorBehaviour_ = GATHERING_MAP_DATA;
		return;
	}

	JSPsearchRightUp();
	JSPsearchRightDown();
	JSPsearchLeftUp();
	JSPsearchLeftDown();

	JPSsearchRight(currentTile_);
	JPSsearchLeft(currentTile_);
	JPSsearchUp(currentTile_);
	JPSsearchDown(currentTile_);

	//After going true all, go true all in openList and check for wish has lowest f value.
	std::shared_ptr<DetectedTileData> currentLowest = nullptr;
	int clID = 0;
	int helper = 0;
	bool overrideFirstCheck = true;

	//Get the cloest one.
	for (auto &openTile : openList_)
	{
		if (overrideFirstCheck)
		{
			currentLowest = openTile;
			overrideFirstCheck = false;
		}
		else
		{

			if (openTile->FCost < currentLowest->FCost
				|| //OR
				(openTile->FCost == currentLowest->FCost
					&& //AND
					openTile->HCost < currentLowest->HCost
					))
			{
				currentLowest = openTile;
				clID = helper;
			}
		}

		helper++;
	}

	if (openList_.size() == 0 || currentLowest == nullptr)
	{
		openListEmpty_ = true;
		return;
	}


	//Remove the new one to check from the open list.
	openList_.at(clID) = nullptr;
	int sizeBeforeErase = (int)openList_.size();
	openList_.erase(
		std::remove(
			openList_.begin(),
			openList_.end(),
			openList_.at(clID)),
		openList_.end());

	currentTile_ = currentLowest;
	currentTile_->isClosed = true;
	closedList_.push_back(currentTile_);
	
	if (settingInstantFindPath_)
		searchPathJPS();

}

bool StarHunter::JPSsearchRight(std::shared_ptr<DetectedTileData> checkMe)
{
	bool foundCheckPoint = false;

	int idHelper = checkMe->tileID;
	bool blockUnder = false;
	bool blockOver = false;
	bool rockFront = false;

	int GCostHelper = 0;

	int minID = checkMe->tileID - checkMe->location.x;
	int maxID = minID + GRID_SIZE_WIDTH - 1;

	while (checkMe->searchRight)
	{
		GCostHelper += 10;
		idHelper++;
		if (idHelper > maxID || idHelper < minID)
		{
			checkMe->searchRight = false;
			break;
		}
		if (detectedTileData_.at(idHelper)->isClosed)
		{
			checkMe->searchRight = false;
			break;
		}

		if (idHelper > GRID_SIZE_WIDTH)
		{
			//Check over
			if (detectedTileData_.at(idHelper - GRID_SIZE_WIDTH)->isRock &&
				!detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1)->isRock)
			{
				blockOver = true;
			}
		}
		if (idHelper < (GRID_SIZE - GRID_SIZE_WIDTH))
		{
			//Check under
			if (detectedTileData_.at(idHelper + GRID_SIZE_WIDTH)->isRock &&
				!detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1)->isRock)
			{
				blockUnder = true;
			}
		}

		if (idHelper < maxID)
		{
			//Check front
			if (detectedTileData_.at(idHelper + 1)->isRock)
			{
				rockFront = true;
			}
		}


		if (blockUnder || blockOver || detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;
					
					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper - 1), GCostHelper);
				}
			}

			if (wasFound == false)
			{
				detectedTileData_.at(idHelper)->searchRight = !rockFront;
				detectedTileData_.at(idHelper)->searchRightUp = blockOver;
				detectedTileData_.at(idHelper)->searchRightDown = blockUnder;
				
				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper - 1), GCostHelper);
				
				openList_.push_back(detectedTileData_.at(idHelper));
			}



			checkMe->searchRight = false;
			foundCheckPoint = true;

		}
		else
		{
			if (rockFront)
				checkMe->searchRight = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper - 1);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}
	}

	return foundCheckPoint;
}

bool StarHunter::JPSsearchLeft(std::shared_ptr<DetectedTileData> checkMe)
{
	bool foundCheckPoint = false;
	int idHelper = checkMe->tileID;
	bool blockUnder = false;
	bool blockOver = false;
	bool rockFront = false;

	int GCostHelper = 0;

	int minID = checkMe->tileID - checkMe->location.x;
	int maxID = minID + GRID_SIZE_WIDTH - 1;

	while (checkMe->searchLeft)
	{
		GCostHelper += 10;
		idHelper--;
		if (idHelper > maxID || idHelper < minID)
		{
			checkMe->searchLeft = false;
			break;
		}
		if (detectedTileData_.at(idHelper)->isClosed)
		{
			checkMe->searchLeft = false;
			break;
		}


		if (idHelper > GRID_SIZE_WIDTH)
		{
			//Check over
			if (detectedTileData_.at(idHelper - GRID_SIZE_WIDTH)->isRock &&
				!detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1)->isRock)
			{
				blockOver = true;
			}
		}
		if (idHelper < (GRID_SIZE - GRID_SIZE_WIDTH))
		{
			//Check under
			if (detectedTileData_.at(idHelper + GRID_SIZE_WIDTH)->isRock &&
				!detectedTileData_.at(idHelper + GRID_SIZE_WIDTH - 1)->isRock)
			{
				blockUnder = true;
			}
		}

		if (idHelper > minID)
		{
			//Check front
			if (detectedTileData_.at(idHelper - 1)->isRock)
			{
				rockFront = true;
			}
		}


		if (blockUnder || blockOver || detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;

					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper + 1), GCostHelper);
				}
			}
			if (wasFound == false)
			{
				detectedTileData_.at(idHelper)->searchLeft = !rockFront;
				detectedTileData_.at(idHelper)->searchLeftUp = blockOver;
				detectedTileData_.at(idHelper)->searchLeftDown = blockUnder;

				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper + 1), GCostHelper);

				openList_.push_back(detectedTileData_.at(idHelper));
			}
			checkMe->searchLeft = false;
			foundCheckPoint = true;
		}
		else
		{
			if (rockFront)
				checkMe->searchLeft = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper + 1);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}
	}
	return foundCheckPoint;
}

bool StarHunter::JPSsearchUp(std::shared_ptr<DetectedTileData> checkMe)
{
	bool foundCheckPoint = false;
	int idHelper = checkMe->tileID;
	bool blockLeft = false;
	bool blockRight = false;
	bool rockFront = false;

	int GCostHelper = 0;

	int minID = 0;
	int maxID = GRID_SIZE - 1;

	while (checkMe->searchUp)
	{

		if (idHelper > maxID || idHelper < minID)
		{
			checkMe->searchUp = false;
			break;
		}

		if (detectedTileData_.at(idHelper)->isClosed)
		{
			checkMe->searchUp = false;
			break;
		}

		if ((idHelper - GRID_SIZE_WIDTH) > minID)
		{
			//Check front
			if (detectedTileData_.at(idHelper - GRID_SIZE_WIDTH)->isRock)
			{
				rockFront = true;
			}

			//Check left
			if (detectedTileData_.at(idHelper)->location.x > 0)
			{
				if (detectedTileData_.at(idHelper - 1)->isRock &&
					!detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1)->isRock)
				{
					blockLeft = true;
				}
			}

			//Check right
			if (detectedTileData_.at(idHelper)->location.x < GRID_SIZE_WIDTH)
			{
				if (detectedTileData_.at(idHelper + 1)->isRock &&
					!detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1)->isRock)
				{
					blockRight = true;
				}
			}

		}

		if (blockLeft || blockRight || detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;

					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper + GRID_SIZE_WIDTH), GCostHelper);
				}
			}

			if (wasFound == false)
			{
				detectedTileData_.at(idHelper)->searchUp = !rockFront;
				detectedTileData_.at(idHelper)->searchLeftUp = blockLeft;
				detectedTileData_.at(idHelper)->searchRightUp = blockRight;

				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper + GRID_SIZE_WIDTH), GCostHelper);

				openList_.push_back(detectedTileData_.at(idHelper));
			}
			checkMe->searchUp = false;
			foundCheckPoint = true;
		}
		else
		{
			if (rockFront)
				checkMe->searchUp = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper + GRID_SIZE_WIDTH);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}

		GCostHelper += 10;
		idHelper -= GRID_SIZE_WIDTH;
	}
	return foundCheckPoint;
}

bool StarHunter::JPSsearchDown(std::shared_ptr<DetectedTileData> checkMe)
{
	bool foundCheckPoint = false;
	int idHelper = checkMe->tileID;
	bool blockLeft = false;
	bool blockRight = false;
	bool rockFront = false;

	int GCostHelper = 0;

	int minID = 0;
	int maxID = GRID_SIZE-1;

	while (checkMe->searchDown)
	{

		if (idHelper > maxID || idHelper < minID)
		{
			checkMe->searchDown = false;
			break;
		}
		if (detectedTileData_.at(idHelper)->isClosed)
		{
			checkMe->searchDown = false;
			break;
		}

		if ((idHelper + GRID_SIZE_WIDTH) < maxID)
		{
			//Check front
			if (detectedTileData_.at(idHelper + GRID_SIZE_WIDTH)->isRock)
			{
				rockFront = true;
			}

			//Check left
			if (detectedTileData_.at(idHelper)->location.x > 0)
			{
				if (detectedTileData_.at(idHelper - 1)->isRock 
					&& !detectedTileData_.at(idHelper + GRID_SIZE_WIDTH - 1)->isRock)
				{
					blockLeft = true;
				}
			}

			//Check right
			if (detectedTileData_.at(idHelper)->location.x < GRID_SIZE_WIDTH)
			{
				if (detectedTileData_.at(idHelper + 1)->isRock &&
					!detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1)->isRock)
				{
					blockRight = true;
				}
			}

		}


		if (blockLeft || blockRight || detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;

					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper - GRID_SIZE_WIDTH), GCostHelper);
				}
			}

			if (wasFound == false)
			{
 				detectedTileData_.at(idHelper)->searchDown = !rockFront;
				detectedTileData_.at(idHelper)->searchLeftDown = blockLeft;
				detectedTileData_.at(idHelper)->searchRightDown = blockRight;

				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper - GRID_SIZE_WIDTH), GCostHelper);

				openList_.push_back(detectedTileData_.at(idHelper));
			}
			checkMe->searchDown = false;
			foundCheckPoint = true;
		}
		else
		{
			if (rockFront)
				checkMe->searchDown = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper - GRID_SIZE_WIDTH);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}

		GCostHelper += 10;
		idHelper += GRID_SIZE_WIDTH;
	}
	return foundCheckPoint;
}

void StarHunter::JSPsearchRightUp()
{
	int idHelper = currentTile_->tileID;
	bool rockFront = false;
	bool rockUnder = false;


	bool rockLeft = false;


	bool thingFoundUp = false;
	bool thingFoundRight = false;

	int GCostHelper = 0;

	int minID = 0;
	int maxID = GRID_SIZE - 1;

	while (currentTile_->searchRightUp)
	{
		if (detectedTileData_.at(idHelper)->location.x >= GRID_SIZE_WIDTH-1
			||
			detectedTileData_.at(idHelper)->location.y <= 0)
		{
			rockFront = true;
			currentTile_->searchRightUp = false;
			break;
		}

		GCostHelper += 14;
		idHelper = idHelper - GRID_SIZE_WIDTH + 1;

		if (idHelper > maxID || idHelper < minID)
		{
			rockFront = true;
			currentTile_->searchRightUp = false;
			break;
		}
		
		if (detectedTileData_.at(idHelper)->isClosed)
		{
			rockFront = true;
			currentTile_->searchRightUp = false;
			break;
		}
		if ((idHelper - GRID_SIZE_WIDTH + 1) > minID)
		{
			//Check front
			if (detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1)->isRock)
			{
				rockFront = true;
			}
		}

		//right
		if (detectedTileData_.at(idHelper)->location.x < GRID_SIZE_WIDTH-1)
		{
			detectedTileData_.at(idHelper)->searchRight = !detectedTileData_.at(idHelper + 1)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchRight = false;
		}

		//up
		if (detectedTileData_.at(idHelper)->location.y > 0)
		{
			detectedTileData_.at(idHelper)->searchUp =
				!detectedTileData_.at(idHelper - GRID_SIZE_WIDTH)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchUp = false;
		}


		if (detectedTileData_.at(idHelper + GRID_SIZE_WIDTH)->isRock)
		{
			rockUnder = !detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1)->isRock;
		}

		thingFoundRight = JPSsearchRight(detectedTileData_.at(idHelper));
		thingFoundUp = JPSsearchUp(detectedTileData_.at(idHelper));

		if (detectedTileData_.at(idHelper)->location.x > 0
			&& detectedTileData_.at(idHelper)->location.y > 0)
		{
			if (detectedTileData_.at(idHelper - 1)->isRock)
			{
				rockLeft = !detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1)->isRock;
			}
			else
				rockLeft = false;
		}


		if (rockLeft || rockUnder || thingFoundUp || thingFoundRight ||detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;

					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper + GRID_SIZE_WIDTH - 1), GCostHelper);
				}
			}
			if (wasFound == false)
			{
				detectedTileData_.at(idHelper)->searchRightUp = !rockFront;
				detectedTileData_.at(idHelper)->searchRightDown = rockUnder;

				detectedTileData_.at(idHelper)->searchLeftUp = rockLeft;


				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper + GRID_SIZE_WIDTH - 1), GCostHelper);

				openList_.push_back(detectedTileData_.at(idHelper));
			}

			currentTile_->searchRightUp = false;

		}
		else
		{
			if (rockFront)
				currentTile_->searchRightUp = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper + GRID_SIZE_WIDTH-1);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}
	}

	if (!rockFront)
		currentTile_->searchRightUp = true;
}

void StarHunter::JSPsearchRightDown()
{
	int idHelper = currentTile_->tileID;
	bool rockFront = false;
	bool rockOver = false;

	bool rockLeft = false;

	bool thingFoundDown = false;
	bool thingFoundRight = false;

	int GCostHelper = 0;

	int minID = 0;
	int maxID = GRID_SIZE - 1;

	while (currentTile_->searchRightDown)
	{

		if (detectedTileData_.at(idHelper)->location.x >= GRID_SIZE_WIDTH - 1
			||
			detectedTileData_.at(idHelper)->location.y >= GRID_SIZE_HEIGHT - 1)
		{
			rockFront = true;
			currentTile_->searchRightDown = false;
			break;
		}

		GCostHelper += 14;
		idHelper = idHelper + GRID_SIZE_WIDTH + 1;

		if (idHelper > maxID || idHelper < minID)
		{
			currentTile_->searchRightDown = false;
			rockFront = true;
			break;
		}

		if (detectedTileData_.at(idHelper)->isClosed)
		{
			currentTile_->searchRightDown = false;
			rockFront = true;
			break;
		}

		if ((idHelper + GRID_SIZE_WIDTH + 1) < maxID)
		{
			//Check front
			if (detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1)->isRock)
			{
				rockFront = true;
			}
		}

		//Checl right
		if (detectedTileData_.at(idHelper)->location.x < GRID_SIZE_WIDTH-1)
		{
			detectedTileData_.at(idHelper)->searchRight = !detectedTileData_.at(idHelper + 1)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchRight = false;
		}

		//Check down
		if (detectedTileData_.at(idHelper)->location.y < GRID_SIZE_HEIGHT-1)
		{
			detectedTileData_.at(idHelper)->searchDown =
				!detectedTileData_.at(idHelper + GRID_SIZE_WIDTH)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchDown = false;
		}

		if (detectedTileData_.at(idHelper - GRID_SIZE_WIDTH)->isRock)
		{
			rockOver = !detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1)->isRock;
		}

		thingFoundRight = JPSsearchRight(detectedTileData_.at(idHelper));
		thingFoundDown = JPSsearchDown(detectedTileData_.at(idHelper));

		if (detectedTileData_.at(idHelper)->location.x > 0
			&& detectedTileData_.at(idHelper)->location.y < (GRID_SIZE_HEIGHT-1))
		{
			if (detectedTileData_.at(idHelper - 1)->isRock)
			{
				rockLeft = !detectedTileData_.at(idHelper + GRID_SIZE_WIDTH - 1)->isRock;
			}
			else
				rockLeft = false;
		}

		if (rockOver || thingFoundDown || thingFoundRight || detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;

					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1), GCostHelper);
				}
			}
			if (wasFound == false)
			{
				detectedTileData_.at(idHelper)->searchRightDown = !rockFront;
				detectedTileData_.at(idHelper)->searchRightUp = rockOver;
				detectedTileData_.at(idHelper)->searchLeftDown = rockLeft;



				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1), GCostHelper);

				openList_.push_back(detectedTileData_.at(idHelper));
			}

			currentTile_->searchRightDown = false;

		}
		else
		{
			if (rockFront)
				currentTile_->searchRightDown = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}
	}

	if (!rockFront)
		currentTile_->searchRightDown = true;
}

void StarHunter::JSPsearchLeftUp()
{
	int idHelper = currentTile_->tileID;
	bool rockFront = false;

	bool rockUnder = false;
	bool rockRight = false;

	bool thingFoundUp = false;
	bool thingFoundLeft = false;

	int GCostHelper = 0;

	int minID = 0;
	int maxID = GRID_SIZE - 1;

	while (currentTile_->searchLeftUp)
	{
		if (detectedTileData_.at(idHelper)->location.x <= 0
			||
			detectedTileData_.at(idHelper)->location.y <= 0)
		{
			currentTile_->searchLeftUp = false;
			rockFront = true;
			break;
		}

		GCostHelper += 14;
		idHelper = idHelper - GRID_SIZE_WIDTH - 1;

		if (idHelper > maxID || idHelper < minID)
		{
			currentTile_->searchLeftUp = false;
			rockFront = true;
			break;
		}

		if (detectedTileData_.at(idHelper)->isClosed)
		{
			currentTile_->searchLeftUp = false;
			rockFront = true;
			break;
		}

		if ((idHelper - GRID_SIZE_WIDTH - 1) > minID)
		{
			//Check front
			if (detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1)->isRock)
			{
				rockFront = true;
			}
		}


		if (detectedTileData_.at(idHelper)->location.x > 0)
		{
			detectedTileData_.at(idHelper)->searchLeft = !detectedTileData_.at(idHelper - 1)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchLeft = false;
		}


		if (detectedTileData_.at(idHelper)->location.y > 0)
		{
			detectedTileData_.at(idHelper)->searchUp =
				!detectedTileData_.at(idHelper - GRID_SIZE_WIDTH)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchUp = false;
		}
		
		if (detectedTileData_.at(idHelper + GRID_SIZE_WIDTH)->isRock)
		{
			rockUnder = !detectedTileData_.at(idHelper + GRID_SIZE_WIDTH - 1)->isRock;
		}



		thingFoundLeft = JPSsearchLeft(detectedTileData_.at(idHelper));
		thingFoundUp = JPSsearchUp(detectedTileData_.at(idHelper));
		
		if (detectedTileData_.at(idHelper)->location.x < GRID_SIZE_WIDTH-1
			&& detectedTileData_.at(idHelper)->location.y > 0)
		{
			if (detectedTileData_.at(idHelper + 1)->isRock)
			{
				rockRight = !detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1)->isRock;
			}
			else
				rockRight = false;
		}

		if (rockRight || rockUnder || thingFoundUp || thingFoundLeft || detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;

					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1), GCostHelper);
				}
			}
			if (wasFound == false)
			{
				detectedTileData_.at(idHelper)->searchLeftUp = !rockFront;
				detectedTileData_.at(idHelper)->searchLeftDown = rockUnder;

				detectedTileData_.at(idHelper)->searchRightUp = rockRight;

				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1), GCostHelper);

				openList_.push_back(detectedTileData_.at(idHelper));
			}

			currentTile_->searchLeftUp = false;

		}
		else
		{
			if (rockFront)
				currentTile_->searchLeftUp = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}
	}

	if (!rockFront)
		currentTile_->searchLeftUp = true;
}

void StarHunter::JSPsearchLeftDown()
{
	int idHelper = currentTile_->tileID;
	bool rockFront = false;
	bool rockOver = false;

	bool rockRight = false;

	bool thingFoundDown = false;
	bool thingFoundLeft = false;

	int GCostHelper = 0;

	int minID = 0;
	int maxID = GRID_SIZE - 1;

	while (currentTile_->searchLeftDown)
	{

		if (detectedTileData_.at(idHelper)->location.x <=0
			||
			detectedTileData_.at(idHelper)->location.y >= GRID_SIZE_HEIGHT - 1)
		{
			currentTile_->searchLeftDown = false;
			rockFront = true;
			break;
		}

		GCostHelper += 14;
		idHelper = idHelper + GRID_SIZE_WIDTH - 1;

		if (idHelper > maxID || idHelper < minID)
		{
			currentTile_->searchLeftDown = false;
			rockFront = true;
			break;
		}

		if (detectedTileData_.at(idHelper)->isClosed)
		{
			currentTile_->searchLeftDown = false;
			rockFront = true;
			break;
		}

		if ((idHelper + GRID_SIZE_WIDTH - 1) < maxID)
		{
			//Check front
			if (detectedTileData_.at(idHelper + GRID_SIZE_WIDTH - 1)->isRock)
			{
				rockFront = true;
			}
		}


		if (detectedTileData_.at(idHelper)->location.x > 0)
		{
			detectedTileData_.at(idHelper)->searchLeft = !detectedTileData_.at(idHelper - 1)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchLeft = false;
		}

		if (detectedTileData_.at(idHelper)->location.y < GRID_SIZE_HEIGHT - 1)
		{
			detectedTileData_.at(idHelper)->searchDown =
				!detectedTileData_.at(idHelper + GRID_SIZE_WIDTH)->isRock;
		}
		else
		{
			detectedTileData_.at(idHelper)->searchDown = false;
		}


		if (detectedTileData_.at(idHelper - GRID_SIZE_WIDTH)->isRock)
		{
			rockOver = !detectedTileData_.at(idHelper - GRID_SIZE_WIDTH - 1)->isRock;
		}

		thingFoundLeft = JPSsearchLeft(detectedTileData_.at(idHelper));
		thingFoundDown = JPSsearchDown(detectedTileData_.at(idHelper));
		
		if (detectedTileData_.at(idHelper)->location.x < GRID_SIZE_WIDTH - 1
			&& detectedTileData_.at(idHelper)->location.y < (GRID_SIZE_HEIGHT - 1))
		{
			if (detectedTileData_.at(idHelper - 1)->isRock)
			{
				rockRight = !detectedTileData_.at(idHelper + GRID_SIZE_WIDTH + 1)->isRock;
			}
			else
				rockRight = false;
		}

		if (rockRight || rockOver || thingFoundDown || thingFoundLeft || detectedTileData_.at(idHelper)->location == actorGoal_)
		{
			bool wasFound = false;
			for (auto &tile : openList_)
			{
				if (tile == detectedTileData_.at(idHelper))
				{
					wasFound = true;

					int newFCost = tile->HCost + GCostHelper;
					int oldFCost = tile->FCost;

					if (newFCost < oldFCost)
						detectedTileData_.at(idHelper)->generateSearchData(
							actorGoal_, detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1), GCostHelper);
				}
			}
			if (wasFound == false)
			{
				detectedTileData_.at(idHelper)->searchLeftDown = !rockFront;
				detectedTileData_.at(idHelper)->searchLeftUp = rockOver;
				detectedTileData_.at(idHelper)->searchRightDown = rockRight;

				detectedTileData_.at(idHelper)->generateSearchData(
					actorGoal_, detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1), GCostHelper);

				openList_.push_back(detectedTileData_.at(idHelper));
			}

			//if (rockFront)
				currentTile_->searchLeftDown = false;

		}
		else
		{
			if (rockFront)
				currentTile_->searchLeftDown = false;

			detectedTileData_.at(idHelper)->parent = detectedTileData_.at(idHelper - GRID_SIZE_WIDTH + 1);
			closedList_.push_back(detectedTileData_.at(idHelper));
		}
	}

	if (!rockFront)
		currentTile_->searchLeftDown = true;
}

bool StarHunter::isInOpenList(int idToCheck)
{
	for (auto &tile : openList_)
	{
		if (tile->tileID == idToCheck)
			return true;
	}
	return false;
}

bool StarHunter::isInOpenList(std::shared_ptr<DetectedTileData> checkMe)
{
	for (auto &tile : openList_)
	{
		if (tile == checkMe)
			return true;
	}
	return false;
}
