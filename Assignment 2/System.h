#ifndef ASSIGNMENT2_SYSTEM_H
#define ASSIGNMENT2_SYSTEM_H
#include <PotatoEngine/GLSLProgram.h>
#include <PotatoEngine/InputManager.h>
#include <PotatoEngine/ResourceManager.h>
#include <PotatoEngine/AudioManager.h>
#include <PotatoEngine/CollisionManager.h>
#include <PotatoEngine/AnimationManager.h>

#include "SimulationState.h"


//***************************************************************************
//*	System struct, keeps pointer to all diffrent system components			*
//*	Like audio manager, input manager etc									*
//***************************************************************************
struct System
{
	//Constructors
	System() { /*~Empty~*/ };
	System(
		SimulationStates *States,
		sf::RenderWindow *Window,
		sf::View *CameraView,
		PotatoEngine::GLSLProgram *TextureProgram,
		PotatoEngine::InputManager *InputManager,
		PotatoEngine::ResourceManager *ResourceManager,
		PotatoEngine::AudioManager *AudioManager,
		PotatoEngine::CollisionManager *CollisionManager,
		PotatoEngine::AnimationManager *AnimationManager,
		std::string* GameDictionary)
		:
		simulationState(States),
		window(Window),
		cameraView(CameraView),
		textureProgram(TextureProgram),
		inputManager(InputManager),
		resourceManager(ResourceManager),
		audioManager(AudioManager),
		collisionManager(CollisionManager),
		animationManager(AnimationManager),
		gameDictionary(GameDictionary)
	{ /*Empty*/
	};

	void resetView(sf::FloatRect rect = sf::FloatRect(0, 0, 1920, 1080))
	{
		cameraView->reset(rect);
		cameraView->setViewport(sf::FloatRect(0.f, 0.f, 1.0f, 1.0f));
		window->setView(*cameraView);
	}

	//Pointer variables of system.
	SimulationStates* simulationState;
	sf::RenderWindow* window;
	sf::View* cameraView;
	PotatoEngine::GLSLProgram* textureProgram;
	PotatoEngine::InputManager* inputManager;
	PotatoEngine::ResourceManager* resourceManager;
	PotatoEngine::AudioManager* audioManager;
	PotatoEngine::CollisionManager* collisionManager;
	PotatoEngine::AnimationManager* animationManager;

	std::string* gameDictionary;
};

#endif // ASSIGNMENT2_SYSTEM_H