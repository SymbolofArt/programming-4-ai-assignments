#pragma once

/* SIMULATION SETTINGS */
#define SIMULATION_SPEED			1											///60 = 1sec

#define SIMULATE_RANDOM_STARS		1
#define SHOW_SETTING_HUD			0
/* GRID SETTINGS */
#define GRID_SIZE_HEIGHT			14//56//14										///Height of the simulation in nmumber of tiles
#define GRID_SIZE_WIDTH				32//128//32											///Width of the simulation in number of tiles.
#define GRID_SIZE					(GRID_SIZE_HEIGHT * GRID_SIZE_WIDTH)		///Total number of tiles,		---DO NOT CHANGE THIS VALUE!!-----

/* ROCKS SETTINGS*/
#define WORLD_START_ROCKS			100


/* ACTOR SETTINGS */
// 0 = false, 1 = true
#define ACTOR_DRAW_SEARCH_DATA		1
#define ACTOR_INSTANT_FIND_PATH		0
#define ACTOR_WALK_TO_TARGET		0
#define ACTOR_USE_JPS				0		//EXPERIMENTAL; KINDA GLITCHY; LOL