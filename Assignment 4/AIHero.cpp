#include "AIHero.h"



AIHero::AIHero()
{
	health_ = AI_HERO_MAXHEALTH;
	maxHealth_ = AI_HERO_MAXHEALTH;
	gotPath = false;

	targetLocation_ = glm::vec2(2, 1);	
	setUpBehaviourTree();
}

AIHero::~AIHero()
{

}

void AIHero::setUpBehaviourTree()
{
	RootSelector_ = std::make_shared<Selector>();

	//std::shared_ptr<>

	/*ATTACK SEQUENCE*/
	std::shared_ptr<Sequence> attackSequence = std::make_shared<Sequence>();
	RootSelector_->addChild(attackSequence);
	
	std::shared_ptr<AIHero_BT_SeenGoblins> SeenGoblins = std::make_shared<AIHero_BT_SeenGoblins>();
	attackSequence->addChild(SeenGoblins);

	//in range selector
	std::shared_ptr<Selector> InRange = std::make_shared<Selector>();
	attackSequence->addChild(InRange);

	std::shared_ptr<AIHERO_BT_MoveInRange> MoveInRange = std::make_shared<AIHERO_BT_MoveInRange>();
	std::shared_ptr<AIHero_BT_MeleeAttack> MeleeAttack = std::make_shared<AIHero_BT_MeleeAttack>();
	InRange->addChild(MoveInRange);
	InRange->addChild(MeleeAttack);

	/*WALK SEQUENCE*/
	std::shared_ptr<Sequence> walkSequence = std::make_shared<Sequence>();
	RootSelector_->addChild(walkSequence);

	std::shared_ptr<AIHero_BT_TileLeftToSearch> tileLeftToSearch = std::make_shared<AIHero_BT_TileLeftToSearch>();
	walkSequence->addChild(tileLeftToSearch);

	std::shared_ptr<AIHero_BT_WalkToUnvisitedTile> walkToTile = std::make_shared<AIHero_BT_WalkToUnvisitedTile>();
	walkSequence->addChild(walkToTile);
	
	/*VICTORY NODE*/
	std::shared_ptr<AIHero_BT_VICTORY> victoryNode = std::make_shared<AIHero_BT_VICTORY>();
	RootSelector_->addChild(victoryNode);

	SeenGoblins->setHero(this);
	MeleeAttack->setHero(this);
	MoveInRange->setHero(this);
	tileLeftToSearch->setHero(this);
	walkToTile->setHero(this);
	victoryNode->setHero(this);
	
}

void AIHero::update()
{
	/*tmp health reg for test purpose.*/
	if (AI_HERO_HP_REG_ENABLE == 1)
	{
		if (lifeRegCooldown > AI_HERO_HP_REG_CD)
		{
			lifeRegCooldown = 0;
			heal(1);
		}
		else
			lifeRegCooldown++;
	}

	RootSelector_->run();

	updateVisibleArea();
}

void AIHero::print()
{
	switch (health_)
	{
	case 9: printatc("9", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 8: printatc("8", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 7: printatc("7", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 6: printatc("6", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 5: printatc("5", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 4: printatc("4", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 3: printatc("3", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 2: printatc("2", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	case 1: printatc("1", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	default: printatc("?", AI_HERO_COLOR, (int)location_.x, (int)location_.y); break;
	}
}

void AIHero::setSharedMap(std::shared_ptr<Grid> worldMap)
{
	gridWorld_ = worldMap;

	pathFinder_ = PathFinder(worldMap);
}

void AIHero::clearDetection()
{
	enemyList_.clear();
}

void AIHero::addEnemyDetected(std::shared_ptr<Entity> enemy)
{
	enemyList_.push_back(enemy);
}

void AIHero::updateVisibleArea()
{
	for (int y = -(AI_HERO_DETECTION_AREA / 2); y < AI_HERO_DETECTION_AREA / 2; y++)
	{
		for (int x = -(AI_HERO_DETECTION_AREA / 2); x < AI_HERO_DETECTION_AREA / 2; x++)
		{
			gridWorld_->setVisible(location_ + glm::vec2(x, y), true);
		}
	}
}

void AIHero::updateTargetLocation()
{
	bool generateFirst = true;

	for (auto tile : gridWorld_->getGridMap())
	{
		if (tile.visible && tile.walkable == true && tile.visited == false)
		{
			if (generateFirst)
			{
				generateFirst = false;
				targetLocation_ = tile.location;
			}
			else
			{
				if (abs(location_.x - tile.location.x) * 10 + abs(location_.y - tile.location.y) * 10
					<
					abs(location_.x - targetLocation_.x) * 10 + abs(location_.y - targetLocation_.y) * 10)
				{

					targetLocation_ = tile.location;
				}
			}
		}
	}

	if (generateFirst)
	{
		targetLocation_ = glm::vec2(1, 1);
		won_ = true;
	}
}

bool AIHero_BT_SeenGoblins::run()
{
	if (hero_->enemyList_.size() > 0)
	{
		glm::vec2 locOfCloestOne = hero_->enemyList_.at(0)->getLocation();
		hero_->targetedGoblin_ = hero_->enemyList_.at(0);
		//find closest one
		for (auto &goblin : hero_->enemyList_)
		{
			if (abs(hero_->getLocation().x - goblin->getLocation().x)
				+ abs(hero_->getLocation().y - goblin->getLocation().y) < 
				
				abs(hero_->getLocation().x - locOfCloestOne.x)
				+ abs(hero_->getLocation().y - locOfCloestOne.y)
				)
			{
				locOfCloestOne = goblin->getLocation();
				hero_->targetedGoblin_ = goblin;
			}
		}

		hero_->targetLocation_ = locOfCloestOne;
		return true;
	}
	
	//if hero can't see any goblins, return false.
	return false;
}

bool AIHero_BT_MeleeAttack::run()
{
	hero_->targetedGoblin_->dealDamage(1);
	return true;
}

bool AIHERO_BT_MoveInRange::run()
{
	if (abs(hero_->getLocation().x - hero_->targetLocation_.x)
		+ abs(hero_->getLocation().y - hero_->targetLocation_.y) > 1)
	{
		
		hero_->pathWay_ = hero_->pathFinder_.FindPath(hero_->location_, hero_->targetLocation_);
		if (hero_->pathWay_.size() == 0)
			return true;

		if (hero_->gridWorld_->isEntity(hero_->pathWay_.at(0)))
			return true;

		hero_->gridWorld_->setVisited(hero_->location_, true);
		if (hero_->location_ != hero_->targetLocation_ && hero_->pathWay_.size() > 0)
		{
			hero_->gridWorld_->setEntity(hero_->location_, false);
			hero_->location_ = hero_->pathWay_.at(0);
			hero_->pathWay_.erase(hero_->pathWay_.begin());

			hero_->gridWorld_->setVisited(hero_->location_, true);
			hero_->gridWorld_->setEntity(hero_->location_, true);
		}

		return true;
	}
	else
	{
		//we are close enough to melee attack
		return false;
	}

	
}

bool AIHero_BT_TileLeftToSearch::run()
{
	/*if (hero_->gotPath)
	{
		return true;
	}*/
	bool generateFirst = true;


	for (auto tile : hero_->gridWorld_->getGridMap())
	{
		if (tile.visible && tile.walkable == true && tile.visited == false)
		{
			if (generateFirst)
			{
				generateFirst = false;
				hero_->targetLocation_ = tile.location;
			}
			else
			{
				if (abs(hero_->location_.x - tile.location.x) * 10 + abs(hero_->location_.y - tile.location.y) * 10
					<
					abs(hero_->location_.x - hero_->targetLocation_.x) * 10 + abs(hero_->location_.y - hero_->targetLocation_.y) * 10)
				{

					hero_->targetLocation_ = tile.location;
				}
			}
		}
	}


	if (generateFirst)
	{
		hero_->targetLocation_ = glm::vec2(1, 1);
		return false;
	}
	else
		return true;
}

bool AIHero_BT_WalkToUnvisitedTile::run()
{
	/*if (hero_->gotPath == false)
	{*/
		hero_->gotPath = true;
		hero_->pathWay_ = hero_->pathFinder_.FindPath(hero_->location_, hero_->targetLocation_);
		if (hero_->pathWay_.size() == 0)
			return true;
	/*}
	else
	{*/
		hero_->gridWorld_->setVisited(hero_->location_, true);
		if (hero_->location_ != hero_->targetLocation_)
		{
			hero_->gridWorld_->setEntity(hero_->location_, false);
			hero_->location_ = hero_->pathWay_.at(0);
			hero_->pathWay_.erase(hero_->pathWay_.begin());

			hero_->gridWorld_->setVisited(hero_->location_, true);
			hero_->gridWorld_->setEntity(hero_->location_, true);
			return true;
		}
		else
			hero_->gotPath = false;
	/*}*/
	return false;
}

bool AIHero_BT_VICTORY::run()
{
	return false;
}
