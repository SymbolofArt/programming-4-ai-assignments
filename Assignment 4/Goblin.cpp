#include "Goblin.h"

Goblin::Goblin()
{
}

Goblin::Goblin(glm::vec2 location, std::shared_ptr<Entity> hero)
{
	location_ = location;
	aiHero_ = hero;
	health_ = AI_GOBLIN_MAXHEALTH;
	maxHealth_ = AI_GOBLIN_MAXHEALTH;

	setUpBehaviourTree();
}

Goblin::~Goblin()
{
}

void Goblin::setUpBehaviourTree()
{
	//main selector
	RootSelector_ = std::make_shared<Selector>();

	/*ATTACK SEQUENCE*/
	std::shared_ptr<Sequence> attackSequence = std::make_shared<Sequence>();
	RootSelector_->addChild(attackSequence);

	std::shared_ptr<AIGoblin_BT_SeenHero> SeenHero = std::make_shared<AIGoblin_BT_SeenHero>();
	attackSequence->addChild(SeenHero);

	//in range selector
	std::shared_ptr<Selector> InRange = std::make_shared<Selector>();
	attackSequence->addChild(InRange);

	std::shared_ptr<AIGoblin_BT_MoveToTarget> MoveInRange = std::make_shared<AIGoblin_BT_MoveToTarget>();
	std::shared_ptr<AIGoblin_BT_MeleeAttack> MeleeAttack = std::make_shared<AIGoblin_BT_MeleeAttack>();
	InRange->addChild(MoveInRange);
	InRange->addChild(MeleeAttack);

	/*HELP SEQUENCE*/
	std::shared_ptr<Sequence> supportSequence = std::make_shared<Sequence>();
	RootSelector_->addChild(supportSequence);

	std::shared_ptr<AIGoblin_BT_SeenEnragedGoblin> seenEnragedGoblin = std::make_shared<AIGoblin_BT_SeenEnragedGoblin>();
	std::shared_ptr<AIGoblin_BT_MoveEnragedGoblin> moveToEnragedGoblin = std::make_shared<AIGoblin_BT_MoveEnragedGoblin>();
	supportSequence->addChild(seenEnragedGoblin);
	supportSequence->addChild(moveToEnragedGoblin);

	SeenHero->setOwner(this);
	MoveInRange->setOwner(this);
	MeleeAttack->setOwner(this);
	seenEnragedGoblin->setOwner(this);
	moveToEnragedGoblin->setOwner(this);

}

void Goblin::update()
{
	RootSelector_->run();
}

void Goblin::print()
{
	if (visible_)
	switch (health_)
	{
	case 9: printatc("9", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 8: printatc("8", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 7: printatc("7", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 6: printatc("6", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 5: printatc("5", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 4: printatc("4", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 3: printatc("3", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 2: printatc("2", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	case 1: printatc("1", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	default: printatc("?", AI_GOBLIN_COLOR, (int)location_.x, (int)location_.y); break;
	}
}


void AI_GOBLIN_NODE::setOwner(Goblin * owner)
{
	owner_ = owner;
}

bool AIGoblin_BT_SeenHero::run()
{	
	if (abs(owner_->getLocation().x - owner_->aiHero_->getLocation().x)
		+ abs(owner_->getLocation().y - owner_->aiHero_->getLocation().y) < AI_GOBLIN_DETECTION_AREA / 2)
	{
		owner_->setIsInCombat(true);
		return true;
	}
	else
	{
		owner_->setIsInCombat(false);
		return false;
	}

}

bool AIGoblin_BT_MoveToTarget::run()
{
	if (abs(owner_->getLocation().x - owner_->aiHero_->getLocation().x)
		+ abs(owner_->getLocation().y - owner_->aiHero_->getLocation().y) > 1)
	{

		owner_->pathWay_ = owner_->pathFinder_.FindPath(owner_->location_, owner_->aiHero_->getLocation());
		
		if (owner_->pathWay_.size() == 0)
			return true;

		if (owner_->gridWorld_->isEntity(owner_->pathWay_.at(0)))
			return true;

		if (owner_->location_ != owner_->aiHero_->getLocation() && owner_->pathWay_.size() > 0)
		{
			owner_->gridWorld_->setEntity(owner_->location_, false);
			owner_->location_ = owner_->pathWay_.at(0);
			owner_->pathWay_.erase(owner_->pathWay_.begin());
			owner_->gridWorld_->setEntity(owner_->location_, true);
		}

		return true;
	}
	else
	{
		//we are close enough to melee attack
		return false;
	}
}

bool AIGoblin_BT_MeleeAttack::run()
{
	if (attackCD_ > AI_GOBLIN_ATTACK_CD)
	{
		attackCD_ = 0;
		owner_->aiHero_->dealDamage(1);
	}
	else
		attackCD_++;
	return true;
}

bool AIGoblin_BT_SeenEnragedGoblin::run()
{
	if (owner_->friendList_.size() > 0)
	{
		for (auto &goblin : owner_->friendList_)
		{
			if (goblin->isInCombat())
			{
				owner_->targetLoc_ = goblin->getLocation();
				return true;
			}

		}
	}

	return false;
}

bool AIGoblin_BT_MoveEnragedGoblin::run()
{
	if (abs(owner_->getLocation().x - owner_->targetLoc_.x)
		+ abs(owner_->getLocation().y - owner_->targetLoc_.y) > 1)
	{

		owner_->pathWay_ = owner_->pathFinder_.FindPath(owner_->location_, owner_->targetLoc_);

		if (owner_->pathWay_.size() == 0)
			return true;

		if (owner_->gridWorld_->isEntity(owner_->pathWay_.at(0)))
			return true;

		if (owner_->location_ != owner_->targetLoc_ && owner_->pathWay_.size() > 0)
		{
			owner_->gridWorld_->setEntity(owner_->location_, false);
			owner_->location_ = owner_->pathWay_.at(0);
			owner_->pathWay_.erase(owner_->pathWay_.begin());
			owner_->gridWorld_->setEntity(owner_->location_, true);
		}

		return true;
	}
	else
	{
		//we are close enough to melee attack
		return false;
	}
	return false;
}
