#include "PathFinder.h"



PathFinder::PathFinder()
{

}

PathFinder::PathFinder(std::shared_ptr<Grid> sharedGridWorld)
	:
	gridWorld_(sharedGridWorld)
{

}


PathFinder::~PathFinder()
{

}


std::vector<glm::vec2> PathFinder::FindPath(glm::vec2 startLocation, glm::vec2 targetLocation)
{
	needsToGenerateFirstTile = true;
	foundPath_ = false;
	openListEmpty_ = false;

	searchNodes_.clear();
	openList_.clear();

	startLocation_ = startLocation;
	targetLocation_ = targetLocation;

	storedGridMap = gridWorld_->getGridMap();
	int id = 0;
	//Create stored map.
	for (auto &tile : storedGridMap)
	{
		searchNodes_.emplace_back(std::make_shared<SearchNode>(tile.location, !tile.walkable, id));
		id++;
	}



	//Find path.
	searchPath();

	if (foundPath_)
	{
		std::vector<glm::vec2> pathData;
		
		//currentNode_ = startNode_;
		currentNode_ = searchNodes_.at(startNode_->retrackChildID);

		while (currentNode_->location != targetLocation_)
		{
			pathData.emplace_back(currentNode_->location);
			currentNode_ = searchNodes_.at(currentNode_->retrackChildID);
		}

		pathData.emplace_back(currentNode_->location);
		return pathData;
	}

	



	return std::vector<glm::vec2>();



}

void PathFinder::searchPath()
{
	//Check if we just started and need to get the start node.
	if (needsToGenerateFirstTile)
	{
		needsToGenerateFirstTile = false;
		for (auto &node : searchNodes_)
		{
			if (node->location == startLocation_)
			{
				currentNode_ = node;
				startNode_ = node;
			}
		}
		currentNode_->generateSearchData(targetLocation_, nullptr, 0);
		currentNode_->isClosed = true;
	}

	//Check if we reached our goal.
	if (currentNode_->location == targetLocation_)
	{
		//Goal reached.
		currentNode_->createPath();
		foundPath_ = true;
		return;
	}

	//Check if openList is empty.
	if (openListEmpty_)
	{
		isStuck_ = true;
		return;
	}

	//Check if there is new neighbors to add or update.
	checkNeighbors();

	//After going true all, go true all in openList and check for wish has lowest f value.
	std::shared_ptr<SearchNode> currentLowest = nullptr;
	int clID = 0;
	int helper = 0;
	bool overrideFirstCheck = true;

	//Get the cloest one.
	for (auto &openNode : openList_)
	{
		if (overrideFirstCheck)
		{
			currentLowest = openNode;
			overrideFirstCheck = false;
		}
		else
		{

			if (openNode->FCost < currentLowest->FCost
				|| //OR
				(openNode->FCost == currentLowest->FCost
					&& //AND
					openNode->HCost < currentLowest->HCost
					))
			{
				currentLowest = openNode;
				clID = helper;
			}
		}
		helper++;
	}
	
	//Check if openlist is empty. Cause then we need to stop right here and finish as we are stuck.
	if (openList_.size() == 0 || currentLowest == nullptr)
	{
		openListEmpty_ = true;
		return;
	}

	//Remove the new one to check from the open list.
	openList_.at(clID) = nullptr;
	int sizeBeforeErase = (int)openList_.size();

	openList_.erase(openList_.begin() + clID);

	currentNode_ = currentLowest;
	currentNode_->isClosed = true;


	searchPath();
}

void PathFinder::checkNeighbors()
{
	//get neighbors
	std::vector<std::shared_ptr<SearchNode>> neighbors;
	neighbors.clear();

	//Check and add all neighbors to the neighbors list.
	if (currentNode_->location.x < GRID_SIZE_WIDTH - 1)			//Right
	{
		neighbors.push_back(
			searchNodes_.at
			(currentNode_->tileID + 1));

		//if (currentNode_->location.y < GRID_SIZE_HEIGHT - 1)	//Bottom Right
		//	neighbors.push_back(searchNodes_.at
		//	(currentNode_->tileID + GRID_SIZE_WIDTH + 1));
		//if (currentNode_->location.y > 0)						//Top Right
		//	neighbors.push_back(searchNodes_.at
		//	(currentNode_->tileID - GRID_SIZE_WIDTH + 1));
	}

	if (currentNode_->location.x > 0)							//Left
	{
		neighbors.push_back(searchNodes_.at
		(currentNode_->tileID - 1));
		//if (currentNode_->location.y < GRID_SIZE_HEIGHT - 1)	//Bottom Left
		//	neighbors.push_back(searchNodes_.at
		//	(currentNode_->tileID + GRID_SIZE_WIDTH - 1));
		//if (currentNode_->location.y > 0)						//Top Left
		//	neighbors.push_back(searchNodes_.at
		//	(currentNode_->tileID - GRID_SIZE_WIDTH - 1));
	}

	if (currentNode_->location.y < GRID_SIZE_HEIGHT - 1)		//Bottom
		neighbors.push_back(searchNodes_.at
		(currentNode_->tileID + GRID_SIZE_WIDTH+1));
	if (currentNode_->location.y > 0)							//Top
		neighbors.push_back(searchNodes_.at
		(currentNode_->tileID - GRID_SIZE_WIDTH-1));

	//Variables to help and check stuff.
	bool skipMe = false;
	int newGCost = 0;

	//Go true all of its neighbors.
	for (auto &neighborTile : neighbors)
	{
		//If its a rock or is closed, just continue and dont check this one.
		if (neighborTile->isRock)// || neighborTile->isClosed)
			continue;

		if (neighborTile->isClosed)
		{
			int currentF = neighborTile->FCost;
			newGCost = currentNode_->GCost
				+ getGCost(currentNode_->location, neighborTile->location);

			int newFCost = neighborTile->HCost + newGCost;
			if (newFCost < currentF)
			{
				neighborTile->generateSearchData(targetLocation_, currentNode_, newGCost);
			}
			continue;
		}

		//Check if its in the open list, if yes we need to calculate and see if its a better GCost.
		bool IsInOpenList = false;

		for (auto &openTile : openList_)
		{
			if (openTile == neighborTile)
			{
				//Get the tiles current fcost
				int currentF = neighborTile->FCost;
				//Get what it would be for f cost if it changes parent.

				//Get new GCost
				newGCost = currentNode_->GCost
					+ getGCost(currentNode_->location, neighborTile->location);

				int newFCost = neighborTile->HCost + newGCost;

				//check if its better! if yes, generate new neighbor search data!
				if (newFCost < currentF)
				{
					neighborTile->generateSearchData(targetLocation_, currentNode_, newGCost);
				}

				IsInOpenList = true;
				break;
			}
		}

		if (IsInOpenList)
			continue;

		//If it was not found in the closed or open list, add it to the openlist.
		openList_.push_back(neighborTile);
		newGCost = currentNode_->GCost;
		newGCost += getGCost(currentNode_->location, neighborTile->location);
		neighborTile->generateSearchData(targetLocation_, currentNode_, newGCost);


	}
}

int PathFinder::getGCost(glm::vec2 a, glm::vec2 b)
{
	//Check if its a corner, if yes, its 14, else 10. : )
	if (a.x != b.x && a.y != b.y)
		return 14;
	else
		return 10;
}
