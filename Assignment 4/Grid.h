#ifndef ASSIGNMENT4_GRID_H
#define ASSIGNMENT4_GRID_H
#include <glm\glm.hpp>
#include <iostream>
#include <memory>
#include <vector>
#include <functional>

#include "pbctp.h"
#include "SimulationSettings.h"

struct Tile
{
	Tile(int X, int Y, bool Walkable)
		: location(glm::vec2(X,Y)),
		walkable(Walkable), 
		visible(false), 
		visited(!Walkable),
		entity(false)
	{ /*Empty*/ }
	Tile(glm::vec2 loc, bool Walkable)
		:
		location(loc),
		walkable(Walkable),
		visible(false),
		visited(!Walkable), entity(false)
	{ /*Empty*/ }

	glm::vec2 location;

	bool walkable;
	bool visible;
	bool visited;

	bool entity;

};

class Grid
{
public:
	//Constructors
	Grid();
	Grid(int width, int height, std::vector<bool> gridData);

	Tile getTileAt(int x, int y);
	Tile getTileAt(glm::vec2 location);

	bool isWalkableAt(int x, int y);
	bool isWalkableAt(glm::vec2 location);

	bool isVisible(int x, int y);
	bool isVisible(glm::vec2 location);

	bool isVisited(int x, int y);
	bool isVisited(glm::vec2 location);

	bool isEntity(int x, int y);
	bool isEntity(glm::vec2 location);

	bool isInsideGrid(int x, int y);
	bool isInsideGrid(glm::vec2 location);

	void setIsWalkableAt(int x, int y, bool isWalkable);
	void setIsWalkableAt(glm::vec2 location, bool isWalkable);

	void setVisible(int x, int y, bool isVisible);
	void setVisible(glm::vec2 location, bool isVisible);

	void setVisited(int x, int y, bool isVisited);
	void setVisited(glm::vec2 location, bool isVisited);
	
	void setEntity(int x, int y, bool isEntity);
	void setEntity(glm::vec2 location, bool isEntity);

	std::vector<Tile> getNeighbors(Tile node);

	std::vector<Tile> getGridMap();
	int getWidth();
	int getHeight();


	void printWorldMap();



private:
	
	int width_;
	int height_;
	std::vector<Tile> gridTiles_;
};

#endif // ASSIGNMENT4_GRID_H