#ifndef ASSIGNMENT4_AIHERO_H
#define ASSIGNMENT4_AIHERO_H

#include "Entity.h"
#include "pbctp.h"
#include "SimulationSettings.h"

#include <string>
#include <stdio.h>
#include <stdlib.h>


#include "Grid.h"
#include "PathFinder.h"
#include "BehaviourTree.h"




//Hero has 9hp! (as max!)
class AIHero : public Entity
{
	friend class AI_HERO_NODE;
	friend class AIHero_BT_SeenGoblins;
	friend class AIHERO_BT_MoveInRange;
	friend class AIHero_BT_MeleeAttack;
	friend class AIHero_BT_TileLeftToSearch;
	friend class AIHero_BT_WalkToUnvisitedTile;
	friend class AIHero_BT_VICTORY;
public:
	//Constructors
	AIHero();

	//Deconstructor
	~AIHero();

	void update() override;
	virtual void print() final override;

	void setSharedMap(std::shared_ptr<Grid> worldMap);

	void clearDetection();
	void addEnemyDetected(std::shared_ptr<Entity> enemy);


private:
	void setUpBehaviourTree();

	void updateVisibleArea();
	void updateTargetLocation();
	
	std::shared_ptr<Grid> gridWorld_;
	PathFinder pathFinder_;
	bool gotPath;
	glm::vec2 targetLocation_;

	std::vector<glm::vec2> pathWay_;
	bool won_;


	std::shared_ptr<Selector> RootSelector_;

	std::vector<std::shared_ptr<Entity>> enemyList_;
	std::shared_ptr<Entity> targetedGoblin_;

	int lifeRegCooldown = 0;
};

class AI_HERO_NODE : public Node
{
public:
	void setHero(AIHero* hero)
	{
		hero_ = hero;
	};
protected:
	AIHero* hero_;
};

class AIHero_BT_SeenGoblins : public AI_HERO_NODE
{
public:
	virtual bool run() override;
private:
};

class AIHero_BT_MeleeAttack : public AI_HERO_NODE
{
public:
	virtual bool run() override;
private:
};

class AIHERO_BT_MoveInRange : public AI_HERO_NODE
{
public:
	virtual bool run() override;
private:
};

class AIHero_BT_TileLeftToSearch : public AI_HERO_NODE
{
public:
	virtual bool run() override;
private:
};

class AIHero_BT_WalkToUnvisitedTile : public AI_HERO_NODE
{
public:
	virtual bool run() override;
private:
};

class AIHero_BT_VICTORY : public AI_HERO_NODE
{
public:
	virtual bool run() override;
private:
};

#endif // ASSIGNMENT4_AIHERO_H