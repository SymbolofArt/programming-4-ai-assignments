/*****************************************************************************************
@Title:		PBs Custom Text Printer
@File:			pbctp.h
@Language:		C
@Version:		8.0
@Last Changed:	2013-10-15 02.00
@Author:		Pontus Berglund

@Purpse/Comments/tips:
The purose of the following code is to help with printing with color, animated text,
combined, and also to go to a spesefic point on the screen.

For update log check bottom (Swedish only)
*****************************************************************************************/
#ifndef PBCTP_H
#define PBCTP_H
#include <stdio.h>
#include <windows.h> 
#include <time.h> 

inline void wait(double speed)
{
	/* wait for the ammount of seconds inputed, 1 = 1second. */
	clock_t waiter;
	waiter = (clock_t)(clock() + speed * CLOCKS_PER_SEC);
	while (clock() < waiter) {}
}

inline void gotoxy(unsigned short x, unsigned short y)
{
	/*Move the programms "cursor" to x and y.*/
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD position = { x, y };
	SetConsoleCursorPosition(hConsole, position);
}

inline void colorp(char *text, int color)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hConsole, color);
	printf("%s", text);
	SetConsoleTextAttribute(hConsole, 7);

}

inline void anicolp(char *printme, double delay, int color)
{
	/*Prints a animated text that can have a color. Each character is delayd between eachother depedning on what delay is */
	unsigned int i;
	char cp[2] = { '\0' };

	for (i = 0; i < strlen(printme); i++)
	{
		cp[0] = printme[i];
		if (color != 0)
			colorp(cp, color);
		else
			printf("%c", cp[0]);
		wait(delay);
	}
}

inline void prutimol(char printme[], double delay, int color, int locx, int locy)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int printruns;
	int i, j;
	int printamount;
	char cp[2] = { '\0' };

	printruns = strlen(printme) / 2;
	for (i = 0; i <= printruns; i++)
	{
		gotoxy(locx - i, locy);
		printamount = i * 2;
		for (j = printamount; j >= 0; j--)
		{
			cp[0] = printme[(printruns + i) - j];
			if (color != 0)
				colorp(cp, color);
			else
				printf("%c", cp[0]);
		}
		wait(delay);
	}
}

inline void prinimol(char printme[], double delay, int color, int locx, int locy)
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	int printruns;
	int i, j;
	int last = strlen(printme);
	char cp[2] = { '\0' };
	printruns = strlen(printme) / 2;

	for (i = 1; i <= printruns; i++)
	{
		gotoxy(locx - printruns, locy);
		for (j = 0; j < i; j++)
		{
			cp[0] = printme[j];
			if (color != 0)
				colorp(cp, color);
			else
				printf("%c", cp[0]);

		}
		gotoxy(locx + printruns - i, locy);
		for (j = i; j > 0; j--)
		{
			cp[0] = printme[last - j];
			if (color != 0)
				colorp(cp, color);
			else
				printf("%c", cp[0]);

		}
		wait(delay);
	}
}

inline void printat(char *text, unsigned int xloc, unsigned int yloc)
{
	gotoxy(xloc, yloc);
	printf("%s", text);
}

inline void printatc(char *text, int color, unsigned int xloc, unsigned int yloc)
{
	gotoxy(xloc, yloc);
	colorp(text, color);
}

inline void aniat(char *text, double delay, unsigned int xloc, unsigned int yloc)
{
	gotoxy(xloc, yloc);
	anicolp(text, delay, 0);
}

inline void anicoat(char *text, double delay, int color, unsigned int xloc, unsigned int yloc)
{
	gotoxy(xloc, yloc);
	anicolp(text, delay, color);
}


/*******************************************
2013-10-13: Started loging,
2013-10-13: Added prutimol and prinimol.
2013-10-13 20.00 B�rjade desgina Advprint f�r att kombinera en massa av de olika till en enda.
2013-10-13 23:00 Tog bort funktionerna, "colorp", "textani", "textanicolor". D� advprint g�r alla deras arbete.
2013-10-15 : 02.00 Tog bort advprint och tog tillbaka simplare funktioner som �r enklare att tyda.
Gjorde sen funktioner som utnytjar alla de �vriga s� man enklare kan k�ra tv� av dem samtidigt.
tex printatc s� man slipper anv�nder gotoxy, colorp, tex

*******************************************/
#endif // PBCTP_H