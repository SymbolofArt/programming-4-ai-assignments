#include "MainSimulator.h"

#include "Goblin.h"
#include "GoblinShaman.h"
#include "SimulationSettings.h"

MainSimulator::MainSimulator()
{
	/* Empty */
}


MainSimulator::~MainSimulator()
{
	/* Empty */
}

void MainSimulator::run()
{
	initSystems();
	mainLoop();
}

void MainSimulator::initSystems()
{
	aiHero_ = std::make_shared<AIHero>();

	setUpConsole();
	loadLevel();

	aiHero_->setSharedMap(gridWorld_);
	for (auto &goblin : goblinList_)
	{
		goblin->setSharedMap(gridWorld_);
	}
}

void MainSimulator::loadLevel()
{
	std::string line;
	std::ifstream myfile("resources/mapData/levelData.txt");
	
	std::vector<bool> levelWallData;

	int yCord = 0;
	int xCord = 0;
	if (myfile.is_open())
	{
		while (std::getline(myfile, line))
		{
			xCord = 0;
			for (char& c : line)
			{
				switch (c)
				{
				case '#': 
					levelWallData.emplace_back(false);
					break;		
				case 'H':
					aiHero_->setLocation(glm::vec2(xCord, yCord));
					levelWallData.emplace_back(true);
					break;
				case 'G':
					goblinList_.emplace_back(std::make_shared<Goblin>(glm::vec2(xCord, yCord), aiHero_));
					levelWallData.emplace_back(true);
					break;
				case 'S':
					goblinList_.emplace_back(std::make_shared<GoblinShaman>(glm::vec2(xCord, yCord), aiHero_));
					levelWallData.emplace_back(true);
					break;
				default: 
					levelWallData.emplace_back(true); 
					break;
				}

				xCord++;
			}
			yCord++;
		}
		myfile.close();
	}
	else std::cout << "Unable to open file";

	gridWorld_ = std::make_shared<Grid>(xCord, yCord, levelWallData);

	for (auto &goblin : goblinList_)
	{
		gridWorld_->setEntity(goblin->getLocation(), true);
	}

}

void MainSimulator::setUpConsole()
{
	HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO     cursorInfo;
	GetConsoleCursorInfo(out, &cursorInfo);
	cursorInfo.bVisible = false; // set the cursor visibility
	SetConsoleCursorInfo(out, &cursorInfo);
}

void MainSimulator::mainLoop()
{
	/* FPS HANDLER */
	PotatoEngine::FpsLimiter fps_limiter;
	
	float currentTime = fps_limiter.getElapsedTime();
	system("cls");
	/* MAIN LOOP */
	

	while (runSimulation)
	{
		if (fps_limiter.getElapsedTime() - currentTime > SIMULATION_SPEED)
		{
			currentTime = fps_limiter.getElapsedTime();


			//Update stuff
			updateEntityDetection();

			//update hero
			aiHero_->update();

			//Update all goblins
			for (auto &goblin : goblinList_)
			{
				goblin->update();
			}


			//Check deaths
			checkDeath();

			/* After updating all, print all */

			//First print the map
			gridWorld_->printWorldMap();

			//The print over with the characters.
			aiHero_->print();
			
			//Print all goblins
			for (auto &goblin : goblinList_)
			{
				goblin->print();
			}
			
			if (aiHero_->getHealth() <= 0)
				runSimulation = false;
		}
	}
	gameOver();
}

void MainSimulator::updateEntityDetection()
{
	aiHero_->clearDetection();

	for (auto &goblin : goblinList_)
	{
		goblin->setVisible(false);
		//abs(location.x - goalLoc.x) + abs(location.y - goalLoc.y);

		if (abs(aiHero_->getLocation().x - goblin->getLocation().x) < AI_HERO_DETECTION_AREA/2
			&&
			abs(aiHero_->getLocation().y - goblin->getLocation().y) < AI_HERO_DETECTION_AREA/2)
		{
			goblin->setVisible(true);

			aiHero_->addEnemyDetected(goblin);

		}
	}

	for (auto &goblin : goblinList_)
	{
		goblin->cleanDetectionList();
		for (auto &otherGoblin : goblinList_)
		{
			if (goblin != otherGoblin)
			{
				if (abs(goblin->getLocation().x - otherGoblin->getLocation().x) < AI_GOBLIN_DETECTION_AREA / 2
					&&
					abs(goblin->getLocation().y - otherGoblin->getLocation().y) < AI_GOBLIN_DETECTION_AREA / 2)
				{

					goblin->addGoblinEntity(otherGoblin);
					
				}
			}
		}
	}



}

void MainSimulator::checkDeath()
{
	bool checkForDeath = true;

	while (checkForDeath)
	{
		checkForDeath = false;
		int i = 0;
		for (auto &goblin : goblinList_)
		{
			if (goblin->getHealth() <= 0)
			{
				gridWorld_->setEntity(goblin->getLocation(), false);
				goblinList_.erase(goblinList_.begin() + i);
				
				checkForDeath = true;
				break;
			}
			i++;
			
			
		}
	}
}

void MainSimulator::gameOver()
{
	const double speed = 0.01;
	const int xCord = 60;
	const int yCord = 5;
	const int color = 240;
	prinimol("   _____          __  __ ______    ______      ________ _____  ", speed, color, xCord, yCord);
	prinimol("  / ____|   /\\   |  \\/  |  ____|  / __ \\ \\    / /  ____|  __ \\ ", speed, color, xCord, yCord + 1);
	prinimol(" | |  __   /  \\  | \\  / | |__    | |  | \\ \\  / /| |__  | |__) |", speed, color, xCord, yCord +2 );
	prinimol(" | | |_ | / /\\ \\ | |\\/| |  __|   | |  | |\\ \\/ / |  __| |  _  / ", speed, color, xCord, yCord + 3);
	prinimol(" | |__| |/ ____ \\| |  | | |____  | |__| | \\  /  | |____| | \\ \\ ", speed, color, xCord, yCord + 4);
	prinimol("  \\_____/_/    \\_\\_|  |_|______|  \\____/   \\/   |______|_|  \\_\\", speed, color, xCord, yCord + 5);
	prinimol("                                                               ", speed, color, xCord, yCord + 6);

	while (true)
	{

	}
}
