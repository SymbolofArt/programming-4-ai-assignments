#ifndef ASSIGNMENT4_MAINSIMULATOR_H
#define ASSIGNMENT4_MAINSIMULATOR_H

#include <iostream>
#include <fstream>
#include <string>

#include <PotatoEngine/Timing.h>

#include "pbctp.h"
#include "AIHero.h"
#include "BaseGoblin.h"

class MainSimulator
{
public:
	//Constructors
	MainSimulator();
	//Deconstructor
	~MainSimulator();

	/// <summary>
	/// Starts and run the simulation
	/// </summary>
	/// <returns></returns>
	void run();

private:
	void initSystems();
	void loadLevel();
	void setUpConsole();

	void mainLoop();

	void updateEntityDetection();

	void checkDeath();

	void gameOver();

	//Variables
	std::shared_ptr<Grid> gridWorld_;

	bool runSimulation = true;


	std::shared_ptr<AIHero> aiHero_;

	std::vector<std::shared_ptr<BaseGoblin>> goblinList_;


};


#endif // ASSIGNMENT4_MAINSIMULATOR_H