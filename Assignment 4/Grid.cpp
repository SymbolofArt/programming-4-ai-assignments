﻿#include "Grid.h"

Grid::Grid()
{
	/* Empty*/
}

Grid::Grid(int width, int height, std::vector<bool> gridData)
	:
	width_(width),
	height_(height)
{
	//Create all nodes.
	for (int y = 0; y < height_; y++)
		for (int x = 0; x < width_; x++)
			gridTiles_.emplace_back(x, y, gridData.at(width_ * y + x));

}

Tile Grid::getTileAt(int x, int y)
{
	return gridTiles_.at(width_ * y + x);
}

Tile Grid::getTileAt(glm::vec2 location)
{
	return getTileAt((int)location.x, (int)location.y);
}

bool Grid::isWalkableAt(int x, int y)
{
	return isInsideGrid(x, y) && gridTiles_.at(width_ * y + x).walkable;
}

bool Grid::isWalkableAt(glm::vec2 location)
{
	return isWalkableAt((int)location.x, (int)location.y);
}

bool Grid::isVisible(int x, int y)
{
	return isInsideGrid(x, y) && gridTiles_.at(width_ * y + x).visible;
}

bool Grid::isVisible(glm::vec2 location)
{
	return isVisible((int)location.x, (int)location.y);
}

bool Grid::isVisited(int x, int y)
{
	return isInsideGrid(x, y) && gridTiles_.at(width_ * y + x).visited;
}

bool Grid::isVisited(glm::vec2 location)
{
	return isVisited((int)location.x, (int)location.y);
}

bool Grid::isEntity(int x, int y)
{
	return isInsideGrid(x, y) && gridTiles_.at(width_ * y + x).entity;
}

bool Grid::isEntity(glm::vec2 location)
{
	return isEntity((int)location.x, (int)location.y);
}

bool Grid::isInsideGrid(int x, int y)
{
	return (x >= 0 && x < width_) && (y >= 0 && y < height_);
}

bool Grid::isInsideGrid(glm::vec2 location)
{
	return isInsideGrid((int)location.x, (int)location.y);
}

void Grid::setIsWalkableAt(int x, int y, bool isWalkable)
{
	if (isInsideGrid(x, y))
		gridTiles_.at(width_ * y + x).walkable = isWalkable;
}

void Grid::setIsWalkableAt(glm::vec2 location, bool isWalkable)
{
	setIsWalkableAt((int)location.x, (int)location.y, isWalkable);
}

void Grid::setVisible(int x, int y, bool isVisible)
{
	if (isInsideGrid(x, y))
		gridTiles_.at(width_ * y + x).visible = isVisible;
}

void Grid::setVisible(glm::vec2 location, bool isVisible)
{
	setVisible((int)location.x, (int)location.y, isVisible);
}

void Grid::setVisited(int x, int y, bool isVisited)
{
	if (isInsideGrid(x, y))
		gridTiles_.at(width_ * y + x).visited = isVisited;
}

void Grid::setVisited(glm::vec2 location, bool isVisited)
{
	setVisited((int)location.x, (int)location.y, isVisited);
}

void Grid::setEntity(int x, int y, bool isEntity)
{
	if (isInsideGrid(x, y))
		gridTiles_.at(width_ * y + x).entity = isEntity;
}

void Grid::setEntity(glm::vec2 location, bool isEntity)
{
	setEntity((int)location.x, (int)location.y, isEntity);
}

std::vector<Tile> Grid::getNeighbors(Tile tile)
{
	int x = tile.location.x;
	int y = tile.location.y;
	std::vector<Tile> neighbors;

	// ↑
	if (isWalkableAt(x, y - 1))
		neighbors.push_back(getTileAt(y - 1, x));
	// →
	if (isWalkableAt(x + 1, y))
		neighbors.push_back(getTileAt(y, x + 1));
	// ↓
	if (isWalkableAt(x, y + 1))
		neighbors.push_back(getTileAt(y + 1, x));
	// ←
	if (isWalkableAt(x - 1, y))
		neighbors.push_back(getTileAt(y, x - 1));

	//Digional
	// ↖
	if (isWalkableAt(x - 1, y - 1))
		neighbors.push_back(getTileAt(y - 1, x - 1));
	// ↗
	if (isWalkableAt(x + 1, y - 1))
		neighbors.push_back(getTileAt(y - 1, x + 1));
	// ↘
	if (isWalkableAt(x + 1, y + 1))
		neighbors.push_back(getTileAt(y + 1, x + 1));
	// ↙
	if (isWalkableAt(x - 1, y + 1))
		neighbors.push_back(getTileAt(y + 1, x - 1));

	//return all neighbors
	return neighbors;
}

std::vector<Tile> Grid::getGridMap()
{
	return gridTiles_;
}

int Grid::getWidth()
{
	return width_;
}

int Grid::getHeight()
{
	return height_;
}

void Grid::printWorldMap()
{
	for (int y = 0; y < height_; y++)
	{
		for (int x = 0; x < width_; x++)
		{
			if (isVisible(x, y))
			{
				if (isWalkableAt(x, y))
				{
					if(isEntity(x, y) == false)
						if (isVisited(x, y))
							printatc(".", GRID_VISITED_FLOOR, x, y);
						else
							printatc(".", GRID_FLOOR, x, y);
				}
				else
				{
					printatc("#", GRID_WALL_COLOR, x, y);
				}
			}

		}
	}
}



