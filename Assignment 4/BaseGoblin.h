#ifndef ASSIGNMENT4_BASEGOBLIN_H
#define ASSIGNMENT4_BASEGOBLIN_H

#include "Entity.h"
#include "pbctp.h"
#include "SimulationSettings.h"
#include "Grid.h"
#include "PathFinder.h"
#include "BehaviourTree.h"

class BaseGoblin : public Entity
{
public:
	void setSharedMap(std::shared_ptr<Grid> worldMap)
	{
		gridWorld_ = worldMap;
		pathFinder_ = PathFinder(worldMap);
	}

	void setVisible(bool isVisible)
	{
		visible_ = isVisible;
	}

	void cleanDetectionList()
	{
		friendList_.clear();
	}
	void addGoblinEntity(std::shared_ptr<BaseGoblin> goblinEntity)
	{

		friendList_.push_back(goblinEntity);
	}

protected:
	virtual void setUpBehaviourTree() = 0;

	std::shared_ptr<Grid> gridWorld_;
	std::vector<glm::vec2> pathWay_;
	PathFinder pathFinder_;

	bool visible_;

	std::vector<std::shared_ptr<BaseGoblin>> friendList_;
	std::shared_ptr<Entity> aiHero_;

	//Behaviour root
	std::shared_ptr<Selector> RootSelector_;

};

#endif // ASSIGNMENT4_BASEGOBLIN_H