#ifndef ASSIGNMENT4_GOBLIN_H
#define ASSIGNMENT4_GOBLIN_H

#include "BaseGoblin.h"
#include "BehaviourTree.h"

class Goblin : public BaseGoblin
{
	friend class AI_GOBLIN_NODE;
	friend class AIGoblin_BT_SeenHero;
	friend class AIGoblin_BT_MoveToTarget;
	friend class AIGoblin_BT_MeleeAttack;
	friend class AIGoblin_BT_SeenEnragedGoblin;
	friend class AIGoblin_BT_MoveEnragedGoblin;
public:
	//Constructors
	Goblin();
	Goblin(glm::vec2 location, std::shared_ptr<Entity> hero);
	//Deconstructors
	~Goblin();


	void update() override;
	void print() override;

private:
	void setUpBehaviourTree() override;

	glm::vec2 targetLoc_;
	glm::vec2 heroLoc_;

	//Behaviour root
	std::shared_ptr<Selector> RootSelector_;
};


class AI_GOBLIN_NODE : public Node
{
public:
	void setOwner(Goblin* owner);
protected:
	Goblin* owner_;
};

class AIGoblin_BT_SeenHero : public AI_GOBLIN_NODE
{
public:
	virtual bool run() override;
private:
};

class AIGoblin_BT_MoveToTarget : public AI_GOBLIN_NODE
{
public:
	virtual bool run() override;
private:
};

class AIGoblin_BT_MeleeAttack : public AI_GOBLIN_NODE
{
public:
	virtual bool run() override;
private:
	int attackCD_ = 0;
};

class AIGoblin_BT_SeenEnragedGoblin : public AI_GOBLIN_NODE
{
public:
	virtual bool run() override;
private:
};

class AIGoblin_BT_MoveEnragedGoblin : public AI_GOBLIN_NODE
{
public:
	virtual bool run() override;
private:
};

#endif // ASSIGNMENT4_GOBLIN_H