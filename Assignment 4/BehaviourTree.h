#ifndef ASSIGNMENT4_BEHAVIOURTREE_H
#define ASSIGNMENT4_BEHAVIOURTREE_H

#include <iostream>
#include <memory>
#include <vector>
#include <functional>

//An simple node that can be runed.
class Node
{
public:
	virtual bool run() = 0; //Return true if action was successfully, else false.
};

//Parents of diffrent nodes. used by selector and sequencer.
class NodeParent : public Node
{
public:
	std::vector<std::shared_ptr<Node>> getChildren();
	void addChild(std::shared_ptr<Node> child);



private:
	//std::vector<Node*> childrens;
	std::vector<std::shared_ptr<Node>> childrens;
};


class Selector : public NodeParent
{
public:
	virtual bool run() override;
};

class Sequence : public NodeParent
{
public:
	virtual bool run() override;

private:
};


#endif // ASSIGNMENT4_BEHAVIOURTREE_H