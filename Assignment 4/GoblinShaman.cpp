#include "GoblinShaman.h"

GoblinShaman::GoblinShaman()
{
}

GoblinShaman::GoblinShaman(glm::vec2 location, std::shared_ptr<Entity> hero)
{
	location_ = location;
	aiHero_ = hero;
	health_ = AI_GOBLINSHAMAN_MAXHEALTH;
	maxHealth_ = AI_GOBLINSHAMAN_MAXHEALTH;

	setUpBehaviourTree();
}

void GoblinShaman::setUpBehaviourTree()
{
	//Main selector
	RootSelector_ = std::make_shared<Selector>();
	
	/*HEALING SEQUENCE*/
	std::shared_ptr<Sequence> healingSequence = std::make_shared<Sequence>();
	RootSelector_->addChild(healingSequence);

	std::shared_ptr<AI_GS_BT_SeenDamagedGoblin> SeenDamagedGoblin = std::make_shared<AI_GS_BT_SeenDamagedGoblin>();
	healingSequence->addChild(SeenDamagedGoblin);
	std::shared_ptr<AI_GS_BT_HealDamagedGoblin> HealGoblin = std::make_shared<AI_GS_BT_HealDamagedGoblin>();
	healingSequence->addChild(HealGoblin);

	/*ATTACK SEQUENCE*/
	std::shared_ptr<Sequence> attackSequence = std::make_shared<Sequence>();
	RootSelector_->addChild(attackSequence);

	std::shared_ptr<AI_GS_BT_SeenHero> SeenHero = std::make_shared<AI_GS_BT_SeenHero>();
	attackSequence->addChild(SeenHero);
	std::shared_ptr<AI_GS_BT_CastFireball> castFireball = std::make_shared<AI_GS_BT_CastFireball>();
	attackSequence->addChild(castFireball);

	SeenDamagedGoblin->setOwner(this);
	HealGoblin->setOwner(this);
	SeenHero->setOwner(this);
	castFireball->setOwner(this);

}

GoblinShaman::~GoblinShaman()
{
	/* EMPTY */
}

void GoblinShaman::update()
{
	actionCD++;
	RootSelector_->run();
}

void GoblinShaman::print()
{
	if (visible_)
		switch (health_)
		{
		case 9: printatc("9", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 8: printatc("8", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 7: printatc("7", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 6: printatc("6", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 5: printatc("5", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 4: printatc("4", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 3: printatc("3", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 2: printatc("2", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		case 1: printatc("1", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		default: printatc("?", AI_GOBLINSHAMAN_COLOR, (int)location_.x, (int)location_.y); break;
		}
}

void AI_GS_NODE::setOwner(GoblinShaman * owner)
{
	owner_ = owner;
}

bool AI_GS_BT_SeenDamagedGoblin::run()
{
	if (owner_->friendList_.size() > 0)
	{
		for (auto &goblin : owner_->friendList_)
		{
			if (goblin->isDamaged())
			{
				owner_->targetGoblin_ = goblin;
				return true;
			}
		}
	}
	
	return false;
}

bool AI_GS_BT_HealDamagedGoblin::run()
{
	if (owner_->actionCD >= AI_GOBLINSHAMAN_ACTION_CD)
	{
		owner_->actionCD = 0;
		owner_->targetGoblin_->heal(AI_GOBLINSHAMAN_HEAL_AMOUNT);
	}
	return false;
}

bool AI_GS_BT_SeenHero::run()
{
	if (abs(owner_->getLocation().x - owner_->aiHero_->getLocation().x)
		+ abs(owner_->getLocation().y - owner_->aiHero_->getLocation().y) < AI_GOBLIN_DETECTION_AREA / 2)
	{
		owner_->setIsInCombat(true);
		return true;
	}
	else
	{
		owner_->setIsInCombat(false);
		return false;
	}
}

bool AI_GS_BT_CastFireball::run()
{
	if (owner_->actionCD >= AI_GOBLINSHAMAN_ACTION_CD)
	{
		owner_->actionCD = 0;
		owner_->aiHero_->dealDamage(AI_GOBLINSHAMAN_DAMAGE);
	}
	return true;
}
