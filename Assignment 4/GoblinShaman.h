#ifndef ASSIGNMENT4_GOBLINSHAMAN_H
#define ASSIGNMENT4_GOBLINSHAMAN_H


#include "Entity.h"
#include "pbctp.h"
#include "SimulationSettings.h"
#include "Grid.h"
#include "PathFinder.h"
#include "BehaviourTree.h"
#include "BaseGoblin.h"

class GoblinShaman : public BaseGoblin
{
	friend class AI_GS_BT_SeenDamagedGoblin;
	friend class AI_GS_BT_HealDamagedGoblin;
	friend class AI_GS_BT_SeenHero;
	friend class AI_GS_BT_CastFireball;
public:
	//Constructors
	GoblinShaman();
	GoblinShaman(glm::vec2 location, std::shared_ptr<Entity> hero);
	
	//Deconstructors
	~GoblinShaman();

	void update() override;
	void print() override;


private:
	void setUpBehaviourTree();

	std::shared_ptr<BaseGoblin> targetGoblin_;

	int actionCD = 0;

	//Behaviour root
	std::shared_ptr<Selector> RootSelector_;
};

class AI_GS_NODE : public Node
{
public:
	void setOwner(GoblinShaman* owner);
protected:
	GoblinShaman* owner_;
};

class AI_GS_BT_SeenDamagedGoblin : public AI_GS_NODE
{
public:
	virtual bool run() override;
protected:
};

class AI_GS_BT_HealDamagedGoblin : public AI_GS_NODE
{
public:
	virtual bool run() override;
protected:
};

class AI_GS_BT_SeenHero : public AI_GS_NODE
{
public:
	virtual bool run() override;
protected:
};

class AI_GS_BT_CastFireball : public AI_GS_NODE
{
public:
	virtual bool run() override;
protected:
};

#endif // ASSIGNMENT4_GOBLINSHAMAN_H