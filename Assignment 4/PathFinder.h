#ifndef ASSIGNMENT4_PATHFINDER_H
#define ASSIGNMENT4_PATHFINDER_H
#include <glm\glm.hpp>
#include <iostream>
#include <memory>
#include <vector>
#include <functional>

#include "Grid.h"
#include "SimulationSettings.h"

//Will be done in order. Wait. stop!
struct PathWay
{
	glm::vec2 location;
};

struct SearchNode
{
public:
	
	std::shared_ptr<SearchNode> parent;
	glm::vec2 location;

	bool isRock;
	bool isClosed;
	bool isPath;
	int GCost; //Gcost = distance from starting nod
	int HCost; //H cost (heuristic) = distance from end node.
	int FCost; //Gcost + Hcost
	
	int tileID;
	int retrackChildID;


	SearchNode(glm::vec2 loc, bool IsRock, int id)
	{
		location = loc;
		isRock = IsRock;
		isClosed = false;
		isPath = false;
		parent = nullptr;
		GCost = 0;
		HCost = 0;
		FCost = 0;
		tileID = id;
		retrackChildID = -1;
	}
	
	///Create path
	void createPath(int RetrackChild = -1)
	{
		if (isPath)
			return;

		retrackChildID = RetrackChild;
		isPath = true;
		if (parent != nullptr)
			parent->createPath(tileID);
	}

	void generateSearchData(glm::vec2 goalLoc, std::shared_ptr<SearchNode> Parent, int GCOST)
	{
		/*if (parent != nullptr)
		return;*/
		parent = Parent;
		HCost = 0;
		//Get HCost_
		glm::vec2 tmpLoc = location;

		while (tmpLoc.x != goalLoc.x || tmpLoc.y != goalLoc.y)
		{
			//Up Right
			if (tmpLoc.x < goalLoc.x && tmpLoc.y < goalLoc.y)
			{
				tmpLoc += glm::vec2(1, 1);
				HCost += 14;
			}// Down Right
			else if (tmpLoc.x < goalLoc.x && tmpLoc.y > goalLoc.y)
			{
				tmpLoc += glm::vec2(1, -1);
				HCost += 14;
			} //Left Up
			else if (tmpLoc.x > goalLoc.x && tmpLoc.y < goalLoc.y)
			{
				tmpLoc += glm::vec2(-1, 1);
				HCost += 14;
			}//Left Down
			else if (tmpLoc.x > goalLoc.x && tmpLoc.y > goalLoc.y)
			{
				tmpLoc += glm::vec2(-1, -1);
				HCost += 14;
			}//UP
			else if (tmpLoc.y < goalLoc.y)
			{
				tmpLoc += glm::vec2(0, 1);
				HCost += 10;
			}//Down
			else if (tmpLoc.y > goalLoc.y)
			{
				tmpLoc += glm::vec2(0, -1);
				HCost += 10;
			}//Right
			else if (tmpLoc.x < goalLoc.x)
			{
				tmpLoc += glm::vec2(1, 0);
				HCost += 10;
			}//Left
			else if (tmpLoc.x > goalLoc.x)
			{
				tmpLoc += glm::vec2(-1, 0);
				HCost += 10;
			}
		}

		GCost = GCOST;
		FCost = HCost + GCost;
	}

};

class PathFinder
{
public:
	//Constructor
	PathFinder();
	PathFinder(std::shared_ptr<Grid> sharedGridWorld);

	//Deconstructor
	~PathFinder();

	//return a vector of walking direction from startlocation to target location.
	std::vector<glm::vec2> FindPath(glm::vec2 startLocation, glm::vec2 targetLocation);

private:

	void searchPath();
	void checkNeighbors();
	int getGCost(glm::vec2 a, glm::vec2 b);



	std::shared_ptr<Grid> gridWorld_;

	std::vector<Tile> storedGridMap;
	glm::vec2 startLocation_;
	glm::vec2 targetLocation_;

	std::vector<std::shared_ptr<SearchNode>> searchNodes_;
	std::vector<std::shared_ptr<SearchNode>> openList_;

	std::shared_ptr<SearchNode> currentNode_;
	std::shared_ptr<SearchNode> startNode_;
	
	
	bool needsToGenerateFirstTile;
	bool foundPath_;
	bool isStuck_;
	bool openListEmpty_;




};

#endif // ASSIGNMENT4_PATHFINDER_H