#include "BehaviourTree.h"


bool Selector::run()
{
	/*Goes true all children and run them one by one untill one succeds.
	When one node succeeds, return true and stop going true the nodes. 
	If no nodes are succeded return false.
	*/

	for (std::shared_ptr<Node> child : getChildren())
	{
		if (child->run())
			return true;
	}

	return false;
}

std::vector<std::shared_ptr<Node>> NodeParent::getChildren()
{
	return childrens;
}

void NodeParent::addChild(std::shared_ptr<Node> child)
{
	childrens.emplace_back(child);
}

bool Sequence::run()
{
	/*Goes true all  nodes one by one, if someone failes to run and return false,
	stop the sequence and return false. If it goes true all nodes without problem, return true.*/
	for (std::shared_ptr<Node> child : getChildren())
	{
		if (child->run() == false)
		{
		return false;
}
	}

	return true;
}
