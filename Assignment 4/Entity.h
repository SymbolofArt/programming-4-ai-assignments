#ifndef ASSIGNMENT4_ENTITY_H
#define ASSIGNMENT4_ENTITY_H
#include <glm\glm.hpp>

class Entity
{
public:

	virtual void update() = 0;
	virtual void print() = 0;


	void setLocation(glm::vec2 newLocation) { location_ = newLocation; }
	glm::vec2 getLocation() { return location_; }

	void setHealth(short newHealth) { health_ = newHealth; }
	short getHealth() { return health_; }

	void dealDamage(short damage)
	{
		if (health_ > damage)
			health_ -= damage;
		else
			health_ = 0;
	}

	void heal(short heal)
	{
		health_ += heal;
		if (health_ > maxHealth_)
			health_ = maxHealth_;
	}

	bool isInCombat() 
	{
		return isInCombat_;
	}

	void setIsInCombat(bool isInCombat)
	{
		isInCombat_ = isInCombat;
	}

	bool isDamaged()
	{
		return (health_ < maxHealth_);
	}
protected:

	glm::vec2 location_;
	short health_;
	short maxHealth_;
	bool isInCombat_;
	//Drawing stuff

};


#endif // ASSIGNMENT4_ENTITY_H

