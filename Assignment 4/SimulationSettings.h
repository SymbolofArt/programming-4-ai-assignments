#pragma once

/* SIMULATION SETTINGS */
#define SIMULATION_SPEED			100											///1000 = 1 sec. close enough


/* GRID SETTINGS */
#define GRID_SIZE_HEIGHT			16											///Height of the simulation in nmumber of tiles
#define GRID_SIZE_WIDTH				64											///Width of the simulation in number of tiles.
#define GRID_SIZE					(GRID_SIZE_HEIGHT * GRID_SIZE_WIDTH)		///Total number of tiles,		---DO NOT CHANGE THIS VALUE!!-----



/* AI HERO SETTINGS */
#define AI_HERO_MAXHEALTH			9		//max 9.

#define AI_HERO_DETECTION_AREA		10
#define AI_HERO_HP_REG_ENABLE		1
#define AI_HERO_HP_REG_CD			10

/* AI BASE GOBLIN SETTINGS */
#define AI_GOBLIN_DETECTION_AREA	10

/*AI GOBLIN SETTINGS */
#define AI_GOBLIN_MAXHEALTH			5
#define AI_GOBLIN_ATTACK_CD			2	//each X run, it will deal damage.



/*AI GOBLIN SHAMAN SETTINGS */
#define AI_GOBLINSHAMAN_MAXHEALTH			2

#define AI_GOBLINSHAMAN_HEAL_AMOUNT			2
#define AI_GOBLINSHAMAN_ACTION_CD			3	//each X run, it will eithr heal or dmg

#define AI_GOBLINSHAMAN_DAMAGE				1

/* COLOR SETTINGS */
#define GRID_WALL_COLOR						136
#define GRID_FLOOR							47
#define GRID_VISITED_FLOOR					34

#define AI_HERO_COLOR						31
#define AI_GOBLIN_COLOR						79
#define AI_GOBLINSHAMAN_COLOR				95