#ifndef ASSIGNMENT1_SHEEP_H
#define ASSIGNMENT1_SHEEP_H

#include <iostream>
#include <memory>
#include <vector>
#include <functional>
#include <PotatoEngine\ResourceManager.h>

#include "Entity.h"
#include "SimulationSettings.h"



class Wolf;
class Grass;
//enum SheepBehaviour
//{
//	EVADE,
//	EAT_GRASS,
//	MORE_SHEEP,
//	SEEK,
//	WANDER
//};	
enum SheepState
{
	EVADE,
	EAT_GRASS,
	MORE_SHEEP,
	SEEK,
	WANDER,
	INDER_EVADE,
	IDLE
};

class Sheep : public Entity
{
public:
	//Constructors
	Sheep();
	Sheep(PotatoEngine::ResourceManager* resManager, glm::vec2 loc, char health = SHEEP_START_HEALTH);
	
	//Deconstructor
	~Sheep();

	void update() override;
	void draw(PotatoEngine::SpriteBatch * spriteBatch) override;

	SheepState getCurrentState() { return currentState_; }
	void setCurrentState(SheepState newState) { currentState_ = newState; }

	//Functions for handling detection
	void cleanDetectionData();
	void addSeenGrass(std::shared_ptr<Grass> seenGrass);
	void addSeenSheep(std::shared_ptr<Sheep> seenSheep);
	void addSeenWolf(std::shared_ptr<Wolf> seenWolf);

private:

	glm::vec2 getRandomMovDir();

	//State handling
	void updateState();
	void stateEvade();
	void stateInderEvade();
	void stateEatGrass();
	void stateMoreSheep();
	void stateSeek();
	void stateWander();
	void stateIdle();

	void increaseStamina();
	//Variables

	int stamina_;
	int wool_;

	//Points to detected stuff.
	std::shared_ptr<Grass> standingGrass_;
	std::vector<std::shared_ptr<Grass>> detectedGrass_;
	std::vector<std::shared_ptr<Sheep>> detectedSheep_;
	std::vector<std::shared_ptr<Wolf>> detectedWolfs_;

	//location of cloest wolf.
	glm::vec2 locOfClosedWolf_;

	glm::vec2 locOfClosedFleeingSheep_;
	

	//AI STUFF
	SheepState currentState_;

	SheepState currentBehaviour_ = SheepState::WANDER;
	glm::vec2 movementGoal_ = glm::vec2(0);
	bool foundGrass_ = false;
};

#endif // ASSIGNMENT1_SHEEP_H

