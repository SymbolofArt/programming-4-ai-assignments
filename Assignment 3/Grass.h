#ifndef ASSIGNMENT1_GRASS_H
#define ASSIGNMENT1_GRASS_H

#include <iostream>
#include <memory>
#include <vector>
#include <functional>
#include <PotatoEngine\ResourceManager.h>

#include "Entity.h"
#include "SimulationSettings.h"



class Grass : public Entity
{
public:
	//Constructors
	Grass();
	Grass(PotatoEngine::ResourceManager* resManger, glm::vec2 worldsize, glm::vec2 loc, char health = 0); //Health == 0 == no grass, its dirt!
	
	//Deconstructor																					  //Deconstructors
	~Grass();


	void update() override;
	void draw(PotatoEngine::SpriteBatch * spriteBatch) override;

	void addNeighbor(std::shared_ptr<Grass> neighbor);

	bool spreadGrassToMe(char health = 1);

	bool hasGrass() { return health_ > 0; }

	void dealDamage(short amount); ///We sort of overule this, as we want to change some extra stuff when grass reach 0 hp

private:
	enum GrassState
	{
		DEAD,
		DYING,
		GROWING,
		MATURE_GROWING,
		MATURE_DYING
	};
	void spreadGrass();


	//State handling
	void updateState();
	void dying();
	void growing();
	void matureGrowing();
	void matureDying();


	//Variables
	GrassState currentState_;
	PotatoEngine::ResourceManager* resourceManger_;
	

	//pointers to its neighbors so it easiler can spread.
	std::vector<std::shared_ptr<Grass>> neighbors_;

};

#endif // ASSIGNMENT1_GRASS_H


