#ifndef ASSIGNMENT1_MAINSIMULATOR_H
#define ASSIGNMENT1_MAINSIMULATOR_H

#include <PotatoEngine/Timing.h>

#include "System.h"
#include "LiveState.h"

#define SIMULATION_SPEED  1

class MainSimulator
{
public:
	//Constructors
	MainSimulator();
	//Deconstructor
	~MainSimulator();
	
	/// <summary>
	/// Starts and run the simulation
	/// </summary>
	/// <returns></returns>
	void run();

private:
	void initSystems();
	void resourceLoader();

	void switchState();
	void processInput();

	void mainLoop();



	//Member variables
	System system_;										///Keeps pointer to all system variables. (tex window_)
	SimulationStates simulationState_;					///Simulationstate enum, keeps state info
	sf::RenderWindow window_;							///The game window
	sf::View cameraView_;								///Camera view... :)
	PotatoEngine::GLSLProgram textureProgram_;			///The shader program
	PotatoEngine::InputManager inputManager_;			///Handles input
	PotatoEngine::ResourceManager resourceManager_;		///Resource manager.
	PotatoEngine::AudioManager audioManager_;			///Manages sound effects and music.
	PotatoEngine::CollisionManager collisionManager_;	///Handles all collision checks
	PotatoEngine::AnimationManager animationManager_;
	SimulationState *currentState_ = nullptr;			///State structor
	
	float fps_;
	std::string gameDictionary_;

	

};
#endif // ASSIGNMENT1_MAINSIMULATOR_H

