#ifndef ASSIGNMENT1_LIVESTATE_H
#define ASSIGNMENT1_LIVESTATE_H
#include "SimulationState.h"
#include "GridWorld.h"
#include "System.h"
#include "SimulationSettings.h"

#include <PotatoEngine\Converter.h>
class LiveState :
	public SimulationState
{
public:
	//Constructors
	LiveState();
	LiveState(System* system, float simulationSpeed = 6);
	//Deconstructor
	~LiveState();

	void update(float deltaTime);
	void draw();
private:
	float simulationTimer_ = 0;

	//variables
	System* system_;
	sf::View camera_;
	PotatoEngine::SpriteBatch spriteBatch_;

	sf::Texture* testLevelTexture_;

	GridWorld world_;
};
#endif // ASSIGNMENT1_LIVESTATE_H
