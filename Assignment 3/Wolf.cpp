#include "Wolf.h"
#include "Sheep.h"

Wolf::Wolf()
{
	/*Empty*/
}

Wolf::Wolf(PotatoEngine::ResourceManager * resManager, glm::vec2 loc, char health)
{
	loc_ = loc;
	health_ = health;
	//Get data for later use, while printing. (sim is 1920x1080 simulated)

	//image/draw stuff
	textureSize_ = sf::IntRect(0, 0, 16, 16);
	textureColor_ = sf::Color(255, 255, 255, 255);
	imageTexture_ = resManager->getTexture("resources/textures/Wolf.png");
}

Wolf::~Wolf()
{
	cleanDetectionData();
}

void Wolf::update()
{
	//First eat sheep, then create more wolfs, then pursue a sheep and last walk around.
	updateClosestSheep();

	updateState();
	switch (currentState_)
	{
	case EAT_SHEEP: stateEatSheep(); break;
	case MORE_WOLVES: stateMoreWolves(); break;
	case PURSUE: statePursue(); break;
	case WALK_AROUND: stateWalkAround(); break;
	}
	health_ = health_ - WOLF_HEALTH_DECAY;
	if (health_ < 0)
	{
		health_ = 0;
	}
}

void Wolf::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	drawArea_ = sf::FloatRect(
		loc_.x * (1920 / GRID_SIZE_WIDTH) - 0.1f,
		loc_.y * (1080 / GRID_SIZE_HEIGHT) - 0.1f,
		(1920 / GRID_SIZE_WIDTH) + 0.2f,
		(1080 / GRID_SIZE_HEIGHT) + 0.2f);

	spriteBatch->draw(
		imageTexture_,
		drawArea_,
		textureSize_,
		textureColor_);
}

void Wolf::cleanDetectionData()
{
	detectedSheep_.clear();
	detectedWolfs_.clear();
}

void Wolf::addSeenSheep(std::shared_ptr<Sheep> seenSheep)
{
	detectedSheep_.push_back(seenSheep);
}

void Wolf::addSeenWolf(std::shared_ptr<Wolf> seenWolf)
{
	detectedWolfs_.push_back(seenWolf);
}

glm::vec2 Wolf::getRandomMovDir()
{
	glm::vec2 randomLoc = glm::vec2(0);

	bool searchingForSpace = true;
	int testes = 0;

	while (searchingForSpace && testes < 8)
	{
		//y
		if (loc_.y == 0)
			randomLoc.y = std::rand() % 2;
		else if (loc_.y == GRID_SIZE_HEIGHT - 1)
			randomLoc.y = std::rand() % 2 - 1;
		else
			randomLoc.y = std::rand() % 3 - 1;
		//X
		if (loc_.x == 0)
			randomLoc.x = std::rand() % 2;
		else if (loc_.x == GRID_SIZE_WIDTH - 1)
			randomLoc.x = std::rand() % 2 - 1;
		else
			randomLoc.x = std::rand() % 3 - 1;


		testes++;

		searchingForSpace = false;
		for (auto &wolf : detectedWolfs_)
		{
			if (wolf->getLocation() == (loc_ + randomLoc))
			{
				searchingForSpace = true;
				continue;
			}
		}
	}

	if (testes >= 8)
	{
		randomLoc = glm::vec2(0);
	}

	return randomLoc;
}

void Wolf::updateClosestSheep()
{
	foundSheep_ = false;
	bool wolfIsAtSpot = false;
	for (auto &sheep : detectedSheep_)
	{
		for (auto &wolf : detectedWolfs_)
		{
			if (wolf->getLocation() == sheep->getLocation())
			{
				wolfIsAtSpot = true;
				break;
			}
		}
		if (wolfIsAtSpot)
			break;
		else if (foundSheep_ == false)
		{
			foundSheep_ = true;
			cloestSheepLoc = sheep->getLocation();
			cloestSheep_ = sheep;
		}
		else if (glm::distance(cloestSheepLoc, loc_) >=
			glm::distance(sheep->getLocation(), loc_))
		{
			cloestSheepLoc = sheep->getLocation();
			cloestSheep_ = sheep;
		}
	}
}

void Wolf::updateState()
{
	if (foundSheep_ == true && cloestSheepLoc == loc_)
	{
		currentState_ = EAT_SHEEP;
	}
	else if (health_ >= WOLF_MAX_HEALTH - WOLF_HEALTH_DECAY)
	{
		currentState_ = MORE_WOLVES;
	}
	else if (foundSheep_)
	{
		currentState_ = PURSUE;
	}
	else
	{
		currentState_ = WALK_AROUND;
	}
}

void Wolf::stateEatSheep()
{
	cloestSheep_->dealDamage(WOLF_DAMAGE_DONE);
	health_ = health_ + WOLF_HEALTH_GAIN;
	if (health_ > WOLF_MAX_HEALTH)
		health_ = WOLF_MAX_HEALTH;
}

void Wolf::stateMoreWolves()
{
	/*PRODUCE MORE SHEEP AND REMOVE HEALTH*/
	health_ -= WOLF_HEALTH_LOST_PRODUCE;
	//Production of new wolf is handled by the grid, 
	//as wolf does not handle the worlddata of wolfs.
}

void Wolf::statePursue()
{
	glm::vec2 moveDir = glm::vec2(0);
	bool wolfMoveCheck = true;
	/* X MOVEMENT */
	if (WOLF_DOUBLE_WALK_WN_PURSUE && loc_.x > cloestSheepLoc.x + 1 && loc_.x > 1)
		moveDir.x = -2;
	else if (WOLF_DOUBLE_WALK_WN_PURSUE && loc_.x < cloestSheepLoc.x - 1 && loc_.x < GRID_SIZE_WIDTH - 2)
		moveDir.x = 2;
	else if (loc_.x > cloestSheepLoc.x && loc_.x > 0)
		moveDir.x = -1;
	else if (loc_.x < cloestSheepLoc.x && loc_.x < GRID_SIZE_WIDTH - 1)
		moveDir.x = 1;

	/* Y MOVEMENT */
	if (WOLF_DOUBLE_WALK_WN_PURSUE && loc_.y > cloestSheepLoc.y + 1 && loc_.y > 1)
		moveDir.y = -2;
	else if (WOLF_DOUBLE_WALK_WN_PURSUE &&loc_.y < cloestSheepLoc.y - 1 && loc_.y < GRID_SIZE_HEIGHT - 2)
		moveDir.y = 2;
	else if (loc_.y > cloestSheepLoc.y && loc_.y > 0)
		moveDir.y = -1;
	else if (loc_.y < cloestSheepLoc.y && loc_.y < GRID_SIZE_HEIGHT - 1)
		moveDir.y = 1;

	for (auto &wolf : detectedWolfs_)
	{
		if (wolf->getLocation() == loc_ + moveDir)
		{
			wolfMoveCheck = false;
			break;
		}
	}

	if (wolfMoveCheck)
	{
		loc_ = loc_ + moveDir;
	}

	//Check if wolf is at the sheep, if yes, it also eats a bit! :)
	if (WOLF_WALK_ATTACK && loc_ == cloestSheepLoc)
	{
		cloestSheep_->dealDamage(WOLF_DAMAGE_DONE);
		health_ += WOLF_HEALTH_GAIN;
	}

}

void Wolf::stateWalkAround()
{
	loc_ = loc_ + getRandomMovDir();
}
