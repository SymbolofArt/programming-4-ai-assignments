#include "GridWorld.h"



GridWorld::GridWorld()
{
	//generateWorldData(1);
}

GridWorld::GridWorld(System* system)
	:
	system_(system)
{
	generateWorldData(SIMULATION_START_GRASS);
	generateSheep(SIMULATION_START_SHEEP);
	generateWolfs(SIMULATION_START_WOLF);
}


GridWorld::~GridWorld()
{
	/* EMPTY */
}

void GridWorld::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	for (auto &grass : sharedWorldData_)
	{
		grass->draw(spriteBatch);
	}

	for (auto &sheep : sharedWorldSheeps_)
	{
		sheep->draw(spriteBatch);
	}

	for (auto &wolf : sharedWorldWolfs_)
	{
		wolf->draw(spriteBatch);
	}
}

void GridWorld::update(float deltaTime)
{
	//Update each grass
	for (auto &grass : sharedWorldData_)
	{
		grass->update();
	}

	//Update sheeps and wolfs
	updateSheeps();
	updateWolfs();

	//Check for birth/death / the cycle of life
	checkForDeath();
	checkForBirth();

}

void GridWorld::updateSheeps()
{
	//Update all sheep
	for (auto &sheep : sharedWorldSheeps_)
	{
		//First clean its  detection data and readd all object it detects.
		sheep->cleanDetectionData();

		//SEE GRASS
		for (auto &grass : sharedWorldData_)
		{
			if (glm::distance(grass->getLocation(), sheep->getLocation()) < SHEEP_DETECTION_AREA)
			{
				sheep->addSeenGrass(grass);
			}
		}

		//SEE SHEEP
		for (auto &sheep2 : sharedWorldSheeps_)
		{
			if (sheep2->getLocation() != sheep->getLocation())
			{
				if (glm::distance(sheep2->getLocation(), sheep->getLocation()) < SHEEP_DETECTION_AREA)
				{
					sheep->addSeenSheep(sheep2);
				}
			}
		}

		//SEE WOLF (ONLY IF SHEEP IS *NOT* EATING; IF EATING; THEY DON'T SEE WOLFS; CAUSE THEY BLIND
		if (sheep->getCurrentState() != EAT_GRASS)
		{
			for (auto &wolf : sharedWorldWolfs_)
			{
				if (glm::distance(wolf->getLocation(), sheep->getLocation()) < SHEEP_DETECTION_AREA)
				{
					sheep->addSeenWolf(wolf);
				}
			}
		}

		//After updating  the detection of the sheep, 
		//run the  sheep "update func" so it can handle its behavoir.
		sheep->update();
	}
}

void GridWorld::updateWolfs()
{
	//update all wolfs
	for (auto &wolf : sharedWorldWolfs_)
	{
		wolf->cleanDetectionData();

		//SEE SHEEPS
		for (auto &sheep : sharedWorldSheeps_)
		{
			if (glm::distance(wolf->getLocation(), sheep->getLocation()) < WOLF_DETECTION_AREA)
			{
				wolf->addSeenSheep(sheep);
			}
		}

		//SEE WOLFS
		for (auto &wolf2 : sharedWorldWolfs_)
		{
			if (wolf->getLocation() != wolf2->getLocation())
			{
				if (glm::distance(wolf->getLocation(), wolf2->getLocation()) < WOLF_DETECTION_AREA)
				{
					wolf->addSeenWolf(wolf2);
				}
			}
		}


		wolf->update();
	}
}

void GridWorld::checkForDeath()
{
	//Check for death /*Handle Death of sheeps
	bool checkForDeath = true;
	//Check sheep death
	while (checkForDeath)
	{
		checkForDeath = false;
		for (auto &sheep : sharedWorldSheeps_)
		{
			if (sheep->getHealth() <= 0)
			{
				glm::vec2 deadSheepLoc = sheep->getLocation();

				sharedWorldSheeps_.erase(
					std::remove(
						sharedWorldSheeps_.begin(),
						sharedWorldSheeps_.end(),
						sheep),
					sharedWorldSheeps_.end());

				if ((deadSheepLoc.x + deadSheepLoc.y*GRID_SIZE_WIDTH) > 0
					&& (deadSheepLoc.x + deadSheepLoc.y*GRID_SIZE_WIDTH) < GRID_SIZE)
				{
					sharedWorldData_.at(
						deadSheepLoc.x + deadSheepLoc.y*GRID_SIZE_WIDTH)
						->spreadGrassToMe(1);
				}

				checkForDeath = true;
				break;
			}
		}
	}

	/*WOLFS!!!!!!!*/
	checkForDeath = true;
	//Check wolf death
	while (checkForDeath)
	{
		checkForDeath = false;
		for (auto &wolf : sharedWorldWolfs_)
		{
			if (wolf->getHealth() <= 0)
			{
				glm::vec2 deadWolfLoc = wolf->getLocation();

				sharedWorldWolfs_.erase(
					std::remove(
						sharedWorldWolfs_.begin(),
						sharedWorldWolfs_.end(),
						wolf),
					sharedWorldWolfs_.end());

				if ((deadWolfLoc.x + deadWolfLoc.y*GRID_SIZE_WIDTH) > 0
					&& (deadWolfLoc.x + deadWolfLoc.y*GRID_SIZE_WIDTH) < GRID_SIZE)
				{
					sharedWorldData_.at(
						deadWolfLoc.x + deadWolfLoc.y*GRID_SIZE_WIDTH)
						->spreadGrassToMe(1);
				}

				checkForDeath = true;
				break;
			}
		}
	}

}

void GridWorld::checkForBirth()
{
	bool checkForBirth = true;
	
	/*SHEEPS*/
	while (checkForBirth)
	{
		checkForBirth = false;

		for (auto &sheep : sharedWorldSheeps_)
		{
			//Check if sheep wants to reproduce 
			if (sheep->getCurrentState() == MORE_SHEEP)
			{
				sheep->setCurrentState(SEEK);
				if (!spawnNewSheep(sheep->getLocation()))
				{
					sheep->setHealth(sheep->getHealth() + SHEEP_HEALTH_LOST_PRODUCE);
				}
				checkForBirth = true;
				break;
			}
		}
	}
	
	/*WOLFS*/
	checkForBirth = true;
	while (checkForBirth)
	{
		checkForBirth = false;

		for (auto &wolf : sharedWorldWolfs_)
		{
			//Check if sheep wants to reproduce 
			if (wolf->getState() == MORE_WOLVES)
			{
				wolf->setState(WALK_AROUND);
				if (!spawnNewWolf(wolf->getLocation()))
				{
					wolf->setHealth(wolf->getHealth() + SHEEP_HEALTH_LOST_PRODUCE);
				}
				checkForBirth = true;
				break;
			}
		}
	}
}

void GridWorld::generateWorldData(int amount)
{
	//If grass amount is more then numbers of grid, force all to have grass and not slump it.
	bool forceGrass = false;
	bool makeDirt = false;
	if (amount >= GRID_SIZE_HEIGHT * GRID_SIZE_WIDTH)
		forceGrass = true;
	//Else if its not, but its above half, make all grass and then remove randoms.
	
	//Create the world
	int helper = 0;
	for (int y = 0; y < GRID_SIZE_HEIGHT; y++)
	{
		for (int x = 0; x < GRID_SIZE_WIDTH; x++)
		{
			if (forceGrass)
				sharedWorldData_.emplace_back(std::make_shared<Grass>
					(
					system_->resourceManager,
					glm::vec2(GRID_SIZE_WIDTH, GRID_SIZE_HEIGHT),
					glm::vec2(x, y),
					(std::rand() % GRASS_MAX_HEALTH) + 1
					));
			else
				sharedWorldData_.emplace_back(std::make_shared<Grass>(
					system_->resourceManager,
					glm::vec2(GRID_SIZE_WIDTH, GRID_SIZE_HEIGHT),
					glm::vec2(x, y),
					0
					));
		}
	}
	//Add grass to the world. (if not added already true force
	if (forceGrass == false)
	{
		for (int i = 0; i < amount; )
		{
			int ranTile = std::rand() % GRID_SIZE;

			if (sharedWorldData_.at(ranTile)->spreadGrassToMe((std::rand() % GRASS_MAX_HEALTH) + 1))
				i++;


		}
	}

	for (int i = 0; i < sharedWorldData_.size(); i++)
	{
		if (sharedWorldData_.at(i)->getLocation().x < GRID_SIZE_WIDTH - 1)	//Right
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + 1));

			if (sharedWorldData_.at(i)->getLocation().y < GRID_SIZE_HEIGHT - 1)	//Bottom Right
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + GRID_SIZE_WIDTH + 1));
			}

			if (sharedWorldData_.at(i)->getLocation().y > 0)		//Top Right
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - GRID_SIZE_WIDTH + 1));
			}
		}

		if (sharedWorldData_.at(i)->getLocation().x > 0)	//Left
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - 1));

			if (sharedWorldData_.at(i)->getLocation().y < GRID_SIZE_HEIGHT - 1)	//Bottom Left
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + GRID_SIZE_WIDTH - 1));
			}

			if (sharedWorldData_.at(i)->getLocation().y > 0)		//Top Left
			{
				sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - GRID_SIZE_WIDTH - 1));
			}
		}

		if (sharedWorldData_.at(i)->getLocation().y < GRID_SIZE_HEIGHT - 1)	//Bottom
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i + GRID_SIZE_WIDTH));
		}

		if (sharedWorldData_.at(i)->getLocation().y > 0)		//Top
		{
			sharedWorldData_.at(i)->addNeighbor(sharedWorldData_.at(i - GRID_SIZE_WIDTH));
		}


	}

}

void GridWorld::generateSheep(int amount)
{
	for (int i = 0; i < amount; i++)
	{
		glm::vec2 randomLoc(
			std::rand() % GRID_SIZE_WIDTH,
			std::rand() % GRID_SIZE_HEIGHT);

		while (isSpotFree(randomLoc) == false)
		{
			randomLoc = glm::vec2(
				std::rand() % GRID_SIZE_WIDTH,
				std::rand() % GRID_SIZE_HEIGHT);
		}

		sharedWorldSheeps_.emplace_back(std::make_shared<Sheep>(
			system_->resourceManager,
			randomLoc
			));
	}
}

void GridWorld::generateWolfs(int amount)
{
	for (int i = 0; i < amount; i++)
	{
		glm::vec2 randomLoc(
			std::rand() % GRID_SIZE_WIDTH,
			std::rand() % GRID_SIZE_HEIGHT);

		while (isSpotFree(randomLoc) == false)
		{
			randomLoc = glm::vec2(
				std::rand() % GRID_SIZE_WIDTH,
				std::rand() % GRID_SIZE_HEIGHT);
		}

		sharedWorldWolfs_.emplace_back(std::make_shared<Wolf>(
			system_->resourceManager,
			randomLoc
			));
	}
}


bool GridWorld::isSpotFree(glm::vec2 loc)
{
	bool isSpotFree = true;

	for (auto &sheep : sharedWorldSheeps_)
	{
		if (sheep->getLocation() == loc)
		{
			isSpotFree = false;
			break;
		}
	}

	for (auto &wolf : sharedWorldWolfs_)
	{
		if (wolf->getLocation() == loc)
		{
			isSpotFree = false;
			break;
		}
	}


	return isSpotFree;
}

bool GridWorld::spawnNewSheep(glm::vec2 parentLoc)
{
	bool spawnedSheep = false;
	glm::vec2 randomLoc = glm::vec2(0);

	bool searchingForSpace = true;
	int testes = 0;

	while (searchingForSpace && testes < 8)
	{
		//y
		if (parentLoc.y == 0)
			randomLoc.y = std::rand() % 2;
		else if (parentLoc.y == GRID_SIZE_HEIGHT - 1)
			randomLoc.y = std::rand() % 2 - 1;
		else
			randomLoc.y = std::rand() % 3 - 1;
		//X
		if (parentLoc.x == 0)
			randomLoc.x = std::rand() % 2;
		else if (parentLoc.x == GRID_SIZE_WIDTH - 1)
			randomLoc.x = std::rand() % 2 - 1;
		else
			randomLoc.x = std::rand() % 3 - 1;


		testes++;

		searchingForSpace = false;
		for (auto &sheep : sharedWorldSheeps_)
		{
			if (sheep->getLocation() == (parentLoc + randomLoc))
			{
				searchingForSpace = true;
				continue;
			}
		}
	}

	if (testes < 8)
	{
		sharedWorldSheeps_.emplace_back(std::make_shared<Sheep>(
			system_->resourceManager,
			parentLoc + randomLoc,
			50
			));
		spawnedSheep = true;
	}

	return spawnedSheep;
}

bool GridWorld::spawnNewWolf(glm::vec2 parentLoc)
{
	bool spawnedWolf = false;
	glm::vec2 randomLoc = glm::vec2(0);

	bool searchingForSpace = true;
	int testes = 0;

	while (searchingForSpace && testes < 8)
	{
		//y
		if (parentLoc.y == 0)
			randomLoc.y = std::rand() % 2;
		else if (parentLoc.y == GRID_SIZE_HEIGHT - 1)
			randomLoc.y = std::rand() % 2 - 1;
		else
			randomLoc.y = std::rand() % 3 - 1;
		//X
		if (parentLoc.x == 0)
			randomLoc.x = std::rand() % 2;
		else if (parentLoc.x == GRID_SIZE_WIDTH - 1)
			randomLoc.x = std::rand() % 2 - 1;
		else
			randomLoc.x = std::rand() % 3 - 1;


		testes++;

		searchingForSpace = false;
		for (auto &wolf : sharedWorldWolfs_)
		{
			if (wolf->getLocation() == (parentLoc + randomLoc))
			{
				searchingForSpace = true;
				continue;
			}
		}
	}

	if (testes < 8)
	{
		sharedWorldWolfs_.emplace_back(std::make_shared<Wolf>(
			system_->resourceManager,
			parentLoc + randomLoc
			));
		spawnedWolf = true;
	}

	return spawnedWolf;
}
