#include "Grass.h"



Grass::Grass()
{
}

Grass::Grass(PotatoEngine::ResourceManager * resManger, glm::vec2 worldsize, glm::vec2 loc, char health)
	:
	resourceManger_(resManger),
	currentState_(DEAD)
{
	loc_ = loc;
	health_ = health;

	float heX = (1920 / worldsize.x);
	float heY = (1080 / worldsize.y);

	drawArea_ = sf::FloatRect(
		loc_.x * heX - 0.1f,
		loc_.y * heY - 0.1f,
		heX + 0.2f,
		heY + 0.2f);

	textureSize_ = sf::IntRect(0, 0, 16, 16);
	textureColor_ = sf::Color(255, 255, 255, 255);

}


Grass::~Grass()
{
}


void Grass::addNeighbor(std::shared_ptr<Grass> neighbor)
{
	neighbors_.push_back(neighbor);
}

void Grass::update()
{
	updateState();
	switch (currentState_)
	{
	case DEAD: break;
	case DYING: dying(); break;
	case GROWING: growing(); break;
	case MATURE_GROWING: matureGrowing(); break;
	case MATURE_DYING: matureDying(); break;
	}
}

void Grass::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	//Selecft wish image to use depending on the grass health value.
	if (currentState_ != DEAD)
	{
		if (health_ > 80)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_1.png");
		else if (health_ > 60)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_2.png");
		else if (health_ > 40)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_3.png");
		else if (health_ > 20)
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_4.png");
		else
			imageTexture_ = resourceManger_->getTexture("resources/textures/Grass_5.png");
	}
	else
	{
		imageTexture_ = resourceManger_->getTexture("resources/textures/Dirt.png");
	}
	//Draw the grass
	spriteBatch->draw(
		imageTexture_,
		drawArea_,
		textureSize_,
		textureColor_);
}

bool Grass::spreadGrassToMe(char health)
{
	//If it does not have grass, spread it to this tile. else do nothig..
	int x = loc_.x;
	int y = loc_.y;
	if (currentState_ == DEAD)
	{
		health_ = health;	
		//successfully spread grass to this tile.
		return true;
	}
	else
	{
		//TIle already had grass, return false.
		return false;
	}
}

void Grass::dealDamage(short amount)
{
	health_ -= amount;
	if (health_ <= 0)
		health_ = 0;
}

void Grass::spreadGrass()
{
	int amountOfNeighbors = neighbors_.size();

	if (amountOfNeighbors == 0)
		return;


	if (std::rand() % 100 <= GRASS_SPREAD_CHANCE)
	{
		int chosenOne = std::rand() % amountOfNeighbors;		
		neighbors_.at(chosenOne)->spreadGrassToMe(10);		
	}
	
}

void Grass::updateState()
{
	//If current health is 0 or belove, the grass is dead.
	if (health_ <= 0)
		currentState_ = DEAD;

	//If the current state is dead, but the health is above 0, change it to growing.
	if(currentState_ == DEAD
		&& health_ >= 1)
		currentState_ = GROWING;

	//If state is growing, and its above mature level, change it to mature growing.
	if (currentState_ == GROWING
		&& health_ >= GRASS_MATURE_LEVEL)
		currentState_ = MATURE_GROWING;

	//If health is above max_health, change state to mature_dying.
	if (health_ >= GRASS_MAX_HEALTH)
		currentState_ = MATURE_DYING;

	//If grass is mature_dying and is belove mature level, change state to just dying.
	if (currentState_ == MATURE_DYING
		&& health_ <= GRASS_MATURE_LEVEL)
		currentState_ = DYING;
}

void Grass::dying()
{
	health_ -= GRASS_DYING_RATE;
	if (health_ <= 0)
		health_ = 0;
}

void Grass::growing()
{
	health_ += GRASS_GROW_RATE;
}

void Grass::matureGrowing()
{
	growing();
	spreadGrass();	
}

void Grass::matureDying()
{
	dying();
	spreadGrass();
}
