#include "Sheep.h"
#include "Wolf.h"
#include "Grass.h"


Sheep::Sheep()
{
	/*Empty*/
}

Sheep::Sheep(PotatoEngine::ResourceManager * resManager, glm::vec2 loc, char health)
{
	loc_ = loc;
	health_ = health;
	stamina_ = SHEEP_STAMINA_START;
	wool_ = 0;
	//Get data for later use, while printing. (sim is 1920x1080 simulated)

	textureSize_ = sf::IntRect(0, 0, 16, 16);
	textureColor_ = sf::Color(255, 255, 255, 255);
	imageTexture_ = resManager->getTexture("resources/textures/Sheep.png");
	
}

Sheep::~Sheep()
{
	cleanDetectionData();
}

void Sheep::update()
{
	/*SET BEHAVOIR*/
	updateState();

	switch (currentState_)
	{
	case EVADE: stateEvade(); break;
	case INDER_EVADE: stateInderEvade(); break;
	case EAT_GRASS: stateEatGrass(); break;
	case MORE_SHEEP: stateMoreSheep(); break;
	case SEEK: stateSeek(); break;
	case WANDER: stateWander(); break;
	case IDLE: stateIdle(); break;
	}

	//After setting  state
	//handleBehaviour();

	//Loss health each tick.
	health_ -= SHEEP_HEALTH_DECAY;
	
	//Grow wool.
	if (wool_ < SHEEP_WOOL_MAX)
	{
		wool_ = SHEEP_WOOL_REG;
		if (wool_ > SHEEP_WOOL_MAX)
			wool_ = SHEEP_WOOL_MAX;
	}
}

void Sheep::draw(PotatoEngine::SpriteBatch * spriteBatch)
{
	drawArea_ = sf::FloatRect(
		loc_.x * (1920 / GRID_SIZE_WIDTH) - 0.1f,
		loc_.y * (1080 / GRID_SIZE_HEIGHT) - 0.1f,
		(1920 / GRID_SIZE_WIDTH) + 0.2f,
		(1080 / GRID_SIZE_HEIGHT) + 0.2f);

	spriteBatch->draw(
		imageTexture_,
		drawArea_,
		textureSize_,
		textureColor_);
}

void Sheep::cleanDetectionData()
{
	detectedGrass_.clear();
	detectedSheep_.clear();
	detectedWolfs_.clear();
}

void Sheep::addSeenGrass(std::shared_ptr<Grass> seenGrass)
{
	detectedGrass_.push_back(seenGrass);
	if (seenGrass->getLocation() == loc_)
	{
		standingGrass_ = seenGrass;
	}
}

void Sheep::addSeenSheep(std::shared_ptr<Sheep> seenSheep)
{
	detectedSheep_.push_back(seenSheep);
}

void Sheep::addSeenWolf(std::shared_ptr<Wolf> seenWolf)
{
	detectedWolfs_.push_back(seenWolf);
}

glm::vec2 Sheep::getRandomMovDir()
{
	glm::vec2 randomLoc = glm::vec2(0);

	bool searchingForSpace = true;
	int testes = 0;

	while (searchingForSpace && testes < 8)
	{
		//y
		if (loc_.y == 0)
			randomLoc.y = std::rand() % 2;
		else if (loc_.y == GRID_SIZE_HEIGHT-1)
			randomLoc.y = std::rand() % 2 - 1;
		else
			randomLoc.y = std::rand() % 3 - 1;
		//X
		if (loc_.x == 0)
			randomLoc.x = std::rand() % 2;
		else if (loc_.x == GRID_SIZE_WIDTH-1)
			randomLoc.x = std::rand() % 2 - 1;
		else
			randomLoc.x = std::rand() % 3 - 1;
		

		testes++;

		searchingForSpace = false;
		for (auto &sheep : detectedSheep_)
		{
			if (sheep->getLocation() == (loc_ + randomLoc))
			{
				searchingForSpace = true;
				continue;
			}
		}
	}

	if (testes >= 8)
	{
		randomLoc = glm::vec2(0);
	}

	return randomLoc;
}

void Sheep::updateState()
{	

	//if sheep stamina is to low, it will idle to get back some stamina
	if (stamina_ <  wool_ / 10)
	{
		currentState_ = IDLE;
		return;
	}
	
	//If sheep sees wolf, and is not eating, then run! else its to  focused on eating.
	if (detectedWolfs_.size() > 0
		&& currentState_ != EAT_GRASS)
	{
		currentState_ = EVADE;
		return;
	}

	bool fleeingSheepFound = false;
	if (detectedSheep_.size() > 0
		&& currentState_ != EAT_GRASS)
	{
		locOfClosedFleeingSheep_ = glm::vec2(200, 200);
		for (auto sheep : detectedSheep_)
		{
			if (sheep->getCurrentState() == EVADE)
			{
				if (glm::distance(sheep->getLocation(), loc_) < glm::distance(locOfClosedFleeingSheep_, loc_))
				{
					locOfClosedFleeingSheep_ = sheep->getLocation();
				}
				currentState_ = INDER_EVADE;
				fleeingSheepFound = true;
			}				
		}

		if (fleeingSheepFound)
			return;
	}
	

	//Else if sheep is on grass, and is not at max, then eat some grass	
	if (standingGrass_->hasGrass() && health_ < SHEEP_MAX_HEALTH - 2)
	{
		currentState_ = EAT_GRASS;
		return;
	}

	// Else if sheep is at max,then produce more sheeps
	if (health_ >= SHEEP_REQUIRED_HP_TO_SPAWN)
	{
		currentState_ = MORE_SHEEP;
		return;
	} 

	//Else check if we have not seeked before, if not, seek
	if (currentState_ != SEEK)
	{
		currentState_ = SEEK;
		return;
	}
	
	//Else if we have  seeked before, then just wander. wander around
	currentState_ = WANDER;
	return;

}

void Sheep::stateEvade()
{
	glm::vec2 moveDir = glm::vec2(0);

	/*RUN AWAY FROM WOLF!!!!*/
	locOfClosedWolf_ = glm::vec2(100, 100);
	//Find the cloest wolf to run away from.
	for (auto &wolf : detectedWolfs_)
	{
		if (glm::distance(wolf->getLocation(), loc_) < glm::distance(locOfClosedWolf_, loc_))
		{
			locOfClosedWolf_ = wolf->getLocation();
		}
	}

	//Set move dirs
	if (locOfClosedWolf_.y > loc_.y && loc_.y > 0)
		moveDir.y = -1;
	else if (locOfClosedWolf_.y < loc_.y && loc_.y < GRID_SIZE_HEIGHT - 1)
		moveDir.y = 1;

	if (locOfClosedWolf_.x > loc_.x && loc_.x > 0)
		moveDir.x = -1;
	else if (locOfClosedWolf_.x < loc_.x && loc_.x < GRID_SIZE_WIDTH - 1)
		moveDir.x = 1;

	//move away from angry wolf!
	loc_ = loc_ + moveDir;
	//Drain stamina
	stamina_ -= wool_ / 10;


}

void Sheep::stateInderEvade()
{
	glm::vec2 moveDir = glm::vec2(0);

	/*RUN AWAY FROM FLEEING SHEEP!!!!!*/

	//Set move dirs
	if (locOfClosedFleeingSheep_.y > loc_.y && loc_.y > 0)
		moveDir.y = -1;
	else if (locOfClosedFleeingSheep_.y < loc_.y && loc_.y < GRID_SIZE_HEIGHT - 1)
		moveDir.y = 1;

	if (locOfClosedFleeingSheep_.x > loc_.x && loc_.x > 0)
		moveDir.x = -1;
	else if (locOfClosedFleeingSheep_.x < loc_.x && loc_.x < GRID_SIZE_WIDTH - 1)
		moveDir.x = 1;

	//move away from angry wolf!
	loc_ = loc_ + moveDir;
	//Drain stamina
	stamina_ -= wool_ / 10;
}

void Sheep::stateEatGrass()
{
	/*REDUCE HEALTH OF GRASS AND INCRASE SHEPE HEALTH*/
	standingGrass_->dealDamage(SHEEP_EATING_AMOUNT);
	health_ += SHEEP_EAT_HEALTH_GAIN;
	if (health_ > SHEEP_MAX_HEALTH)
		health_ = SHEEP_MAX_HEALTH;

	increaseStamina();
}

void Sheep::stateMoreSheep()
{
	/*PRODUCE MORE SHEEP AND REMOVE HEALTH*/
	health_ -= SHEEP_HEALTH_LOST_PRODUCE;
	//Production of new sheep is handled by the grid, 
	//as sheep does not handle the worlddata of sheeps.
}

void Sheep::stateSeek()
{
	/*SEARCH AND DESTROY GRASS ;)*/
	foundGrass_ = false;
	for (auto &grass : detectedGrass_)
	{
		if (grass->hasGrass())
		{
			if (foundGrass_ == false)
			{
				foundGrass_ = true;
				movementGoal_ = grass->getLocation();
			}
			else if (glm::distance(movementGoal_, loc_) >
				glm::distance(grass->getLocation(), loc_))
			{
				movementGoal_ = grass->getLocation();
			}
		}
	}
	
	increaseStamina();
}

void Sheep::stateWander()
{
	glm::vec2 moveDir = glm::vec2(0);
	/* WANDER */
	if (foundGrass_)
	{
		/*Move towards grass*/

		if (movementGoal_.x > loc_.x)
			moveDir.x = 1;
		else if (movementGoal_.x < loc_.x)
			moveDir.x = -1;

		if (movementGoal_.y > loc_.y)
			moveDir.y = 1;
		else if (movementGoal_.y < loc_.y)
			moveDir.y = -1;

		//Check so there is nothing at the space
		bool canMove = true;
		for (auto &sheep : detectedSheep_)
		{
			if (sheep->getLocation() == (loc_ + moveDir))
			{
				canMove = false;
				foundGrass_ = false;
				continue;
			}
		}

		if (canMove)
		{
			loc_ = loc_ + moveDir;
		}
	}
	else
	{
		/*Move randomly*/
		loc_ = loc_ + getRandomMovDir();
	}
	increaseStamina();
}

void Sheep::stateIdle()
{
	//Sheep is idling and geting back stamina.
	increaseStamina();
}

void Sheep::increaseStamina()
{
	stamina_ += SHEEP_STAMINA_REG;
	if (stamina_ > SHEEP_STAMINA_MAX)
		stamina_ = SHEEP_STAMINA_MAX;
}
