#ifndef ASSIGNMENT1_WOLF_H
#define ASSIGNMENT1_WOLF_H
#include <iostream>
#include <memory>
#include <vector>
#include <functional>
#include <PotatoEngine\ResourceManager.h>

#include "Entity.h"
#include "SimulationSettings.h"



class Sheep;

enum WolfState
{
	EAT_SHEEP,
	MORE_WOLVES,
	PURSUE,
	WALK_AROUND
};

class Wolf : public Entity
{
public:
	//Constructors
	Wolf();
	Wolf(PotatoEngine::ResourceManager* resManager, glm::vec2 loc, char health = WOLF_START_HEALTH);

	//Deconstructor
	~Wolf();

	void update() override;
	void draw(PotatoEngine::SpriteBatch *spriteBatch) override;

	//state stuff
	void setState(WolfState newState) { currentState_ = newState; }
	WolfState getState() { return currentState_; }

	//Detection handler
	void cleanDetectionData();
	void addSeenSheep(std::shared_ptr<Sheep> seenSheep);
	void addSeenWolf(std::shared_ptr<Wolf> seenWolf);

private:
	glm::vec2 getRandomMovDir();
	void updateClosestSheep();

	//State handling
	void updateState();
	void stateEatSheep();
	void stateMoreWolves();
	void statePursue();
	void stateWalkAround();

	//Bool handler of some stuff
	bool isEatingSheep_;
	bool foundSheep_;

	//Pointer to detected stuiff.
	std::shared_ptr<Sheep> cloestSheep_;
	std::vector<std::shared_ptr<Wolf>> detectedWolfs_;
	std::vector<std::shared_ptr<Sheep>> detectedSheep_;
	//Location of cloests sheep
	glm::vec2 cloestSheepLoc;

	//AI STUFF
	WolfState currentState_;
};


#endif // ASSIGNMENT1_WOLF_H

